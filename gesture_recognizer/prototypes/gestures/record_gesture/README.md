# Gesture Recognizer: Record Prototype

This prototype adapts the  pygarl code to allow controlling the gesture record from the desktop keyboard.

To run use:

`python -m record_gesture.record <port> <gesture_id> <target_dir>`

For example:

`python -m record_gesture.record  /dev/ttyUSB0 updown,leftright gestures`

Note that in this case, we are recording two gesture `updown` and `leftright`.
