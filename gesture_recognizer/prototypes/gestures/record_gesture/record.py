import sys

# from pygarl.data_readers import SerialDataReader
# from .recorders import record_new_samples
from .recorders import StreamRecorder, DiscreteRecorder
from .data_readers import IterativeSerialDataReader,KeyboardRecordController


def run():
    if len(sys.argv) <= 3:
        print('usage: {}  <serial port> <gesture_id> <target_dir>'.format(sys.argv[0]))
        exit(1)

    port = sys.argv[1]
    gesture_id = sys.argv[2]
    target_dir = sys.argv[3]

    serial_reader = IterativeSerialDataReader(
                        port, expected_axis=6, verbose=False)
    data_reader = KeyboardRecordController(serial_reader)

    def on_start():
        print('\nSTART RECORDING GESTURE')

    def on_stop():
        print('STOP RECORDING GESTURE')

    data_reader.on_start_recording.listen(on_start)
    data_reader.on_stop_recording.listen(on_stop)

    def on_start_loop():
        print('Press <space> to start or finish gesture recording\n')

    # recorder = StreamRecorder(data_reader,  gesture_id, target_dir, verbose=True, threshold=20,group_samples=True,sample_window=15,sample_step=8,min_sample_len=8)
    recorder = DiscreteRecorder(data_reader,  gesture_id, target_dir, plot=True)
    recorder.on_starting_loop.listen(on_start_loop)

    recorder.record_samples()


run()
