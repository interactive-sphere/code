
class Notifier(object):
    def __init__(self):
        self.listeners = []

    def notify(self, *args, **kwargs):
        for l in self.listeners:
            l(*args, **kwargs)

    def listen(self, listener):
        self.listeners.append(listener)

    def remove(self, listener):
        self.listeners.remove(listener)

    def clear(self):
        self.listeners = []
