from pygarl.abstracts import ControlSignal
from pygarl.data_readers import SerialDataReader

import sys

import logging


class LineProcessor(object):

    def __init__(self, notifier, verbose=False, expected_axis=None):
        self._notifier = notifier
        self.verbose = verbose
        self.expected_axis = expected_axis

    def notify_signal(self, signal):
        self._notifier.notify_signal(signal)

    def notify_data(self, data):
        self._notifier.notify_data(data)

    def process_line(self, line):
        ''' Analyze the received data, dispatching the correct event'''

        line = self._preprocess_line(line)

        if line == "STARTING BATCH":
            self.notify_signal(ControlSignal.START)

        elif line == "CLOSING BATCH":
            self.notify_signal(ControlSignal.STOP)

        elif line.startswith("START") and line.endswith("END"):
            self._process_data_line(line)

        elif line == "":  # This could be a timeout
            self.notify_signal(ControlSignal.TIMEOUT)

        else:  # This must be an error
            logging.warn('invalid line: "{}"'.format(line))
            self.notify_signal(ControlSignal.ERROR)

    def _preprocess_line(self, line):
        # This line is needed for backward compatibility
        # Converts the string into binary data for python > 3
        if sys.version_info >= (3,):
            line = line.decode("utf-8")

        # Deleting the new line characters
        # line = line.replace("\r\n", "")
        line = line.strip()

        # If verbosity is true, print the received line
        if self.verbose:
            print(line)

        return line

    def _process_data_line(self, line):
        # Data line, parse the data
        # A data line should have this format
        # START -36 1968 16060 -108 258 -136 END

        # Get the values by splitting the line
        value_list = line.split(" ")

        # Get the values contained in the list,
        # by removing the first and last element ( START and END )
        string_values = value_list[1:-1]

        if not self._has_expected_axis(string_values):
            # An error occurred, dispatch the ERROR event
            self.notify_signal(ControlSignal.ERROR)
        else:
            try:
                fvalues = self._to_float_values(string_values)
                # Dispatch the DATA event, sending the values
                self.notify_data(fvalues)
            except ValueError:  # Conversion error
                # An error occurred, dispatch the ERROR event
                self.notify_signal(ControlSignal.ERROR)

    def _has_expected_axis(self, values):
        if self.expected_axis is None:
            return len(values) > 0

        return len(values) == self.expected_axis

    def _to_float_values(self, string_values):
        # Convert the values from string to float
        values = []

        for value in string_values:
            # Make sure that the character can be converted to float
            # Note: may throw ValueError
            fvalue = float(value)  # Convert the string
            values.append(fvalue)  # Add to the values array

        return values
