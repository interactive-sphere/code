from __future__ import print_function
import sys

# Useful commands
# python -m pygarl record -p COM6 -m stream -g tap,doubletap,tapclockwise,tapanticlockwise,pull,push -d D:\GitHub\pygarl-datasets\finger_dataset
#


from pygarl.data_readers import FileDataReader
# from pygarl.predictors import ClassifierPredictor
# from pygarl.sample_managers import DiscreteSampleManager
from pygarl.sample_managers import StreamSampleManager

from .recognizer import make_sample_manager, make_predictor, on_gesture_notify
from .recognizer import make_magnitude_sample_manager
from gestures.record_gesture.data_readers import IterativeFileDataReader


print("LOADED")
sys.stdout.flush()


class DebugSampleManager(StreamSampleManager):

    def receive_data(self, data):
        # print('received data: ', data)

        super(DebugSampleManager, self).receive_data(data)

    def receive_signal(self, signal):
        # print('got signal: ', signal)

        super(DebugSampleManager, self).receive_signal(signal)


def main():
    if len(sys.argv) < 4:
        # Usage
        print('usage: <model_path.svm> <gesture1,gesture2,...> <measures_file>')
        exit(1)

    model_path = sys.argv[1]
    gestures = sys.argv[2]
    measures_file = sys.argv[3]

    verbose = False

    # Create the SerialDataReader
    # data_reader = FileDataReader(measures_file, verbose=verbose)
    data_reader = IterativeFileDataReader(measures_file, verbose=verbose)

    # manager = DebugSampleManager(step=10, window=30)
    # data_reader.attach_manager(manager)
    # manager = make_sample_manager(data_reader, step=10, window=30)
    manager = make_magnitude_sample_manager(data_reader, min_magnitude=1400, min_length=10)

    # Show debug info
    manager.attach_receiver(DebugReceiver())

    predictor = make_predictor(model_path)

    # Attach the classifier predictor
    manager.attach_receiver(predictor)

    on_gesture_notify(predictor, gestures)

    run_loop(data_reader)


def run_loop(data_reader):
    data_reader.open()
    print("STARTED")
    sys.stdout.flush()

    # Start the main loop
    data_reader.mainloop()
    data_reader.close()
    print("FINISHED")


class DebugReceiver(object):

    def __init__(self):
        super(DebugReceiver, self).__init__()

        self.sample_count = 0

    def receive_sample(self, sample):
        self.sample_count += 1

        gradient = sample_gradient_average(sample)

        print(('received sample #{}: ' +
              '<len:{}, data: [{},...,{}], gradient: {}>').format(
                self.sample_count,
                len(sample.data), sample.data[0], sample.data[-1], gradient))

        sample.plot()


def sample_gradient_average(sample):
    import numpy as np

    gradient = sample.gradient()

    # Calculate the average
    average = np.average(gradient)

    # Calculate the absolute value
    return np.absolute(average)


main()
