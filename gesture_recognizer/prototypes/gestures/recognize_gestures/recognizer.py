from pygarl.classifiers import SVMClassifier
from pygarl.middlewares import GradientThresholdMiddleware
# from pygarl.mocks import VerboseMiddleware
from pygarl.predictors import ClassifierPredictor
# from pygarl.sample_managers import DiscreteSampleManager
from pygarl.sample_managers import StreamSampleManager
from pygarl.base import CallbackManager

import sys

from gestures.sample_managers import MagnitudeSampleManager


class SVMThresholdClassifier(SVMClassifier):
    def __init__(self, threshold=0.5, *args, **kwargs):
        super(SVMThresholdClassifier, self).__init__(*args, **kwargs)

        self.threshold = threshold

    def predict_sample(self, sample):
        # Linearize the sample data
        linearized_sample = sample.get_linearized()

        # Predict the gesture id with the trained model
        internal_id = self.clf.predict(linearized_sample)
        predict_proba = self.clf.predict_proba(linearized_sample)
        print('predicted id: {}, predict_proba: {}'.format(internal_id, predict_proba))

        internal_id2 = (predict_proba[:, 1] >= self.threshold).astype(bool)
        print(internal_id2)

        # Convert the internal_id to the gesture_id string
        gesture_id = self.gestures[internal_id[0]]

        return gesture_id


def make_predictor(model_path, threshold=0.7):
    # Create a classifier
    # classifier = SVMClassifier(model_path=model_path)
    classifier = SVMThresholdClassifier(
                    model_path=model_path, threshold=threshold)

    # Load the model
    classifier.load()

    # Print classifier info
    classifier.print_info()

    # Create a ClassifierPredictor
    predictor = ClassifierPredictor(classifier)

    return predictor


def make_magnitude_sample_manager(
        sdr, min_magnitude=1000, max_pause=2, min_length=10):

    manager = MagnitudeSampleManager(
        min_magnitude=min_magnitude, max_pause=max_pause, min_length=min_length)

    sdr.attach_manager(manager)

    return manager


def make_sample_manager(sdr, step=2, window=10, threshold=0, verbose=False):
    # Create the SampleManager
    # manager = StreamSampleManager(step=20, window=20)
    manager = StreamSampleManager(step=step, window=window)

    # Attach the manager
    sdr.attach_manager(manager)

    # return manager

    # trim to remove "blank"

    # Create a threshold middleware
    # middleware = GradientThresholdMiddleware(verbose=False, threshold=40, sample_group_delay=5, group=True)
    # middleware = GradientThresholdMiddleware(verbose=True, threshold=10, sample_group_delay=5, group=True,autotrim=True)
    middleware = GradientThresholdMiddleware(
                    verbose=verbose, threshold=threshold, group=False,
                    autotrim=True)

    # Attach the middleware
    manager.attach_receiver(middleware)

    return middleware

    # # Filter the samples that are too short or too long
    # lfmiddleware = LengthThresholdMiddleware(verbose=False, min_len=180, max_len=600)
    # middleware.attach_receiver(lfmiddleware)
    #
    # return lfmiddleware


def on_gesture_notify(predictor, gestures):
    # Create a CallbackManager
    callback_mg = CallbackManager(verbose=False)

    # Attach the callback manager
    predictor.attach_callback_manager(callback_mg)

    gestures_list = gestures.split(',')

    print('Gestures: {}'.format(gestures_list))

    def receive_gesture(gesture):
        print("GESTURE", gesture)
        sys.stdout.flush()

    # Attach the callbacks
    for g in gestures_list:
        callback_mg.attach_callback(g, receive_gesture)
