from pygarl.base import Sample

import sys
import os
from os import path
import math

import pandas


class GestureStatisticsApp(object):

    def __init__(self):
        self.gesture_samples = {}

    def run(self):
        self.process_args()
        self.load_data()
        self.print_statistics()

    def process_args(self):
        if len(sys.argv) < 2:
            print('usage: <gestures_dir>')
            exit(1)

        self.gestures_dir = sys.argv[1]

    def load_data(self):
        for filename in os.listdir(self.gestures_dir):
            sample_file = path.join(self.gestures_dir, filename)

            sample = Sample.load_from_file(sample_file)
            g_id = sample.gesture_id

            gestures = self.gesture_samples.get(g_id, None)

            if gestures is None:
                gestures = GestureStatistic(g_id)
                self.gesture_samples[g_id] = gestures

            gestures.append_sample(sample)

    def print_statistics(self):
        cols = ['len.min', 'len.max', 'len.mean', 'num_samples',
                'magn.min', 'magn.max', 'magn.mean']
        print('\t'.join(['gesture  '] + cols))

        for g_id, gesture in self.gesture_samples.items():
            print(gesture.summary_str())


class GestureStatistic(object):
    def __init__(self, gesture_id):
        self.gesture = gesture_id
        self._samples = []
        self._samples_df = pandas.DataFrame(
            columns=[
                'framelen', 'magnitude_mean', 'magnitude_min', 'magnitude max'
            ]
        )

    def summary_str(self):
        len_range = self.samples_len_range()
        magnitude = self.samples_magnitude_range()

        values = [self.gesture] + len_range + magnitude

        return '\t'.join(map(str, values))

    def append_sample(self, sample):
        self._samples.append(sample)

        sample_magnitudes = self._magnitude_range(sample)

        self._samples_df = self._samples_df.append(
                {
                    'framelen': sample.framelen(),
                    'magnitude_mean': sample_magnitudes[0],
                    'magnitude_min': sample_magnitudes[1],
                    'magnitude_max': sample_magnitudes[2]
                }, ignore_index=True
            )

    def _magnitude_range(self, sample):
        magnitude_sum = 0
        magnitude_min = float("inf")
        magnitude_max = 0
        count = 0

        for row in sample.data:
            magnitude = self._calc_data_magnitude(row)
            print(' row: ', row, '; magnitude: ', magnitude)

            magnitude_sum += magnitude
            count += 1
            magnitude_min = min(magnitude, magnitude_min)
            magnitude_max = max(magnitude, magnitude_max)

        magnitude_mean = magnitude_sum/count

        print('magnitude mean: ', magnitude_mean)
        print('magnitude min: ', magnitude_min)
        print('magnitude max: ', magnitude_max)

        return [magnitude_mean, magnitude_min, magnitude_max]

    def _calc_data_magnitude(self, data):
        return math.sqrt(self._calc_squared_magnitude(data))

    def _calc_squared_magnitude(self, data):
        magnitude_square = 0
        idx = 0
        for value in data:
            idx += 1

            magnitude_square += value * value

        return magnitude_square

    def samples_len_range(self):
        agg_funcs = ['min', 'max', 'mean', 'count']
        agg = self._samples_df.agg(agg_funcs)

        return [float(agg.at[row, 'framelen']) for row in agg_funcs]

    def samples_magnitude_range(self):
        agg = self._samples_df.agg({
            'magnitude_min': ['min'],
            'magnitude_max': ['max'],
            'magnitude_mean': ['mean'],
        })

        return [
                float(agg.at['min', 'magnitude_min']),
                float(agg.at['max', 'magnitude_max']),
                float(agg.at['mean', 'magnitude_mean'])
            ]


GestureStatisticsApp().run()
