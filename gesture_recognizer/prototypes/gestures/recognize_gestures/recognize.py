from __future__ import print_function
import sys

# Useful commands
# python -m pygarl record -p COM6 -m stream -g tap,doubletap,tapclockwise,tapanticlockwise,pull,push -d D:\GitHub\pygarl-datasets\finger_dataset
#


from serial import SerialException

# from pygarl.classifiers import SVMClassifier, MLPClassifier
# from pygarl.middlewares import GradientThresholdMiddleware
# from pygarl.middlewares import LengthThresholdMiddleware
# from pygarl.mocks import VerboseMiddleware
from pygarl.data_readers import SerialDataReader
# from pygarl.predictors import ClassifierPredictor
# from pygarl.sample_managers import DiscreteSampleManager, StreamSampleManager

from .recognizer import make_sample_manager, make_predictor, on_gesture_notify
from .recognizer import make_magnitude_sample_manager


print("LOADED")
sys.stdout.flush()


def main():
    if len(sys.argv) < 4:
        # Usage
        print('usage: <PORT> <model_path.svm> <gesture1,gesture2,...>')
        exit(1)

    PORT = sys.argv[1]
    model_path = sys.argv[2]
    gestures = sys.argv[3]

    # Create the SerialDataReader
    sdr = SerialDataReader(PORT, expected_axis=6, verbose=False)

    # manager = make_sample_manager(sdr)
    manager = make_magnitude_sample_manager(sdr, min_magnitude=1400, min_length=10)
    predictor = make_predictor(model_path)

    # Attach the classifier predictor
    manager.attach_receiver(predictor)

    on_gesture_notify(predictor, gestures)

    run_loop(sdr)


def run_loop(sdr):
    # Open the serial connection
    try:
        sdr.open()
        print("STARTED")
        sys.stdout.flush()

        # Start the main loop
        sdr.mainloop()
    except SerialException as e:
        print("EXCEPTION")
        print(e)
        sys.stdout.flush()


main()
