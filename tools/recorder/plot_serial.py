# Code adapted from: https://thepoorengineer.com/en/arduino-python-plot

#!/usr/bin/env python

from threading import Thread
import serial
import time
import collections
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import struct
import pandas as pd

import queue
from queue import Queue

import threading


class Listeners:
    def __init__(self):
        self._lock = threading.Lock()
        self.listeners = set()

    def listen(self, listener):
        with self._lock:
            self.listeners.add(listener)

    def unlisten(self, listener):
        with self._lock:
            self.listeners.remove(listener)

    def notify(self, *args, **kwargs):
        with self._lock:
            for l in self.listeners:
                l(*args, **kwargs)


class serialPlot:
    def __init__(self, serialPort='/dev/ttyUSB0', serialBaud=38400):
        self.port = serialPort
        self.baud = serialBaud
        #
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        # self.csvData = []

        self.listeners = []

        self.serialConnection = self._make_serial_conn(serialPort, serialBaud)
        # self.line_reader = ReadLine(self.serialConnection)


    def _make_serial_conn(self, serialPort, serialBaud):
        print('Trying to connect to: {} at {} BAUD'.format(
                    serialPort,  serialBaud))
        try:
            s = serial.Serial(serialPort, serialBaud, timeout=4)
            print('Connected to {} at {} BAUD'.format(serialPort,  serialBaud))

            return s
        except:
            print("Failed to connect with " + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
            raise


    def listen(self, listener):
        self.listeners.append(listener)

    def startReadingSerial(self):
        if self.thread is None:
            self.thread = Thread(target=self.backgroundThread)
            self.thread.start()

    def send(self, value):
        self.serialConnection.write(value)
        self.serialConnection.flush()

    def wait_for_serial_output(self):
        if self.thread is None:
            raise Exception('should call start reading before')

        # Block till we start receiving values
        while self.isReceiving is not True:
            time.sleep(0.1)

    def backgroundThread(self):    # retrieve data
        time.sleep(1.0)  # give some buffer time for retrieving data
        self.serialConnection.reset_input_buffer()
        while (self.isRun):
            self.read_serial()

            self.isReceiving = True

    def read_serial(self):
        new_line = self.readline()

        for l in self.listeners:
            l(new_line)

    def readline(self):
        new_line = self.serialConnection.readline().decode('ascii')
        # for new_line in self.readlines():
        # new_line = self.line_reader.readline()

        return new_line

    def close(self):
        self.isRun = False
        self.thread.join()
        self.serialConnection.close()
        print('Disconnected...')
        # df = pd.DataFrame(self.csvData)
        # df.to_csv('/home/rikisenia/Desktop/data.csv')


class ReadLine:
    def __init__(self, s):
        self.buf = bytearray()
        self.s = s

    def readline(self, encoding='ascii'):
        return self._readline().decode(encoding)

    def _readline(self):
        i = self.buf.find(b"\n")
        if i >= 0:
            r = self.buf[:i+1]
            self.buf = self.buf[i+1:]
            return r
        while True:
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i)
            i = data.find(b"\n")
            if i >= 0:
                r = self.buf + data[:i+1]
                self.buf[0:] = data[i+1:]
                return r
            else:
                self.buf.extend(data)


class MeasuresReader:
    def __init__(self, verbose=False):
        self.verbose = verbose
        self.listeners = Listeners()

    def on_new_line(self, line):
        measures = self.parse_measures(line)

        if measures:
            self.listeners.notify(measures)

    def parse_measures(self, line):
        try:
            # line = line.decode('UTF-8')
            values = self.parse_line(line)

            if self.verbose:
                print(values)
            return values

        except queue.Empty:
            return None

        return None

    def parse_line(self, line):
        parts = line.split()

        values = {}
        for part in parts:
            key, value = self._split_key_value(part)

            if key is not None and value is not None:
                f_value = self._try_to_float(value)
                if f_value is not None:
                    values[key] = f_value

        return values

    def _split_key_value(self, key_value_text):
        if not key_value_text:
            return None, None

        parts = key_value_text.split(':', maxsplit=1)

        if len(parts) == 2:
            return parts[0], parts[1]

        return None, None

    def _try_to_float(self, str_value):
        try:
            return float(str_value)
        except ValueError:
            return None


class MeasuresAnimator:
    def __init__(self, plotLength=100, verbose=False, animate_interval_ms=50):
        '''
        animate_interval_ms: Period at which the plot animation updates [ms]
        '''
        self.plotMaxLength = plotLength
        self.verbose = verbose
        self.animate_interval_ms = animate_interval_ms

        self.measures = Queue()
    #
        self.lines_data = {}
        self.lines_plot = {}
        #
        self.isRun = True
        self.isReceiving = False
        self.plotTimer = 0
        self.previousTimer = 0
        # self.csvData = []

    def on_new_measure(self, measure):
        self.measures.put(measure)

    def start(self, title='Serial reading', xlabel='time', ylabel='measures'):
        fig, ax = plt.subplots()
        ax.set_title(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)

        timeText = ax.text(0.50, 0.95, '', transform=ax.transAxes)
        anim = animation.FuncAnimation(
                        fig, self.animate,
                        fargs=(timeText, ax),  # fargs has to be a tuple
                        interval=self.animate_interval_ms)

        plt.legend(loc="upper left")
        plt.show()

    def animate(self, frame, timeText, ax):
        self._update_time_text(timeText)

        measure = self.measures.get()
        if self.update_measures(measure, ax):
            # redraw legend
            lines = self.lines_plot.values()
            labels = [l.get_label() for l in lines]

            ax.legend(lines, labels, loc=0)

        ax.relim()
        ax.autoscale_view()

        return self.lines_plot.values()

    def _update_time_text(self, timeText):
        currentTimer = time.perf_counter()
        self.plotTimer = int((currentTimer - self.previousTimer) * 1000)     # the first reading will be erroneous
        self.previousTimer = currentTimer
        timeText.set_text('Plot Interval = ' + str(self.plotTimer) + 'ms')

    def update_measures(self, new_measures, ax):
        if not new_measures:
            return False

        new_keys = set(new_measures.keys())
        changed = self.lines_plot.keys() != new_keys

        if changed:
            current_keys = set(self.lines_plot.keys())
            removed_keys = current_keys.difference(new_keys)
            for k in removed_keys:
                l = self.lines_plot[k]
                ax.lines.remove(l)

            self.lines_plot = {
                                k: self.lines_plot[k]
                                for k in new_keys
                                if k in self.lines_plot.keys()
                              }

        for k, value in new_measures.items():
            self.update_measure_for(k, value, ax)

        return changed

    def update_measure_for(self, key, value, ax):
        # we get the latest data point and append it to our array
        # self.data.append(value)
        # lineValueText.set_text('[' + lineLabel + '] = ' + str(value))
        # lines.set_data(range(self.plotMaxLength), self.data)

        line_data, line_plot = self.get_line_data_and_plot(key, ax)

        line_data.append(value)
        line_plot.set_data(range(self.plotMaxLength), line_data)

    def get_line_data_and_plot(self, key, ax):
        line_data = self.lines_data.get(key, None)
        line_plot = self.lines_plot.get(key, None)

        if line_data is None:
            line_data = collections.deque([0] * self.plotMaxLength,
                                          maxlen=self.plotMaxLength)
            self.lines_data[key] = line_data

        if line_plot is None:
            line_plot = ax.plot([], [], label=key)[0]
            self.lines_plot[key] = line_plot

        return line_data, line_plot


class KeyboardThread:

    def __init__(self, input_cbk=None):
        self._should_stop = False
        self.listeners = Listeners()
        self.thread = None

    def listen(self, listener):
        self.listeners.listen(listener)

    def unlisten(self, listener):
        self.listeners.unlisten(listener)

    def start(self):
        if self.thread is None:
            self.thread = Thread(target=self.run)
            self.thread.daemon = True  # so to thread will finish on exit
            self.thread.start()

    def try_stop(self):
        self._should_stop = True

    def run(self):
        while not self._should_stop:
            data = input()
            # print("got line: ", data)
            self.listeners.notify(data)


class Recorder:

    def __init__(self, record_dir='.'):
        self.record_dir = record_dir

        self._clean()

    def _clean(self):
        self.measures = []
        self.measures_keys = set()
        self._recording = False
        self._file_prefix = None

    def on_new_measure(self, measure):
        if self._recording:
            self.measures.append(measure)
            self.measures_keys.update(set(measure.keys()))

    def toggle_recording(self, file_prefix=None):
        if file_prefix is not None:
            self._file_prefix = file_prefix

        if self._recording:
            self.finish()
        else:
            self.start_recording()

    def start_recording(self):
        self._recording = True

    def finish(self):
        self.save_measures()
        self._clean()

    def save_measures(self):
        outfile = self._gen_outfile_path()
        print('saving ', len(self.measures), ' measures at: ', outfile)

        with open(self._gen_outfile_path(), 'w') as f:
            self.format_measures_to(f)
            f.close()

    def _gen_outfile_path(self):
        from os import path
        import time

        timestr = time.strftime("%Y-%m-%d_%H-%M-%S")
        prefix  = self._file_prefix if self._file_prefix else 'measures'
        ext     = 'csv'

        return path.join(self.record_dir, '{}-{}.{}'.format(
            prefix, timestr, ext
        ))

    def format_measures_to(self, out_writer):
        # Using csv format
        keys = sorted(list(self.measures_keys))

        sep = '\t'

        # Header + measures
        measures = [{key: key for key in keys}] + self.measures

        for m in measures:
            for k in keys:
                value = m.get(k, '')
                out_writer.write('{}{}'.format(value, sep))
            out_writer.write('\n')


class KeyboardCommand(object):
    def __init__(self, serial_reader, kbd_input, verbose=False):
        self.serial_reader = serial_reader
        self.kbd_input = kbd_input
        self.verbose = verbose
        self.commands = {}

    def start(self):
        self.kbd_input.listen(self._on_line)
        # self.kbd_input.start()

    def _on_line(self, line):
        import shlex

        if self.verbose:
            print('sending: "{}"'.format(line))

        cmd, *args = shlex.split(line)

        if not self._matches_command(cmd, args):
            self.send_line(line)

    def send_line(self, line):
        line = line + '\r\n'
        print('sending to serial: "{}"'.format(line))

        self.serial_reader.send(line.encode())

    def listen_command(self, cmd_key, cmd_listener):
        cmd_listeners = self._get_cmd_listeners(cmd_key)

        if cmd_listeners is None:
            cmd_listeners = Listeners()
            self.commands[cmd_key.strip()] = cmd_listeners

        cmd_listeners.listen(cmd_listener)

    def _matches_command(self, cmd, args):
        cmd_listeners = self._get_cmd_listeners(cmd)

        if cmd_listeners is not None:
            cmd_listeners.notify(args)
            return True

        return False

    def _get_cmd_listeners(self, cmd_key):
        return self.commands.get(cmd_key.strip())


def main():
    # portName = 'COM5'     # for windows users
    portName = '/dev/ttyUSB0'
    baudRate = 115200
    maxPlotLength = 100
    animate_interval_ms = 5 # 50

    record_cmd = 'g'  # 'r' is already used at the Arduino code

    s = serialPlot(portName, baudRate)   # initializes all required variables
    measures_reader = MeasuresReader()
    animator = MeasuresAnimator(
            plotLength=maxPlotLength,
            animate_interval_ms=animate_interval_ms)
    kbd_input = KeyboardThread()
    kbd_cmd = KeyboardCommand(s, kbd_input)
    recorder = Recorder()

    def print_line(line):
        print("got: '{}'".format(line))

    s.listen(measures_reader.on_new_line)
    # s.listen(print_line)
    measures_reader.listeners.listen(animator.on_new_measure)
    measures_reader.listeners.listen(recorder.on_new_measure)
    kbd_cmd.listen_command(record_cmd, lambda args: recorder.toggle_recording(*args))

    s.startReadingSerial()
    kbd_input.start()
    kbd_cmd.start()

    s.wait_for_serial_output()
    animator.start()

    s.close()


if __name__ == '__main__':
    main()
