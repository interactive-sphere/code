import sys
import asyncio
import signal
import functools
import logging

from sphere.gestures.recorders import StreamRecorder, DiscreteRecorder
from sphere.gestures.data_readers import IterativeDataReader,KeyboardRecordController, SphereMeasureReader
from sphere import BluetoothConnector, SphereClient
from sphere.client import ResponseStatus

from sphere.gestures.recognize import make_predictor, on_gesture_notify, make_magnitude_sample_manager
from sphere.gestures import GestureRecognizer

from contextlib import asynccontextmanager


async def main():
    if len(sys.argv) < 3:
        # Usage
        print('usage: <model_path.svm> <gesture1,gesture2,...>')
        exit(1)

    logging.basicConfig(level=logging.INFO)

    model_path = sys.argv[1]
    gestures = sys.argv[2]
    device_name = "sphere"

    connector = await BluetoothConnector.discover(device_name)
    if connector is None:
        print("device not found")
        return

    print("found: ", connector.device)

    try:
        async with SphereClient(connector) as client:
            await start_recognizing(client, model_path, gestures)
    except asyncio.CancelledError:
        pass
    finally:
        print("finishing")

async def start_recognizing(client, model_path, gestures):
    # Create the data reader
    sphere_reader = SphereMeasureReader(client)
    data_reader = IterativeDataReader(sphere_reader)

    recognizer = GestureRecognizer(data_reader)
    recognizer.load_gestures(model_path)

    def on_gesture(gesture):
        print('recognized: ', gesture)

    recognizer.on_gesture(on_gesture, gestures=gestures)

    await recognizer.start_recognizing()



loop = asyncio.get_event_loop()
main_task = loop.create_task(main())
for signal in [signal.SIGINT, signal.SIGTERM]:
    loop.add_signal_handler(signal, main_task.cancel)
loop.run_until_complete(main_task)
