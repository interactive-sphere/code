import asyncio

from sphere import *

async def run():
    connector = await BluetoothConnector.discover("sphere")

    print("found: ", connector.device)

    async with SphereClient(connector) as client:
        print(client)

        def on_response(response):
            print(response, '\n')

        r = await client.get("/")
        print("routes:", r.json_body())

        orient_listener = client.listen("/orientation", headers = {
            'interval': 500,
            'format': 'AccelGyro'
        },  on_response =
                lambda res: print("orientation({}): {}".format(
                                    res.header('format'), res.body)))

        press_listener = client.listen("/pressure/sensors",
            lambda r: print("pressure: ", r.body))

        await press_listener.next() # await first response

        print("press button on sphere to finish")
        await press_listener.next()

        print("stopping")
        print(await press_listener.stop())
        print(await orient_listener.stop())



loop = asyncio.get_event_loop()
loop.run_until_complete(run())