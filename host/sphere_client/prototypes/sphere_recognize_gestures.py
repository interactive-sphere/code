import asyncio
import sys
import signal
import logging

from sphere import SphereControl


class SphereRecognizerApp:
    def __init__(self):
        self.gesture_model = None
        self.device_name = None
        self.control = None

    async def run(self):
        logging.basicConfig(level=logging.INFO)

        if(not self.validate_input()):
            return

        async with SphereControl.connect_with(self.device_name) as control:
            # self.control = control
            await control.load_gestures(self.gesture_model)

            control.on_gesture(self.on_gesture, gestures=self.gestures)

            print("press CTRL+C to interrupt")
            await self.wait_interrupt()


    def on_gesture(self, gesture):
        print('recognized: ', gesture)

    async def wait_interrupt(self):
        loop = asyncio.get_event_loop()

        fut = asyncio.Future()

        def handler(signum, frame):
            print('got signal: ', signum)
            fut.set_result(signum)
            fut.done()

        # signal.signal(signal.SIGINT, handler)

        try:
            await fut
        except asyncio.CancelledError:
            print('cancelled. Finishing')

    def validate_input(self):
        if(len(sys.argv) <  2):
            print("usage: <gesture_model> [<gesture1,gesture2,...>] [device_name]")

            return False

        self.gesture_model = sys.argv[1]
        self.gestures = None if len(sys.argv) < 3 else sys.argv[2].split(",")
        self.device_name = "sphere" if len(sys.argv) < 4 else sys.argv[3]

        logging.debug(f"gestures: {self.gestures}")

        return True

app = SphereRecognizerApp()

# loop = asyncio.get_event_loop()
# loop.run_until_complete(app.run())

loop = asyncio.get_event_loop()
main_task = loop.create_task(app.run())
for signal in [signal.SIGINT, signal.SIGTERM]:
    loop.add_signal_handler(signal, main_task.cancel)
try:
    loop.run_until_complete(main_task)
finally:
    loop.close()