from .bte_scanner import BluetoothScanner

class BluetoothConnector(object):

    @staticmethod
    async def discover(device_name):
        device = await BluetoothScanner().discover(device_name)

        if device is None:
            return None

        return BluetoothConnector(device)

    def __init__(self, bte_device):
        self.device = bte_device

    async def connect(self):
        await self.device.connect()

    async def disconnect(self):
        self.device.disconnect()

    def is_connected(self):
        return self.device.is_connected()

    @property
    def writer(self):
        return self.device.writer if self.device else None

    @property
    def reader(self):
        return self.device.reader if self.device else None