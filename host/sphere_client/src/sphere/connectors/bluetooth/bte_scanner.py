import bluetooth

import asyncio

from .device import Device


class BluetoothScanner(object):
    def __init__(self):
        self.discoverer = AsyncDiscoverer()

    async def discover(self, device_name):

        self.discoverer.find_devices(lookup_names=True)

        # async for d in discoverer.async_discover():
        while self.discoverer.is_discovering():
            d = await self.discoverer.discover_next()

            if not d:
                continue

            bte_device = Device(*d)

            if bte_device.match_name(device_name):
                self.discoverer.cancel_inquiry()

                return bte_device

        # Not found
        return None


class AsyncDiscoverer(bluetooth.DeviceDiscoverer):

    def __init__(self, *args, **kwargs):
        super(AsyncDiscoverer, self).__init__(*args, **kwargs)

        self.devices = []

    def pre_inquiry(self):
        self.done = False

    def device_discovered(self, address, device_class, rssi, name):
        # print("%s - %s" % (address, name))

        self.devices.append((address, name, device_class, rssi))

    # async def async_discover(self, loop=None):
    #
    #     if loop is None:
    #         loop = asyncio.get_event_loop()
    #
    #     self.find_devices(lookup_names=True)
    #
    #     try:
    #         while not self.done:
    #             await wait_ready(self, loop)
    #             dev = await read_discovered(self)
    #
    #             if dev:
    #                 yield dev
    #     except Exception:
    #         if self.is_inquiring:
    #             self.cancel_inquiry()
    #         raise

    def is_discovering(self):
        return (self.is_inquiring and not self.done) or len(self.devices) > 0

    async def discover_next(self, loop=None):
        if loop is None:
            loop = asyncio.get_event_loop()

        while not self.done:
            await wait_ready(self, loop)
            dev = await read_discovered(self)

            if dev:
                return dev

        return None

    def inquiry_complete(self):
        print("completed!")
        self.done = True

    def discovered(self):
        if self.devices:
            return self.devices.pop()
        return None


def on_ready(discoverer):
    print('on ready')
    discoverer.process_event()

    print(discoverer.discovered())


async def wait_ready(fd, loop):
    future = asyncio.Future()
    loop.add_reader(fd, future.set_result, None)
    future.add_done_callback(lambda f: loop.remove_reader(fd))
    await future


async def read_discovered(d):
    d.process_event()
    return d.discovered()
