import logging
import asyncio

from contextlib import asynccontextmanager

from sphere import BluetoothConnector, SphereClient
from sphere.gestures import GestureRecognizer
from sphere.gestures.data_readers import SphereMeasureReader, IterativeDataReader


class SphereControl:
    @staticmethod
    @asynccontextmanager
    async def connect_with(device_name):
        connector = await BluetoothConnector.discover(device_name)
        if connector is None:
            logging.info("device not found")
            return

        logging.info("found: {}".format(connector.device))

        control = SphereControl(connector)
        try:
            await control.connect()
            yield control
        finally:
            await control.stop()


    async def __aenter__(self):
        await self.start()

    async def __aexit__(self, exc_type, exc_value, exc_tb):
        await self.stop()


    def __init__(self, connector, loop = None):
        self.client = SphereClient(connector)
        self.sphere_reader = SphereMeasureReader(self.client)
        self.iter_reader = IterativeDataReader(self.sphere_reader)
        self.gesture_recognizer = GestureRecognizer(self.iter_reader)
        self.recognizer_task = None

    async def connect(self):
        await self.client.connect()

    async def load_gestures(self, gesture_model, **kwargs):
        self.gesture_recognizer.load_gestures(gesture_model, **kwargs)

        await self._start_recognition()

    def on_gesture(self, callback, gestures=None, verbose=False):
        self.gesture_recognizer.on_gesture(callback, gestures=gestures, verbose=verbose)


    async def _start_recognition(self):
        # Cancel previous recognizer task if exists
        if self.recognizer_task:
            await self._cancel_recognizer_task()

        logging.debug('starting recognizing task')
        self.recognizer_task = asyncio.create_task(
            self.gesture_recognizer.start_recognizing()
        )

    async def stop(self):
        logging.info("stopping control")
        self.gesture_recognizer.stop_recognizing()
        await self._cancel_recognizer_task()
        await self.client.disconnect()

    async def _cancel_recognizer_task(self):
        if self.recognizer_task and not self.recognizer_task.cancelled():
            try:
                self.recognizer_task.cancel()
                await self.recognizer_task
                self.recognizer_task = None
            except asyncio.CancelledError:
                pass


