import re
import asyncio
import json

from enum import Enum
from asyncio import Future, CancelledError


class SphereClient(object):

    def __init__(self, connector):
        self.connector = connector
        self.response_stream = None
        self.req_counter = 0

    async def __aenter__(self):
        await self.connect()

        return self

    async def __aexit__(self, exc_type, exc_value, exc_tb):
        if self.response_stream:
            self.response_stream.stop()
            await self.response_stream.read_task

        if self.connector.is_connected():
            await self.connector.disconnect()

    async def connect(self):
        await self.connector.connect()
        self.response_stream = ResponseStream(self.connector.reader)
        self.response_stream.start()

    async def disconnect(self):
        if self.connector.is_connected():
            await self.connector.disconnect()

        if self.response_stream:
            self.response_stream.stop()
            self.response_stream = None

    def get(self, resource, on_response=None, headers=None):
        id = self._write_request("GET", resource, headers=headers)

        return self.listen_for_response(id, on_response, once=True).next()

    def listen(self, resource, on_response, headers=None):
        id = self._write_request("LISTEN", resource, headers=headers)

        def on_stop_listen():
            return self.unlisten(resource, id).next()

        return self.listen_for_response(id, on_response, on_stop=on_stop_listen)

    def unlisten(self, resource, req_id, on_response=None):
        id = self._write_request("UNLISTEN", resource, headers={

            # NOTE: device still not support multiple listeners
            'listener': req_id
        })

        return self.listen_for_response(id, on_response=on_response)

    def _write_request(self, method, resource, id=None, headers=None):
        if id is None:
            id = self._next_id()

        req_lines = ["{} {} {}".format(method, resource,  id)]

        if headers:
            for k, v in headers.items():
                req_lines.append("{}: {}".format(k, v))

        req = ("\n".join(req_lines) + "\n\n").encode()

        self.connector.writer.write(req)

        return id


    def _next_id(self):
        self.req_counter += 1
        return self.req_counter

    def listen_for_response_once(self, req_id, on_response=None, once=True):
        def on_future(fut_res):
            if on_response:
                on_response(fut_res.result())

        fut = Future()
        fut.add_done_callback(on_future)

        self.response_stream.listen(fut.set_result, id=req_id, once=once)

        return fut

    def listen_for_response(self,
            req_id, on_response=None, once=False, on_stop=None):

        return self.response_stream.listen(
            on_response, id=req_id, once=once, on_stop=on_stop)


class ResponseListener(object):
    def __init__(self, callback, listen_id=None, once=False, on_stop=None):
        self.callback = callback
        self.listen_id = listen_id
        self.once = once
        self.stopped = False
        self.on_stop = on_stop
        self.next_future = Future()

    def next(self):
        return self.next_future

    def stop(self):
        self.stopped = True

        stop_fut = self.on_stop() if self.on_stop else None

        if stop_fut:
            fut = self.next_future
            self.next_future = stop_fut

            # Chain callbacks
            stop_fut.add_done_callback(lambda f: fut.set_result(f.result()))
        else:
            self.resolve_future(None)

        return self.next_future

    def on_response(self, res):
        if self.finished():
            return

        if self.listen_id is None or self.listen_id == res.id:
            if self.once:
                self.stopped = True

            if self.callback:
                self.callback(res)
            self.resolve_future(res)

    def resolve_future(self, res):
        f = self.next_future
        self.next_future = Future()

        f.set_result(res)

    def should_remove(self):
        return self.finished()

    def finished(self):
        return self.stopped


class ResponseStream(object):
    def __init__(self, reader):
        self.res_reader = ResponseReader(reader)
        self.listeners = set()
        self.read_task = None
        self.fut_response = Future()

    def start(self):
        self.read_task = asyncio.create_task(self._read_responses())

    def stop(self):
        if self.read_task and not self.read_task.cancelled():
            self.read_task.cancel()

    def listen(self, listener, id=None, once=None, on_stop=None):
        l = ResponseListener(listener, listen_id=id, once=once, on_stop=on_stop)

        self.listeners.add(l)

        return l

    def unlisten(self, listener):
        to_remove = set()
        if listener in self.listeners:
            to_remove.add(listener)
        else:
            for l in self.listeners:
                if l and l.callback == listener:
                    to_remove.add(l)

        self.listeners.difference_update(to_remove)

    async def await_for_response(self):
        return await self.fut_response

    async def _read_responses(self):
        try:
            while(True):
                response = await self.res_reader.next()
                if response is None:
                    return

                self._notify_response(response)
        except CancelledError:
            raise
        except Exception as e:
            self._notify_error(e)

    def _notify_response(self, response):
        to_remove = set()
        for l in self.listeners:
            l.on_response(response)

            if l.should_remove():
                to_remove.add(l)

        self.listeners.difference_update(to_remove)

        self.fut_response.set_result(response)
        self.fut_response = Future()

    def _notify_error(self, error):
        self.fut_response.set_exception(error)
        self.fut_response = Future()

class ResponseReader(object):
    def __init__(self, reader):
        self.reader = reader

        NUM = "(\d+)"
        SPACES = "[ \t]+"
        SPACES_maybe = "[ \t]*"
        WORDS = "(\w[\w \t]*)"
        WORD = "(\w+)"
        ENDLINE = "\r?\n"

        self.statusline_pattern = re.compile(
            "".join([NUM, SPACES ,NUM, SPACES, WORDS, ENDLINE])
        )
        self.header_pattern = re.compile(
            "".join([WORD, ":" , SPACES_maybe, WORDS, ENDLINE])
        )

    async def next(self):
        status_line = await self._read_status_line()
        if status_line is None:
            return None

        headers = await self._read_headers()
        body = await self._read_body(headers.get('len', None))

        return Response(
            id=status_line[0],
            statuscode=status_line[1],
            statusmsg=status_line[2],
            headers=headers,
            body=body
        )


    async def _read_status_line(self):
        header_match = await self.next_line_matching(self.statusline_pattern)
        if header_match:
            r_id = header_match.group(1)
            r_code = header_match.group(2)
            status = header_match.group(3)
            return [int(r_id), int(r_code), status]

        print(header_match)

        return None

    async def _read_headers(self):
        headers = {}

        finished = False
        while not finished:
            header = await self._read_header()
            if header:
                headers[header[0]] =  header[1]

            finished = not header

        return headers

    async def _read_header(self):
        line = await self.reader.readline()
        line = line.decode('utf-8')

        match = self.header_pattern.match(line)
        if match:
            return [match.group(1), match.group(2)]

        return None

    async def _read_body(self, len_text):
        if not len_text:
            return None

        length = int(len_text)
        if length > 0:
            return await self.reader.readexactly(length)

        return None


    async def next_line_matching(self, pattern):
        matched = False

        while not matched:
            line = await self.reader.readline()
            line_str  = line.decode('utf-8')

            # print(line_str)

            matched = pattern.match(line_str)
            if matched:
                return matched
            # return matched
            # return line_str

        return None


class ResponseStatus(Enum):
    Ok           = 200
    Notification = 210
    Registered   = 211
    Unregistered = 212
    BadRequest   = 400
    NotFound     = 404
    BadFormat    = 420
    ServerError  = 500

class Response(object):
    def __init__(self,
        id=None, statuscode=None, statusmsg=None, headers=None, body=None):

        self.id=id
        self.statuscode = statuscode
        self.statusmsg = statusmsg
        self.headers = headers if headers is not None else {}
        self.body = body

    def header(self, key):
        return self.headers.get(key) if self.headers else None

    def json_body(self):
        return json.loads(self.body) if self.body is not None else None

    def __str__(self):
        out = [
            str(self.id), ' ', str(self.statuscode), ' ', self.statusmsg, '\n',
        ]

        for key, value in self.headers.items():
            out.append(key)
            out.append(': ')
            out.append(value)
            out.append('\n')

        out.append('\n')
        out.append(self.body.decode('utf-8') if self.body else '')

        return ''.join(out)