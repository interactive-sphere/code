from __future__ import print_function
import os
from pygarl.middlewares import GradientThresholdMiddleware, PlotterMiddleware
from pygarl.middlewares import LengthThresholdMiddleware
from pygarl.recorders import FileGestureRecorder
from pygarl.sample_managers import DiscreteSampleManager, StreamSampleManager

from pygarl.utils import RandomGestureChooser

from .utils import Notifier


class AbstractRecorder(object):

    def __init__(self, data_reader, gesture_id, target_dir, receivers = None,
                 verbose=False, plot=False):
        self.sdr = data_reader
        self.target_dir = target_dir

        self.gestures = self._parse_gesture_id(gesture_id)
        self.verbose = verbose
        self.should_plot = plot

        self.receivers = [] if not receivers else receivers

        self.on_starting_loop = Notifier()

    def _parse_gesture_id(self, gesture_id):
        # If the gesture id is made of multiple gestures
        # separated by a comma, extract them
        if ',' in gesture_id:
            return map(lambda x: x.strip(), gesture_id.split(","))

        return [gesture_id]

    def record_samples(self):
        """
        Used to record new samples for the specified gesture_id
        """
        self._setup()

        self._start_recording()

    def _setup(self):
        print("RECORDING NEW SAMPLES")
        self._create_target_dir()

        print("TARGET DIRECTORY:", self.target_dir)
        print("GESTURES:", ','.join(self.gestures))

        pipeline = self.configure_reading_pipeline(self.sdr)

        if self.should_plot:
            pipeline.attach_receiver(PlotterMiddleware())

        for r in self.receivers:
            print('attach_receiver: ', r)
            pipeline.attach_receiver(r)

        if not self.receivers:
            print('no receivers')

        recorder = self._create_recorder()

        # Attach the recorder to the manager
        pipeline.attach_receiver(recorder)

    def attach_receiver(self, middleware):
        if self.receivers:
            self.receivers.append(middleware)

    def _create_target_dir(self):
        # Create the target directory if it doesn't exists
        if not os.path.exists(self.target_dir):
            os.makedirs(self.target_dir)

    def configure_reading_pipeline(self, sdr):
        # Should return a SampleManager

        raise NotImplementedError(
            'needs to implement this method in derivative classes')

    def _create_recorder(self):
        # Create the gesture chooser
        gesture_chooser = RandomGestureChooser(self.gestures)

        # Create a FileGestureRecorder
        recorder = FileGestureRecorder(
            target_dir=self.target_dir,
            forced_gesture_id=gesture_chooser,
            verbose=self.verbose)

        return recorder

    def _start_recording(self):
        # Open the serial connection
        print("Opening data connection...")
        self.sdr.open()
        print("Opened!")

        print("To exit the loop, press Ctrl+C.")

        self.on_starting_loop.notify()

        # Start the main loop
        self.sdr.mainloop()


class DiscreteRecorder(AbstractRecorder):

    def configure_reading_pipeline(self, sdr):
        # Create the SampleManager
        manager = DiscreteSampleManager()

        # Attach the manager
        sdr.attach_manager(manager)

        return manager


class StreamRecorder(AbstractRecorder):

    def __init__(self, data_reader, gesture_id, target_dir,
                 threshold=50, verbose=False, plot=True,
                 sample_window=20, sample_step=20,
                 group_samples=True,
                 sample_group_delay=2, autotrim=False, trim_threshold=10,
                 min_sample_len=5,
                 max_sample_len=600):
        super(StreamRecorder, self).__init__(
            data_reader, gesture_id, target_dir, verbose=verbose, plot=plot)

        self.threshold = threshold

        self.sample_window = sample_window
        self.sample_step = sample_step

        self.group_samples=group_samples
        self.sample_group_delay = sample_group_delay
        self.autotrim = autotrim
        self.trim_threshold = trim_threshold
        self.min_sample_len = min_sample_len
        self.max_sample_len = max_sample_len

    def configure_reading_pipeline(self, sdr):

        # Create the SampleManager
        # manager = StreamSampleManager(step=20, window=20)
        manager = StreamSampleManager(
                        step=self.sample_step, window=self.sample_window)

        # Attach the manager
        sdr.attach_manager(manager)

        # Create a threshold middleware
        middleware = GradientThresholdMiddleware(
                        verbose=self.verbose,
                        threshold=self.threshold,
                        sample_group_delay=self.sample_group_delay,
                        group=self.group_samples,
                        autotrim=self.autotrim,
                        trim_threshold=self.trim_threshold)

        # Attach the middleware
        manager.attach_receiver(middleware)

        # lfmiddleware = LengthThresholdMiddleware(
        #                     verbose=self.verbose, min_len=180, max_len=600)
        lfmiddleware = LengthThresholdMiddleware(
                            verbose=self.verbose,
                            min_len=self.min_sample_len,
                            max_len=self.max_sample_len)
        middleware.attach_receiver(lfmiddleware)

        return lfmiddleware


class PiezoRecorder(AbstractRecorder):
    """
    Used to record new samples for the specified gesture_id using
    a StreamSampleManager and a GradientThresholdMiddleware
    """

    def __init__(self, data_reader, gesture_id, target_dir,
                 threshold=50, verbose=False):
        super(PiezoRecorder, self).__init__(
            data_reader, gesture_id, target_dir, verbose=verbose)

        self.threshold = threshold

    def configure_reading_pipeline(self, sdr):
        # Create the SampleManager
        manager = StreamSampleManager(step=10, window=10)

        # Attach the manager
        sdr.attach_manager(manager)

        # Create a threshold middleware
        middleware = GradientThresholdMiddleware(
                        verbose=self.verbose, threshold=self.threshold,
                        sample_group_delay=20, group=True)

        # Attach the middleware
        manager.attach_receiver(middleware)

        # Also plot the sample
        plotter_mid = PlotterMiddleware()
        middleware.attach_receiver(plotter_mid)

        return middleware
