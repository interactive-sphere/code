import asyncio
import logging
import sys

from .recognizer import make_magnitude_sample_manager, make_predictor, register_gesture_callback
from pygarl.base import CallbackManager


class GestureRecognizer(object):

    def __init__(self, data_reader):
        self.data_reader = data_reader

    # TODO: calculate gesture magnitude and length automatically from data
    def load_gestures(self, model_path,
                 min_gesture_magnitude=1400, min_gesture_len=10):

        self.manager = make_magnitude_sample_manager(
            self.data_reader,
            min_magnitude=min_gesture_magnitude, min_length=min_gesture_len)
        self.predictor = make_predictor(model_path)

        self.gestures = self.predictor.classifier.gestures

        logging.info(f'loaded model with gestures: {self.gestures}')

        # Attach the classifier predictor
        self.manager.attach_receiver(self.predictor)

    def on_gesture(self, callback, gestures=None, verbose=False):
        if gestures is None:
            gestures = self.gestures

        register_gesture_callback(self.predictor, gestures, callback, verbose=verbose)


    async def start_recognizing(self, loop=None):
        if loop is None:
            loop = asyncio.get_event_loop()

        try:
            self.data_reader.open()
            await loop.run_in_executor(None, self.data_reader.mainloop)
        finally:
            self.data_reader.close()

    def stop_recognizing(self):
        self.data_reader.close()

