from pygarl.classifiers import SVMClassifier
from pygarl.middlewares import GradientThresholdMiddleware
from pygarl.predictors import ClassifierPredictor
from pygarl.base import CallbackManager

import sys
import logging

from ..sample_managers import MagnitudeSampleManager


class SVMThresholdClassifier(SVMClassifier):
    def __init__(self, threshold=0.5, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.threshold = threshold

    def predict_sample(self, sample):
        # Linearize the sample data
        linearized_sample = sample.get_linearized()

        # Predict the gesture id with the trained model
        internal_id = self.clf.predict(linearized_sample)
        predict_proba = self.clf.predict_proba(linearized_sample)

        logging.debug('predicted id: {}, predict_proba: {}'.format(internal_id, predict_proba))

        internal_id2 = (predict_proba[:, 1] >= self.threshold).astype(bool)
        logging.debug(internal_id2)

        # Convert the internal_id to the gesture_id string
        gesture_id = self.gestures[internal_id[0]]

        logging.info("predicted: " + gesture_id)

        return gesture_id


def make_predictor(model_path, threshold=0.7):
    # Create a classifier
    classifier = SVMClassifier(model_path=model_path)
    # classifier = SVMThresholdClassifier(
                    # model_path=model_path, threshold=threshold)

    # Load the model
    classifier.load()

    # Print classifier info
    # classifier.print_info()

    # Create a ClassifierPredictor
    predictor = ClassifierPredictor(classifier)

    # print(predictor.classifier.gestures)

    return predictor


def make_magnitude_sample_manager(
        sdr, min_magnitude=1000, max_pause=2, min_length=10):

    manager = MagnitudeSampleManager(
        min_magnitude=min_magnitude, max_pause=max_pause, min_length=min_length)

    sdr.attach_manager(manager)

    return manager


def on_gesture_notify(predictor, gestures, callback):
    gestures_list = gestures.split(',')

    register_gesture_callback(predictor, gestures_list, callback)

def register_gesture_callback(predictor, gestures_list, callback, verbose=False):
    # Create a CallbackManager
    callback_mg = CallbackManager(verbose=verbose)

    # Attach the callback manager
    predictor.attach_callback_manager(callback_mg)

    # Attach the callbacks
    for g in gestures_list:
        callback_mg.attach_callback(g, callback)