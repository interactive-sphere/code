import sys
import asyncio

from contextlib import asynccontextmanager
from queue import Queue
from pygarl.abstracts import AbstractDataReader, ControlSignal

from sphere.client import ResponseStatus


class SphereMeasureReader(object):
    def __init__(self, client, loop=None):
        self.client = client
        self.line_queue =  Queue()
        self.accel_listener = None
        self.loop = loop if loop is not None else asyncio.get_event_loop()
        self.listening_task = None

    def open(self):
        # print("open")
        self.listening_task = self.loop.create_task(self.start_listening())
        # pass

    def close(self):
        return self.stop_listening()

    async def start_listening(self):
        l = self.client.listen("/orientation", headers = {
            'format': 'AccelGyro'
        },  on_response = self.on_orientation_response)

        self.accel_listener = l

        await l.next()

    def stop_listening(self):
        fut = None

        if self.accel_listener:
            fut = self.accel_listener.stop()
        else:
            fut = asyncio.Future()
            fut.done()

        if self.listening_task:
            self.listening_task.cancel()

        # Unblock queue
        self.line_queue.put(None)

        return fut

    def readline(self):
        return self.line_queue.get()

    def on_orientation_response(self, res):
        if res.statuscode != ResponseStatus.Notification.value:
            return

        measure = res.json_body()
        self.line_queue.put(self.format_measure(measure))

    def format_measure(self, measure):
        int_measure = [str(int(v)) for v in measure]

        return "START " + " ".join(int_measure) + " END"