from pynput import keyboard
from pygarl.abstracts import AbstractSampleManager, ControlSignal

import logging

from .delegate_readers import IterativeDelegateDataReader
from ..utils import Notifier


class RecordingMonitor(AbstractSampleManager):
    def __init__(self, recording_control):
        super(AbstractSampleManager, self).__init__()

        self._in_batch = False
        self._recording_control = recording_control

        self.on_start_recording = Notifier()
        self.on_stop_recording = Notifier()

    def toggle_recording(self):
        if not self.is_recording():
            self.start_recording()
        else:
            self.stop_recording()

    def start_recording(self):
        logging.debug('Sending: ControlSignal.START')
        self._recording_control.notify_signal(ControlSignal.START)

    def stop_recording(self):
        logging.debug('Sending: ControlSignal.STOP')
        self._recording_control.notify_signal(ControlSignal.STOP)

    def is_recording(self):
        return self._in_batch is True

    def receive_data(self, data):
        pass

    def receive_signal(self, signal):
        if signal == ControlSignal.START:
            self._in_batch = True
            self.on_start_recording.notify()
        elif signal == ControlSignal.STOP:
            self._in_batch = False
            self.on_stop_recording.notify()

    def package_sample(self):
        self._in_batch = False

        print('PACKAGE SAMPLE')


class KeyboardRecordController(IterativeDelegateDataReader):
    def __init__(self, iterative_data_reader, record_key=None, exit_key=None):
        super(KeyboardRecordController, self).__init__(iterative_data_reader)

        if record_key is None:
            record_key = keyboard.Key.space
        if exit_key is None:
            exit_key = keyboard.Key.esc

        self.record_key = record_key
        self.exit_key = exit_key

        self.record_monitor = RecordingMonitor(self)
        self.delegate.attach_manager(self.record_monitor)

        self.should_record = False

    @property
    def on_start_recording(self):
        return self.record_monitor.on_start_recording

    @property
    def on_stop_recording(self):
        return self.record_monitor.on_stop_recording

    def _run_loop(self):
        with keyboard.Listener(on_press=self._on_press,
                               on_release=self._on_release) as listener:
            super(KeyboardRecordController, self)._run_loop()

            listener.stop()

    def _on_press(self, key):
        if key == self.record_key:
            logging.debug('PRESSED RECORD KEY')

            # toggle state
            self.should_record = not self.should_record

        elif key == self.exit_key:
            return False

    def _on_release(self, key):
        # if key == self.record_key:
        #     logging.debug('RELEASED RECORD KEY')
        #     self.should_record = False
        pass

    def run_step(self):
        self._update_recording()

        self.delegate.run_step()

    def _update_recording(self):
        if self.should_record == self.record_monitor.is_recording():
            return

        if self.should_record:
            self.record_monitor.start_recording()
        else:
            self.record_monitor.stop_recording()

    def _toggle_state(self):
        self.record_monitor.toggle_recording()
