from pygarl.abstracts import AbstractDataReader


class AbstractDelegateDataReader(AbstractDataReader):
    """
    Pass all DataReader methods to a delegate.
    Useful as an superclass.
    """

    def __init__(self, delegate):
        self.delegate = delegate

    def attach_manager(self, manager):
        self.delegate.attach_manager(manager)

    def detach_manager(self, manager):
        self.delegate.detach_manager(manager)

    def notify_data(self, data):
        self.delegate.notify_data(data)

    def notify_signal(self, signal):
        self.delegate.notify_signal(signal)

    def open(self):
        self.delegate.open()

    def close(self):
        self.delegate.close()

    def mainloop(self):
        self.delegate.mainloop()

    def attach_receiver(self, receiver):
        self.delegate.append(receiver)

    def detach_receiver(self, receiver):
        self.delegate.remove(receiver)

    def notify_receivers(self, sample):
        self.delegate.notify_receivers(sample)



class IterativeDelegateDataReader(AbstractDelegateDataReader):
    def __init__(self, iterative_data_reader):
        super(IterativeDelegateDataReader,
              self).__init__(iterative_data_reader)

    def mainloop(self):
        try:
            self._run_loop()
        except KeyboardInterrupt:
            print('FINISHING LOOP')

    def _run_loop(self):
        while True:
            self.run_step()

    def run_step(self):
        self.delegate.run_step()
