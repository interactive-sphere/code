from pygarl.abstracts import ControlSignal
from pygarl.abstracts import AbstractDataReader

import sys
import logging


class IterativeDataReader(AbstractDataReader):

    def __init__(self, input_reader, verbose=False, expected_axis=6):
        super().__init__()

        self.input_reader = input_reader
        self.verbose = verbose
        self.expected_axis = expected_axis

        self.stopped = False

    def open(self):
        logging.debug("opening")
        self.input_reader.open()
        logging.debug("opened")

    def close(self):
        logging.debug("closing")
        self.stop()
        self.input_reader.close()
        logging.debug("closed")

    def stop(self):
        self.stopped = True

    def mainloop(self):
        """
        Endless loop that waits for data from the serial connection
        and dispatch events when they occur
        """
        # Enclosed in a try block to intercept a Ctrl+C press
        try:

            logging.debug("start main loop")
            # Start the endless loop
            while not self.stopped and self.run_step():
                pass
        except KeyboardInterrupt:  # When Ctrl+C is pressed, the loop finishes
            logging.info("got keyboard interrupt")
        finally:
            logging.info('close iterative data reader mainloop')

    def run_step(self):
        # logging.debug("step")

        line = self._readline()

        # logging.debug("got line: " + line)

        if line is not None:
            return self._process_line(line)

        return False

    def _readline(self):
        # Read a line from the serial connection
        line = self.input_reader.readline()

        if line is None:
            return None

        # This line is needed for backward compatibility
        # Converts the string into binary data for python > 3
        if sys.version_info >= (3,) and isinstance(line, (bytes, bytearray)):
            line = line.decode("utf-8")

        # Deleting the new line characters
        line = line.replace("\r\n", "")

        # If verbosity is true, print the received line
        if self.verbose:
            print(line)

        return line

    def _process_line(self, line):
        ''' Analyze the received data, dispatching the correct event'''

        if line == "STARTING BATCH":
            self.notify_signal(ControlSignal.START)

        elif line == "CLOSING BATCH":
            self.notify_signal(ControlSignal.STOP)

        elif line.startswith("START") and line.endswith("END"):
            self._process_data_line(line)

        elif line == "":  # This could be a timeout
            self.notify_signal(ControlSignal.TIMEOUT)

        else:  # This must be an error
            self.notify_signal(ControlSignal.ERROR)

        return True

    def _process_data_line(self, line):
        # Data line, parse the data
        # A data line should have this format
        # START -36 1968 16060 -108 258 -136 END

        # Get the values by splitting the line
        value_list = line.split(" ")

        # Get the values contained in the list,
        # by removing the first and last element ( START and END )
        string_values = value_list[1:-1]

        # Ensure it has the expected axis
        if len(string_values) != self.expected_axis:
            # An error occurred, dispatch the ERROR event
            self.notify_signal(ControlSignal.ERROR)
        else:
            try:
                fvalues = self._to_float_values(string_values)
                # Dispatch the DATA event, sending the values
                self.notify_data(fvalues)
            except ValueError:  # Conversion error
                # An error occurred, dispatch the ERROR event
                self.notify_signal(ControlSignal.ERROR)

    def _to_float_values(self, string_values):
        # Convert the values from string to float
        values = []

        for value in string_values:
            # Make sure that the character can be converted to float
            # Note: may throw ValueError
            fvalue = float(value)  # Convert the string
            values.append(fvalue)  # Add to the values array

        return values
