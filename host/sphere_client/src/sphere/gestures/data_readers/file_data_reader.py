from pygarl.data_readers import FileDataReader

from .line_processor import LineProcessor


class IterativeFileDataReader(FileDataReader):

    def __init__(self, file_path, verbose=False):
        super(IterativeFileDataReader, self).__init__(file_path, verbose)

        self._line_processor = LineProcessor(self, verbose=verbose)

    def mainloop(self):
        """
        Endless loop that waits for data from the serial connection
        and dispatch events when they occur
        """
        # Enclosed in a try block to intercept a Ctrl+C press
        try:
            # Start the endless loop
            while self.run_step():
                pass
        except KeyboardInterrupt:  # When Ctrl+C is pressed, the loop finishes
            print('CLOSED MAINLOOP!')

    def run_step(self):
        line = self._readline()

        if not line:
            return False

        self._process_line(line)

        return True

    def _readline(self):
        # Read a line from the file
        return self.file.readline()

    def _process_line(self, line):
        self._line_processor.process_line(line)
