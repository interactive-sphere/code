from pygarl.base import Sample
from pygarl.abstracts import ControlSignal, AbstractSampleManager


class MagnitudeSampleManager(AbstractSampleManager):

    def __init__(self, min_magnitude=1000, max_pause=2, min_length=10):
        '''
        Creates samples above whose magnitude is above the {min_magnitude}.
        The sample initiates when a value above the threshold is detected
        and finishes when {max_pause} values below the threshold are detected.
        Samples with less than than {min_length} are discarded.
        '''

        # Call the base constructor to initialize buffer and axis
        AbstractSampleManager.__init__(self)

        # Parameters should be greater than 0
        #
        for params in (
                ('min_magnitude', min_magnitude),
                ('max_pause', max_pause),
                ('min_length', min_length)):

            if params[1] < 0:
                raise ValueError(
                    '{} must be a positive value. Found {}.'.format(*params))

        self.min_magnitude = min_magnitude
        self.max_pause = max_pause
        self.min_length = min_length

        self._pause_count = 0

    def _reset_sample(self):
        self._pause_count = 0
        self.buffer = []

    def end_sample(self):
        """
        Prematurely end the sample, even if has not been completed
        """
        self.package_sample()

    def receive_signal(self, signal):
        """
        Called from a DataReader when a signal is received
        If the STOP signal is received, prematurely end the sample, even if the window size
        has not been reached
        """
        # Call the appropriate method based on the received signal
        if signal == ControlSignal.STOP:
            # When a STOP signal is received, end the sample
            self.end_sample()

    def receive_data(self, data):
        """
        Called from a DataReader when new data is available
        """
        # print(data)

        data_magnitude = self._calc_data_magnitude(data)
        # print('got data: {}, with magnitude: {}'.format(data, data_magnitude))

        if data_magnitude >= self.min_magnitude:
            # Add the current data frame to the buffer
            self.buffer.append(data)
            self._pause_count = 0

        elif self._has_started_movement():
            self.buffer.append(data)
            self._pause_count += 1  # one more blank state

            if self._has_reached_movement_pause():
                self.end_sample()

    def _calc_data_magnitude(self, data):
        import math

        return math.sqrt(self._calc_squared_magnitude(data))

    def _calc_squared_magnitude(self, data):
        magnitude_square = 0
        idx = 0
        for value in data:
            idx += 1

            magnitude_square += value * value

        return magnitude_square

    def _has_started_movement(self):
        return len(self.buffer) > 0

    def _has_reached_movement_pause(self):
        return self._pause_count >= self.max_pause

    def package_sample(self):
        """
        Package the sample with the data in the buffer and notify all the attached receivers
        """
        # Create a sample with the buffer data
        sample = Sample(data=self.buffer)
        self._reset_sample()

        if len(sample.data) >= self.min_length:
            # Notify all the attached receivers
            self.notify_receivers(sample)
