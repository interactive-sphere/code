import sys
import asyncio
import logging

from pygarl.abstracts import AbstractMiddleware

from sphere.gestures.recorders import StreamRecorder, DiscreteRecorder
from sphere.gestures.data_readers import IterativeDataReader,KeyboardRecordController, SphereMeasureReader
from sphere import BluetoothConnector, SphereClient
from sphere.client import ResponseStatus

from queue import Queue


import matplotlib.pyplot as plt
class AsyncPlotter(AbstractMiddleware):
    def __init__(self):
        super().__init__()
        self.queue = asyncio.Queue()
        self.cancelled = False

        self.task = asyncio.create_task(self.plot_loop())

    def process_sample(self, sample):
        print('got sample: ', sample)
        self.put_next(sample)

    def cancel(self):
        self.cancelled = True
        self.put_next(None)
        self.task.cancel()

    async def plot_loop(self):
        while not self.task.cancelled():
        # while not self.cancelled:
            await self.plot_next()

    async def plot_next(self):
        # print('wait for next sample')
        sample = await self.queue.get()

        if sample:
            # sample.plot(block=False)
            await self.plot_sample(sample)

    def put_next(self, sample):
        self.queue.put_nowait(sample)

    async def plot_sample(self, sample):
        #plt.ion()  # enable interactive mode
        # Clear the plot
        plt.clf()

        # Add each axis to the plot
        for axis in range(sample.data.shape[1]):
            plt.plot(sample.data[:, axis], label="AXIS_{axis}".format(axis=axis))

            plt.pause(0.01)

        # Add the axis labels
        plt.xlabel('time', fontsize=18)
        plt.ylabel('value', fontsize=16)
        plt.legend(loc='best', frameon=False)

        plt.show(block=False)


async def run():
    # logging.basicConfig(level=logging.DEBUG)

    if len(sys.argv) <= 2:
        print('usage: {}  <gesture_id> <target_dir> [device name]'.format(sys.argv[0]))
        exit(1)

    gesture_id  = sys.argv[1]
    target_dir  = sys.argv[2]
    device_name = "sphere" if len(sys.argv) <= 3 else sys.argv[3]

    connector = await BluetoothConnector.discover(device_name)
    if connector is None:
        print("device not found")
        return

    print("found: ", connector.device)

    async with SphereClient(connector) as client:
        # await asyncio.gather(plot_reward(), start_recording(client, gesture_id, target_dir))

        await start_recording(client, gesture_id, target_dir)

async def start_recording(client, gesture_id, target_dir):
    sphere_reader = SphereMeasureReader(client)
    iter_reader = IterativeDataReader(sphere_reader)
    data_reader = KeyboardRecordController(iter_reader)

    def on_start():
        print('\nSTART RECORDING GESTURE')

    def on_stop():
        print('STOP RECORDING GESTURE')

    data_reader.on_start_recording.listen(on_start)
    data_reader.on_stop_recording.listen(on_stop)

    def on_start_loop():
        print('Press <space> to start or finish gesture recording\n')

    plotter = AsyncPlotter()

    recorder = DiscreteRecorder(data_reader,  gesture_id, target_dir, plot=False, receivers=[plotter])

    recorder.on_starting_loop.listen(on_start_loop)

    await sphere_reader.start_listening()

    loop = asyncio.get_event_loop()
    await asyncio.gather(
        plotter.plot_loop(),
        loop.run_in_executor(None, recorder.record_samples)
    )





# if __name__ == "main":

loop = asyncio.get_event_loop()
loop.run_until_complete(run())
