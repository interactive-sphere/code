# Gesture Recognizer: Record Prototype

This prototype adapts the  pygarl code to allow controlling the gesture record from the desktop keyboard.

To run use:

`python <script> <port> <gesture_id> <target_dir>`

For example:

`python <script>  /dev/ttyUSB0 updown,leftright gestures`

Note that in this case, we are recording two gesture `updown` and `leftright`.
