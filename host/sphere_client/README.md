# Interactive Sphere Client

Library to connect with an Interactive Sphere device.

## Requirements

pygarl requires python with bz2.
But when using pyenv, it may be missing.
So, install these requirements before installing the pyenv version (acording to [this answer](https://stackoverflow.com/a/71457141) on stackoverflow):

```sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl git```

You may need to reinstall your python version with:

`pyenv install <version> --force`