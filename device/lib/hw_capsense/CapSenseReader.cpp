#include "reader/CapSenseReader.h"

CapSenseReader::CapSenseReader(
    int rcvPin,
    int sendPin,
    unsigned long timeoutMillis,
    uint8_t numSamples)
    : c_sense(sendPin, rcvPin)
    , numSamples(numSamples)
{

    c_sense.set_CS_Timeout_Millis(timeoutMillis);
}


long CapSenseReader::read(){
    return c_sense.capacitiveSensorRaw(numSamples);
}
