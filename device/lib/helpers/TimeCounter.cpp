#include "TimeCounter.h"

#include "ArduinoFramework.h"

TimeCounter::TimeCounter(int durationMillis)
  : durationMillis(durationMillis)
{
}

void TimeCounter::setTime(int durationMillis){
  this->durationMillis = durationMillis;
}

void TimeCounter::resetTimer(int durationMillis){
  setTime(durationMillis);
  reset();
}

bool TimeCounter::completed(){
    if (durationMillis == 0){
        return true;
    }

    return hasPassedMillis(durationMillis);
}
