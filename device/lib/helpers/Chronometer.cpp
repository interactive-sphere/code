#include "Chronometer.h"

#include "ArduinoFramework.h"

Chronometer::Chronometer(){
    reset();
}

void Chronometer::reset(){
    lastUpdate = millis();
}

bool Chronometer::hasPassedMillis(int time){
    return millis() - lastUpdate > time;
}
