#ifndef PRINT_STREAM_H
#define PRINT_STREAM_H

#include "ArduinoFramework.h"

template<typename T>
Print & operator<<(Print & out, const T & data){
    out.print(data);
    return out;
}

template<typename T>
String & operator<<(String & out, const T & data){
    out.concat(data);
    return out;
}

#endif
