#pragma once

#include "StaticVector.hpp"

template<typename T>
using Vector6D = StaticVector<T, 6>;