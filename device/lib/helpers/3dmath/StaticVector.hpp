#ifndef STATIC_VECTOR_H
#define STATIC_VECTOR_H

#include "Vector.hpp"

template<typename T, size_t N>
class StaticVector : public Vector<T>{
public:
    using super = Vector<T>;

public:
  StaticVector(){
    for (size_t i = 0; i < N; i++) {
      values[i] = 0;
    }
  }

  StaticVector(T values[N]){
    for (size_t i = 0; i < N; i++) {
      this->values[i] = values[i];
    }
  }
  StaticVector(std::initializer_list<T> l)
  {
    int i = 0;
    for (const auto & v: l) {
      if(i >= N) break;

      this->values[i] = v;

      ++i;
    }
  }

  template<typename U>
  StaticVector(const Vector<U> & other)
    : StaticVector()
  {
      this->set(other);
  }

public:
    std::size_t size() const override
    { return N; }

    const
    T * data() const   { return values;}
    T * data()         { return values;}

public:
  StaticVector<T, N> getNormalized(float scale = 1.0f) const{
    StaticVector<T, N> n{*this};
    n.normalize(scale);
    return n;
  }

public:
  T values[N];
};

#endif
