#pragma once

#include "Vector.hpp"

#include <vector>


template<typename T>
class DynamicVector : public Vector<T>{
public:
    using super = Vector<T>;

public:
  DynamicVector() = default;

  DynamicVector(int capacity)
    : values(capacity, 0)
  {}

  DynamicVector(std::vector<T> values)
    : values(std::move(values))
  {}

  template<typename U>
  DynamicVector(const Vector<U> & other)
    : DynamicVector()
  {
      this->set(other);
  }

public:
    std::size_t size() const override
    { return values.size(); }

    const
    T * data() const   { return values.data();}
    T * data()         { return values.data();}

public:
    void resize(std::size_t size) override{
        this->values.resize(size);
    }

public:
    std::vector<T> values;
};
