#ifndef LOG_H
#define LOG_H

#include <cstdio>

#define log_info(...) printf(__VA_ARGS__);
#define log_error(...) printf(__VA_ARGS__);

#endif
