#ifndef TIME_COUNTER_H
#define TIME_COUNTER_H

#include "Chronometer.h"

class TimeCounter: public Chronometer{
public:
    TimeCounter(int durationMillis=0);
    void setTime(int durationMillis);
    void resetTimer(int durationMillis);
    bool completed();

protected:
    int durationMillis;
};

#endif
