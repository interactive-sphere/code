#ifndef STRINGSTREAM_H
#define STRINGSTREAM_H

//Adapted from: https://github.com/0xTJ/StringStream

#include <ArduinoFramework.h>

#include <utility> //move

class StringStream : public Stream {
public:
    StringStream(String value = {});

    size_t write(uint8_t c) override;
    int available() override;
    int read() override;
    int peek() override;
    void flush() override{}

    StringStream& operator=(String other);
    const
    String & inStr() const;
    String & inStr(String other);

    const
    String & outStr() const {return out;}
    String & outStr()       {return out;}

    void clear(){in = "";}
protected:
    String in, out;
    int counter;
};

#endif
