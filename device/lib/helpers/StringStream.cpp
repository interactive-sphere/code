#include "StringStream.h"

StringStream::StringStream(String value)
    : in(std::move(value))
    , out()
    , counter(0)
{}

size_t StringStream::write(uint8_t c){
    out += static_cast<char>(c);
    return 1;
}

int StringStream::available(){
    return in.length() - counter;
}

int StringStream::read(){
    int v = peek();
    if (v != EOF){
        ++counter;
    }

    return v;
}

int StringStream::peek(){
    if (available() > 0) {
        return in[counter];
    }
    return EOF;
}

StringStream& StringStream::operator=(String other) {
    inStr(other);
    return *this;
}

const
String & StringStream::inStr() const {
    return in;
}

String & StringStream::inStr(String other){
    this->in = std::move(other);
    counter = 0 ;
    return in;
}
