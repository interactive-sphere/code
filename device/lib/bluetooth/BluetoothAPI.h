#ifndef BLUETOOTH_API_H
#define BLUETOOTH_API_H

//This example code is in the Public Domain (or CC0 licensed, at your option.)
//By Evandro Copercini - 2018
//
//This example creates a bridge between Serial and Classical Bluetooth (SPP)
//and also demonstrate that SerialBT have the same functionalities of a normal Serial

#include <ArduinoFramework.h>
#include <BluetoothSerial.h>

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif


class BluetoothAPI{
public:
    BluetoothSerial SerialBT;
public:
    bool setup(String deviceName);

    void update();
};

#endif //BLUETOOTH_API_H
