#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include "ArduinoFramework.h"

#include "Request.h"
#include "Response.h"

#include "3dmath/Vector3D.hpp"
#include "OrientationFormat.h"
#include "pressure/SensorDescriptions.h"
#include "pressure/Event.h"
#include "ApiRouter.h"

namespace api {

class Router;

class Parser {
public:
    String serializeResponse(const Response & response);
    String serializePressureSensor(const pressure::SensorDescription & sensor) const;
    String serializePressureSensors(const SensorDescriptions & sensors) const;
    String serializePressureEvent(const pressure::Event & evt) const;
    String serializeRoutes(const Router::RouteMap & routes);

    bool parseVector(const String & in, Vector3D<float> & out);
    bool parseOrientationFormat(const String & in, OrientationFormat & out);
};

} /* api */


#endif
