#include "ApiRequestReader.h"

#include "BoardConfig.h"
#include "log.h"

#include <cctype>

using namespace api;

RequestReader::RequestReader (Stream * stream)
    : _stream(stream)
    , state(State::Start)
{}


bool RequestReader::update(){
    if (!stream()){
        return false;
    }

    while(stream()->available() && state != State::Completed){
        auto c = stream()->read();
        buffer.push(c);

        if (c == '\n'){
            parseLine();
        }
    }

    if (state == State::Completed){
        changeState(State::Start);
        return true;
    }

    return false;
}

void RequestReader::changeState(State state){
    this->state = state;

    switch (state){
    case State::ReadingArguments:
        _request.arguments.clear();
        break;
    default:
        break;
    }
}

void RequestReader::parseFailed(const String & cause){
    log_error("%s\n", cause.c_str());
    changeState(State::Start);

    _request = {};
}

bool RequestReader::parseLine(){
    switch(state){
    case State::Start:
        parseRequest();
        break;
    case State::ReadingArguments:
        parseArguments();
        break;
    default:
        break;
    }

    return true;
}

bool RequestReader::parseRequest(){
    if(extractMethod()
        && extractPath()
        && extractReqId())
    {
        changeState(State::ReadingArguments);
        return true;
    }
    return false;
}

void RequestReader::parseArguments(){
    skipBlankAndSpaces();
    String key = extractWord(true, ':');

    if (key.length() == 0){
        changeState(State::Completed);
        return;
    }

    if (buffer.first() != ':'){//invalid
        parseFailed("Failed to parse request. Arguments has invalid format.\n");

        return;
    }
    buffer.shift();

    skipBlankAndSpaces();
    String value = readUntil('\n', false);
    _request.arguments.insert({std::move(key), std::move(value)});
}

bool RequestReader::extractMethod(){
    skipBlankAndSpaces();
    if (buffer.isEmpty()){//empty line
        return false;
    }

    String method = extractWord();

    Methods m = parseMethod(method);

    if (m != Methods::Unknown){
        _request.method = m;
        return true;
    }

    parseFailed("Failed to find method. Extracted: \"" + method + "\"");
    return false;
}

Methods RequestReader::parseMethod(String strMethod){
    static const std::map<String, Methods> methodsMap{
        {"GET", Methods::Get},
        {"PUT", Methods::Put},
        {"POST", Methods::Post},
        {"LISTEN", Methods::Listen},
        {"UNLISTEN", Methods::UnListen}
    };

    strMethod.toUpperCase();

    auto found = methodsMap.find(strMethod);

    return (found == methodsMap.end()) ? Methods{Methods::Unknown} : found->second;
}

bool RequestReader::extractPath(){
    String path = extractWord();
    if (path){
        _request.target = std::move(path);
        return true;
    }

    parseFailed("Failed to extract path");

    return false;
}
bool RequestReader::extractReqId(){
    String idStr = extractWord();

    auto id = idStr.toInt();
    if (id == 0 && idStr != "0"){
        parseFailed("Failed to parse request id");
        return false;
    }

    _request.id = id;

    return true;
}

String RequestReader::extractWord(bool skipBlank, char terminatorChar){
    if (skipBlank){
        this->skipBlankAndSpaces();
    }

    return readUntil(terminatorChar);
}


String RequestReader::readUntil(char terminatorChar, bool stopAtSpace){
    String word;

    while(!buffer.isEmpty()){
        if(isspace(buffer.first()) && stopAtSpace){
            break;
        }
        if(terminatorChar && terminatorChar == buffer.first()){
            break;
        }

        word.concat(buffer.shift());
    }

    return std::move(word);
}

void RequestReader::skipBlankAndSpaces(){
    while(!buffer.isEmpty() && isspace(buffer.first())){
        buffer.shift();
    }
}
