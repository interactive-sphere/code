#ifndef API_RESPONSE_H
#define API_RESPONSE_H

#include "ResponseStatus.h"
#include "RequestArguments.h"

#include "ArduinoFramework.h"

namespace api {

class Response{
public:
    using Status = ResponseStatus;
    using Args   = RequestArguments;

public:
    Response(long id=0,
            Status status=Status::Ok,
            String description={},
            Args args = {},
            String data={})
        : _id(id)
        , _status(status)
        , _description(std::move(description))
        , _args(std::move(args))
        , _data(std::move(data))
    {}

public:
    inline long id()             const{ return _id; };
    inline Status status()       const{ return _status;}
    inline
    const String & description() const{ return _description;}
    inline
    const String & data()        const{ return _data;}
    inline
    const Args   & args()        const{ return _args;}

    inline Response & id(long id){ _id = id; return *this;}
    inline Response & status(Status status){_status = status; return *this;}
    inline Response & description(String description){
        _description = std::move(description);
        return *this;
    }
    inline Response & data(String data){
        _data = std::move(data);
        return *this;
    }
    inline Response & args(Args args){
        _args = std::move(args);
        return *this;
    }
public:
    long _id;
    Status _status;
    String _description;
    Args _args;
    String _data;
};

} /* api */

#endif
