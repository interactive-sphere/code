#ifndef API_ROUTE_H
#define API_ROUTE_H

#include "Request.h"
#include "Response.h"

#include <functional>

namespace api {
    using Route = std::function<void(Request &, Response &)>;
} /* api */

#endif
