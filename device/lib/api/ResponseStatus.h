#ifndef RESPONSE_STATUS_H
#define RESPONSE_STATUS_H

namespace api {

class ResponseStatus{
public:
  enum Value : int{
      None = 0,
      Ok           = 200,
      Notification = 210,
      Registered   = 211,
      Unregistered = 212,
      BadRequest = 400,
      NotFound   = 404,
      BadFormat = 420,
      ServerError = 500
  };

  ResponseStatus() = default;
  constexpr ResponseStatus(Value aResponseStatus) : value(aResponseStatus) { }

  operator Value() const { return value; }

public:
    const char * c_str() const{
        switch (value){
        case None:         return "None";
        case Ok:           return "Ok";
        case Notification: return "Notification";
        case Registered:   return "Registered";
        case Unregistered: return "Unregistered";
        case BadRequest:   return "Bad request";
        case NotFound:     return "Not found";
        case BadFormat:    return "Bad format";
        case ServerError:  return "Server error";
        default:           return "Unknown status";
        }
    }

protected:
  Value value;
};

} /* api */

#endif
