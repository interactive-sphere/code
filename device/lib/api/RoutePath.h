#ifndef API_ROUTE_PATH_H
#define API_ROUTE_PATH_H

#include "Methods.h"
#include <ArduinoFramework.h>

namespace api {

class RoutePath {
public:
    RoutePath (String path={}, Methods method=Methods::Any)
        : path(std::move(path))
        , method(method)
    {}

    bool operator<(const RoutePath & other) const{
        return method < other.method
            || (method == other.method && path < other.path);
    }

    bool operator==(const RoutePath & other) const{
        return method == other.method
                && path == other.path;
    }

    String toString() const{
        return "(" + path + ", " + methodToString(method) + ")";
    }

    static String methodToString(Methods m){
        switch (m){
        case Methods::Any:
            return "Any";
        case Methods::Get:
            return "Get";
        case Methods::Post:
            return "Post";
        case Methods::Put:
            return "Put";
        case Methods::Listen:
            return "Listen";
        default:
            return "Unknown";
        }
    }

public:
    String path;
    Methods method;
};

} /* api */

#endif
