#ifndef REQUEST_ARGUMENTS_H
#define REQUEST_ARGUMENTS_H

#include "ArduinoFramework.h"

#include <map>

using RequestArguments = std::map<String, String>;

#endif
