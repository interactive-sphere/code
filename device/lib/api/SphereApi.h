#ifndef SPHERE_API_H
#define SPHERE_API_H

#include "SphereServices.h"

#include "ApiRouter.h"
#include "ApiRequestReader.h"
#include "ApiParser.h"
#include "ApiNotifier.h"

#include "routes/LedApi.h"
#include "routes/PressureApi.h"
#include "routes/SoundApi.h"
#include "routes/VibrationApi.h"
#include "routes/OrientationApi.h"

#include <Stream.h>
#include <array>

namespace api {

class SphereApi: public Notifier{
public:
    using RouteMethod = void (SphereApi::*)(Request &, Response &);
    using ApiList = std::array<Api *, 5>;

public:
    SphereApi (Stream * stream=nullptr, SphereServices * services=nullptr);
    virtual ~SphereApi (){}

public:
    void setup();
    void update();

    void disconnected();

public:
    inline SphereServices * services(){ return _services;}
    inline SphereServices * services(SphereServices * services){
        return this->_services = services;
    }

    inline Stream * stream(){ return reqReader.stream();}
    inline Stream * stream(Stream * stream){
        return reqReader.stream(stream);
    }

public:
    void notify(const Response & res) override;

protected:
    void addRoute(RoutePath path, RouteMethod m);
    void handleRequest();

    void listRoutes(Request & req, Response & res);
    void routeDefault(Request & req, Response & res);
protected:
    SphereServices * _services;
    Stream * _stream;
    RequestReader reqReader;
    Router router;
    Parser parser;

protected://apis
    LedApi ledApi;
    OrientationApi orientationApi;
    PressureApi pressureApi;
    SoundApi soundApi;
    VibrationApi vibrationApi;
    ApiList apis;
};

} /* api */

#endif
