#include "SphereApi.h"

#include "log.h"

#include <array>
#include <functional>

using namespace api;
using namespace std::placeholders;
using namespace std;

SphereApi::SphereApi (Stream * stream, SphereServices * services)
    : _services(services)
    , reqReader(stream)
    , ledApi(services)
    , orientationApi(services)
    , pressureApi(services)
    , soundApi(services)
    , vibrationApi(services)
    , apis({&ledApi, &orientationApi, &pressureApi, &soundApi, &vibrationApi})
{}

void SphereApi::setup(){
    for(auto * api: apis)
    {
        api->registerRoutes(router);
        api->notifier(this);
    }

    addRoute({"/", Methods::Get}, &SphereApi::listRoutes);
    router.defaultRoute(Router::makeRoute(&SphereApi::routeDefault, this));
}

void SphereApi::disconnected(){
    for(auto * api: apis){
        api->onDisconnected();
    }
}

void SphereApi::update(){
    if(stream() && reqReader.update()){
        handleRequest();
    }
}

void SphereApi::handleRequest(){
    Request & req = reqReader.request();
    Response res{req.id};

    router.route(req, res);

    stream()->print(parser.serializeResponse(res));
}

void SphereApi::listRoutes(Request & req, Response & res){
    res.data(parser.serializeRoutes(router.routes()));
}


void SphereApi::routeDefault(Request & req, Response & res){
    res.status(Response::Status::NotFound);
}

void SphereApi::notify(const Response & res){
    stream()->print(parser.serializeResponse(res));
}

void SphereApi::addRoute(RoutePath path, RouteMethod m){
    router.putRoute(path, std::bind(m, this, _1, _2));
}
