#ifndef API_METHODS_H
#define API_METHODS_H

namespace api {

class Methods{
public:
  enum Value{
      Unknown, Any=Unknown,
      Get,
      Put,
      Post,
      Listen,
      UnListen
  };

  constexpr Methods(Value value=Unknown) : value(value) { }

  operator Value() const { return value; }

public:
    const char * c_str() const{
        switch (value){
        case Any:          return "*";
        case Get:          return "Get";
        case Post:         return "Post";
        case Put:          return "Put";
        case Listen:       return "Listen";
        case UnListen:     return "UnListen";
        default:           return "?";
        }
    }

protected:
  Value value;
};

} /* api */

#endif
