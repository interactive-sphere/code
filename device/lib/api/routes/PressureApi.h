#ifndef PRESSURES_API_H
#define PRESSURES_API_H

#include "Api.h"
#include "SphereServices.h"

namespace api {
    class PressureApi: public Api{
    public:
        using Api::Api; //importing constructor

    public:
        virtual void registerRoutes(Router & router){
            addRoute({"/pressure/sensors", Methods::Get},
                    this, &PressureApi::getSensors, router);
            addRoute({"/pressure/sensors", Methods::Listen},
                    this, &PressureApi::listenSensors, router);
            addRoute({"/pressure/sensors", Methods::UnListen},
                    this, &PressureApi::removeListener, router);
        }

        void onDisconnected() override{
            if (services() && services()->pressure()){
                //FIXME: should check if current listen is this instance
                services()->pressure()->removeListener();
            }
        }
    protected:
        void getSensors(Request & req, Response & res){
            if(!checkServices(res)){
                return;
            }

            auto sensors = services()->pressure()->getSensorDescriptions();
            res.data(parser().serializePressureSensors(sensors));
        }

        void listenSensors(Request & req, Response & res){
            if(!checkServices(res)){
                return;
            }

            //Default status
            res.status(Response::Status::Registered);

            auto reqId = req.id;

            services()->pressure()->listenPressureEvents(
                [this, reqId](const pressure::Event & evt){
                    sendNotification({
                        reqId, Response::Status::Notification,"", {},
                        parser().serializePressureEvent(evt)
                    });
                });
        }

        void removeListener(Request & req, Response & res){
            if(!checkServices(res)){
                return;
            }

            res.status(Response::Status::Unregistered);

            services()->pressure()->removeListener();
        }

        bool checkServices(Response & res){
            if(!services() || !services()->pressure()){
                res.status(Response::Status::ServerError);
                return false;
            }

            return true;
        }
    };
} /* api */

#endif
