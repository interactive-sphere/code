#ifndef API_NOTIFIER_H
#define API_NOTIFIER_H

#include "Response.h"

namespace api {

class Notifier {
public:
    virtual ~Notifier (){}

    virtual void notify(const Response & res)=0;
};

} /* api */

#endif
