#ifndef API_REQUEST_READER_H
#define API_REQUEST_READER_H

#include "Request.h"
#include "BoardConfig.h"

#include <Stream.h>
#include <CircularBuffer.h>


namespace api {

class RequestReader {
public:
    enum class State{
        Start,
        ReadingArguments,
        Completed
    };

public:
    RequestReader(Stream * stream = nullptr);

    bool update();

public:
    inline Request & request(){ return _request;}

    inline Stream * stream(){ return _stream;}
    inline Stream * stream(Stream * stream){
        return this->_stream = stream;
    }

protected:
    void changeState(State state);
    void parseFailed(const String & cause);

protected:
    bool parseLine();
    bool parseRequest();
    void parseArguments();
    void skipBlankAndSpaces();

    bool extractMethod();
    bool extractPath();
    bool extractReqId();

    String extractWord(bool skipBlank=true, char terminatorChar=0);
    String readUntil(char terminatorChar, bool stopAtSpace=true);

    Methods parseMethod(String strMethod);

protected:
    Stream * _stream;
    State state;
    Request _request;
    CircularBuffer<char, REQUEST_BUFFER_SIZE> buffer;
};

} /* api */

#endif
