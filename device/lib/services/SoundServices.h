#ifndef SOUND_SERVICES_H
#define SOUND_SERVICES_H

#include "Service.h"

#include <vector>

class ToneDef {
public:
  ToneDef(int frequency, int duration, int pause)
    : frequency(frequency)
    , duration(duration)
    , pause(pause)
  {}

public:
  int frequency;
  int duration;
  int pause;
};

class SoundServices: public Service {
public:
  using Melody = std::vector<ToneDef>;
public:
  virtual void play(const Melody & melody)=0;
  virtual bool isPlaying()=0;
  virtual bool isOnPause()=0;
  virtual void stop()=0;
  virtual int currentToneIndex()=0;
};

#endif
