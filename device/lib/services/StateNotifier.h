#pragma once

class StateNotifier{
public:
    virtual ~StateNotifier(){};

public:
    virtual void onStartCalibration()=0;
    virtual void onStopCalibration()=0;
};