#ifndef SERVICE_H
#define SERVICE_H

class Service {
public:
  virtual ~Service (){}

  virtual void begin(){}

  virtual void update(){};
};

#endif
