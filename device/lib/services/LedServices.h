#ifndef LED_SERVICES_H
#define LED_SERVICES_H

#include "ArduinoFramework.h"

#include "Service.h"
#include "ColorDef.h"

class LedServices : public Service{
public:
  virtual void changeColor(ColorDef color)=0;

  virtual void off(){
    changeColor({0, 0, 0});
  }
};

#endif
