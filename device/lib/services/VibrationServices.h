#ifndef VIBRATE_SERVICES_H
#define VIBRATE_SERVICES_H

#include "Service.h"

class VibrationServices: public Service {
public:
  void begin() override{
    stop();
  }

  virtual void vibrate(int durationMillis)=0;
  virtual bool isVibrating()=0;
  virtual void stop()=0;
};

#endif
