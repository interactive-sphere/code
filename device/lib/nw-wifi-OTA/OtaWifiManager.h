#ifndef OTA_WIFI_MANAGER_H
#define OTA_WIFI_MANAGER_H

#include "WiFiConfigurator.h"
#include "OTAManager.h"


class OtaWifiManager{
public:
    bool begin(
      const char * hostname=MDNS_HOST,
      const char * OTA_password=OTA_PASS,
      const char * ap_ssid=nullptr, const char * ap_pass=nullptr);

    /* Remove stored setting. */
    void clearSettings();
    // TO-DO: expand interface

public:
    void update();

protected:
    WiFiConfigurator wifiManager;
    OtaManager otaManager;
};

#endif
