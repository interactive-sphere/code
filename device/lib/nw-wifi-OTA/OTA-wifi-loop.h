#ifndef OTA_WIFI_LOOP_H
#define OTA_WIFI_LOOP_H

#include "OtaWifiManager.h"

// Application callbacks
void setup_app();
void loop_app();

static OtaWifiManager otaWifiManager;

void setup(){
  setup_app();

  //Uses default configurations (see defines in OTAWifiManager.h to redefine)
  if (!otaWifiManager.begin()) {
    Serial.println("Setup Wifi and OTA failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }
}

void loop(){
  otaWifiManager.update();

  loop_app();
}



#endif
