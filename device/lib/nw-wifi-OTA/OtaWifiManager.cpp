#include "OtaWifiManager.h"


bool OtaWifiManager::begin(
  const char * hostname,
  const char * OTA_password,
  const char * ap_ssid, const char * ap_pass)
{
  return this->wifiManager.autoConnect(ap_ssid, ap_pass)
      && this->otaManager.begin(OTA_password, hostname);
}

/* Remove stored setting. */
void OtaWifiManager::clearSettings(){
  this->wifiManager.resetSettings();
}
// TO-DO: expand interface

void OtaWifiManager::update(){
  this->wifiManager.update();
  this->otaManager.update();
}
