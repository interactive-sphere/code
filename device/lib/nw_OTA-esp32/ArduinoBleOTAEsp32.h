#pragma once
#ifndef ARDUINO_BLE_OTA_ESP32_H
#define ARDUINO_BLE_OTA_ESP32_H


class ArduinoBleOTAEsp32;
extern ArduinoBleOTAEsp32 ArduinoBleOTA;

#include "BleOtaStorage.h"
#include "BleOtaVersion.h"
#include "BleOtaUploader.h"
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

namespace
{
constexpr auto UNKNOWN = "UNKNOWN";
}

class ArduinoBleOTAEsp32:
    public BLECharacteristicCallbacks,
    public BleSender
{
public:
    bool begin(const std::string& deviceName, OTAStorage& storage);
    bool begin(OTAStorage& storage);
    bool begin(const std::string& deviceName, OTAStorage& storage,
               const std::string& hwName, BleOtaVersion hwVersion,
               const std::string& swName, BleOtaVersion swVersion);
    bool begin(OTAStorage& storage,
               const std::string& hwName, BleOtaVersion hwVersion,
               const std::string& swName, BleOtaVersion swVersion);
    bool begin(BLEServer * server, OTAStorage& storage,
               const std::string& hwName = UNKNOWN,
               BleOtaVersion hwVersion   = {},
               const std::string& swName = UNKNOWN,
               BleOtaVersion swVersion   = {});
    void pull();

public:
    bool isUpdating() const {
        return bleOtaUploader.isUploading()
            || bleOtaUploader.isInstalling();
    }

private:
    void begin(BLEService& service,
               const std::string& hwName, BleOtaVersion hwVersion,
               const std::string& swName, BleOtaVersion swVersion);
    void onWrite(BLECharacteristic* characteristic) override;
    void send(const uint8_t* data, size_t length) override;
    BLECharacteristic* txCharacteristic;

protected:
    BleOtaUploader bleOtaUploader;
};

#endif