#if defined(USE_BLE_ESP32)
#include "ArduinoBleOTAEsp32.h"
#include "BleOtaUploader.h"
#include "BleOtaUuids.h"
#include "BleOtaUtils.h"
#include "BleOtaSizes.h"


// #define READ_CHARACTERISTIC NIMBLE_PROPERTY::READ
// #define WRITE_NR_PROPERTY NIMBLE_PROPERTY::WRITE_NR

#define READ_CHARACTERISTIC BLECharacteristic::PROPERTY_READ
#define WRITE_CHARACTERISTIC BLECharacteristic::PROPERTY_WRITE
#define WRITE_NR_PROPERTY BLECharacteristic::PROPERTY_WRITE_NR
#define NOTIFY_CHARACTERISTIC BLECharacteristic::PROPERTY_NOTIFY


bool ArduinoBleOTAEsp32::begin(const std::string& deviceName, OTAStorage& storage)
{
    return begin(deviceName, storage, UNKNOWN, {}, UNKNOWN, {});
}

bool ArduinoBleOTAEsp32::begin(OTAStorage& storage)
{
    return begin(storage, UNKNOWN, {}, UNKNOWN, {});
}

bool ArduinoBleOTAEsp32::begin(const std::string& deviceName, OTAStorage& storage,
                               const std::string& hwName, BleOtaVersion hwVersion,
                               const std::string& swName, BleOtaVersion swVersion)
{
    BLEDevice::init(deviceName);
    auto* server = BLEDevice::createServer();

    if(!begin(storage, hwName, hwVersion, swName, swVersion))
        return false;

    auto* advertising = server->getAdvertising();
    advertising->setScanResponse(true);
    advertising->setMinPreferred(0x06); // functions that help with iPhone connections issue
    advertising->setMaxPreferred(0x12);
    advertising->start();
    return true;
}

bool ArduinoBleOTAEsp32::begin(OTAStorage& storage,
                               const std::string& hwName, BleOtaVersion hwVersion,
                               const std::string& swName, BleOtaVersion swVersion)
{
    auto* server = BLEDevice::createServer();

    return begin(server, storage, hwName, hwVersion, swName, swVersion);
}

bool ArduinoBleOTAEsp32::begin(
    BLEServer * server, OTAStorage& storage,
    const std::string& hwName, BleOtaVersion hwVersion,
    const std::string& swName, BleOtaVersion swVersion)
{
    BLEDevice::setMTU(BLE_OTA_MTU_SIZE);

    bleOtaUploader.begin(storage, this);
    auto* service = server->createService(BLE_OTA_SERVICE_UUID);

    auto* rxCharacteristic = service->createCharacteristic(
        BLE_OTA_CHARACTERISTIC_UUID_RX,
        WRITE_NR_PROPERTY
    );
    rxCharacteristic->setCallbacks(this);

    auto* txCharacteristic = service->createCharacteristic(
        BLE_OTA_CHARACTERISTIC_UUID_TX,
        READ_CHARACTERISTIC | NOTIFY_CHARACTERISTIC
    );
    this->txCharacteristic = txCharacteristic;

    begin(*service, hwName, hwVersion, swName, swVersion);

    service->start();

    auto* advertising = server->getAdvertising();
    advertising->addServiceUUID(BLE_OTA_SERVICE_UUID);
    return true;
}

void ArduinoBleOTAEsp32::begin(BLEService& service,
                               const std::string& hwName, BleOtaVersion hwVersion,
                               const std::string& swName, BleOtaVersion swVersion)
{
    auto* hwNameCharacteristic = service.createCharacteristic(
        BLE_OTA_CHARACTERISTIC_UUID_HW_NAME,
        READ_CHARACTERISTIC
    );
    hwNameCharacteristic->setValue(hwName);
    auto* swNameCharacteristic = service.createCharacteristic(
        BLE_OTA_CHARACTERISTIC_UUID_SW_NAME,
        READ_CHARACTERISTIC
    );
    swNameCharacteristic->setValue(swName);
    auto* hwVerCharacteristic = service.createCharacteristic(
        BLE_OTA_CHARACTERISTIC_UUID_HW_VER,
        READ_CHARACTERISTIC
    );
    hwVerCharacteristic->setValue(
        (uint8_t *)refToAddr(hwVersion), sizeof(BleOtaVersion));
    auto* swVerCharacteristic = service.createCharacteristic(
        BLE_OTA_CHARACTERISTIC_UUID_SW_VER,
        READ_CHARACTERISTIC
    );
    swVerCharacteristic->setValue(
        (uint8_t *)refToAddr(swVersion), sizeof(BleOtaVersion));
}

void ArduinoBleOTAEsp32::pull()
{
    bleOtaUploader.pull();
}

void ArduinoBleOTAEsp32::onWrite(BLECharacteristic* characteristic)
{
    auto value = characteristic->getValue();
    auto data = (const uint8_t *)value.data();
    auto length = value.length();

    // Serial.println("onWrite: " + String(value.length())  + " bytes");

    bleOtaUploader.onData(data, length);
}

void ArduinoBleOTAEsp32::send(const uint8_t* data, size_t length)
{
    // Serial.println("sending " + String(length) + " bytes");
    txCharacteristic->setValue((uint8_t *)data, length);
    txCharacteristic->notify();
}

// ArduinoBleOTAEsp32 ArduinoBleOTA{};
#endif