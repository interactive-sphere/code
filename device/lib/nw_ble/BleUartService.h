#pragma once

#include <Arduino.h>

#include <functional>
#include <string>

#include "BleStack.h"
#include "CharacteristicDesc.h"


class UartUuids{
public:
    static constexpr const char * serviceId =
        "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
    static constexpr const char * rxCharacteristic =
        "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
    static constexpr const char * txCharacteristic =
        "6E400003-B5A3-F393-E0A9-E50E24DCCA9E";
};


class BleUartService: public BLECharacteristicCallbacks {
public:
    using RcvCallback = std::function<void(const std::string &)>;
    using CharacteristicsList = std::vector<CharacteristicDesc>;

protected:
    const char * serviceUiid;
    const char * rxUiid;
    const char * txUiid;

    BLEService * service = nullptr;
    BLECharacteristic * txCharacteristic;

    RcvCallback rcvCallback;

public:
    BleUartService(
        const char * serviceUiid = UartUuids::serviceId,
        const char * rxUiid = UartUuids::rxCharacteristic,
        const char *txUiid = UartUuids::txCharacteristic);

public: //getters and setters
    const char * getServiceUiid() const;

    void setOnData(RcvCallback callback);

public: //initialization
    void begin(BLEServer * server, const CharacteristicsList & extraCharacteristics = {});

    BLEService * createService(BLEServer * server, const CharacteristicsList & extraCharacteristics = {} );

protected:
    void registerCharacteristics(BLEService * service);

public:
    size_t send(const uint8_t *buffer, size_t size, bool sendAll=true);

protected: //calbacks
    void onWrite(BLECharacteristic *characteristic) override;
};
