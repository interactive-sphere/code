#include "BLECharacteristic.h"
#pragma once

#include "BleStack.h"

#include <string>

class CharacteristicDesc{
public:
  CharacteristicDesc(std::string uuid, uint32_t flags, std::string initialValue={})
    : uuid(uuid)
    , flags(flags)
    , initialValue(initialValue)
  {}

public:
  CharacteristicDesc & callbacks(BLECharacteristicCallbacks * callbacks){
    this->charactCallbacks = callbacks;

    return *this;
  }    

public:
  BLECharacteristic * createCharacteristic(BLEService * service) const{
    auto* charact = service->createCharacteristic(
        uuid,
        flags
    );

    if(!initialValue.empty()){
      charact->setValue(initialValue);
    }
    if(this->charactCallbacks != nullptr){
      charact->setCallbacks(this->charactCallbacks);
    }

    return charact;
  }

public:
  std::string uuid;
  uint32_t flags;
  std::string initialValue;

protected:
  BLECharacteristicCallbacks * charactCallbacks = nullptr;
};
