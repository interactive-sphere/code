#pragma once

#ifdef USE_NIM_BLE_ARDUINO_LIB
    #include <NimBLEDevice.h>
#else
    #include <BLEDevice.h>
    #include <BLEServer.h>
    #include <BLEUtils.h>
    #include <BLE2902.h>
#endif

#ifndef BLE_MAX_WRITE_SIZE
#if defined(ESP_GATT_MAX_ATTR_LEN)
    constexpr const unsigned BLE_MAX_WRITE_SIZE = ESP_GATT_MAX_ATTR_LEN;
#elif defined(BLE_ATT_ATTR_MAX_LEN)
    constexpr const unsigned BLE_MAX_WRITE_SIZE = BLE_ATT_ATTR_MAX_LEN;
#endif
#endif

#ifdef USE_NIM_BLE_ARDUINO_LIB
#define PROPERTY_READ       NIMBLE_PROPERTY::READ
#define PROPERTY_WRITE      NIMBLE_PROPERTY::WRITE
#define PROPERTY_WRITE_NR   NIMBLE_PROPERTY::WRITE_NR
#define PROPERTY_INDICATE   NIMBLE_PROPERTY::INDICATE
#define PROPERTY_NOTIFY     NIMBLE_PROPERTY::NOTIFY
#else
#define PROPERTY_READ       BLECharacteristic::PROPERTY_READ
#define PROPERTY_WRITE      BLECharacteristic::PROPERTY_WRITE
#define PROPERTY_WRITE_NR   BLECharacteristic::PROPERTY_WRITE_NR
#define PROPERTY_INDICATE   BLECharacteristic::PROPERTY_INDICATE
#define PROPERTY_NOTIFY     BLECharacteristic::PROPERTY_NOTIFY
#endif