#include "BleUartService.h"

BleUartService::BleUartService(
    const char * serviceUiid,
    const char * rxUiid,
    const char *txUiid)
    : serviceUiid(serviceUiid)
    , rxUiid(rxUiid)
    , txUiid(txUiid)
{}

// getters and setters --------------------------------------

const char * BleUartService::getServiceUiid() const{
  return serviceUiid;
}

void BleUartService::setOnData(RcvCallback callback){
    this->rcvCallback = callback;
}

// initialization ------------------------------------------

void BleUartService::begin(BLEServer * server, const CharacteristicsList & extraCharacteristics){
    createService(server, extraCharacteristics);
    this->service->start();

    BLEAdvertising * advertising = server->getAdvertising();
    advertising->addServiceUUID(this->service->getUUID());
}

BLEService * BleUartService::createService(BLEServer * server, const CharacteristicsList & extraCharacteristics ){
    this->service = server->createService(this->serviceUiid);

    registerCharacteristics(service);

    for(auto & charac : extraCharacteristics){
      charac.createCharacteristic(service);
    }

    return service;
}

void BleUartService::registerCharacteristics(BLEService * service){
    auto* rxCharacteristic = service->createCharacteristic(
        rxUiid,
        PROPERTY_WRITE_NR
    );
    rxCharacteristic->setCallbacks(this);

    this->txCharacteristic = service->createCharacteristic(
        txUiid,
        PROPERTY_READ | PROPERTY_NOTIFY
    );
}

size_t BleUartService::send(const uint8_t *buffer, size_t size, bool sendAll){
    auto sentBytes = std::min(size, BLE_MAX_WRITE_SIZE);

    txCharacteristic->setValue((uint8_t *)buffer, sentBytes);
    txCharacteristic->notify();

    if(sendAll && sentBytes < size){
        auto remainingData = buffer + sentBytes;
        auto remainingSize = size - sentBytes;

        sentBytes += send(remainingData, remainingSize, sendAll);
    }

    return sentBytes;
}

//calbacks --------------------------------------------------

void BleUartService::onWrite(BLECharacteristic *characteristic) {
    if(rcvCallback && characteristic != nullptr){
        rcvCallback(characteristic->getValue());
    }
}