#include "BleStream.h"

#ifdef USE_NIM_BLE_ARDUINO_LIB
#define PROPERTY_READ       NIMBLE_PROPERTY::READ
#define PROPERTY_WRITE      NIMBLE_PROPERTY::WRITE
#define PROPERTY_WRITE_NR   NIMBLE_PROPERTY::WRITE_NR
#define PROPERTY_INDICATE   NIMBLE_PROPERTY::INDICATE
#define PROPERTY_NOTIFY     NIMBLE_PROPERTY::NOTIFY
#else
#define PROPERTY_READ       BLECharacteristic::PROPERTY_READ
#define PROPERTY_WRITE      BLECharacteristic::PROPERTY_WRITE
#define PROPERTY_WRITE_NR   BLECharacteristic::PROPERTY_WRITE_NR
#define PROPERTY_INDICATE   BLECharacteristic::PROPERTY_INDICATE
#define PROPERTY_NOTIFY     BLECharacteristic::PROPERTY_NOTIFY
#endif

BleStream::~BleStream(){
    flush();

#ifndef USE_NIM_BLE_ARDUINO_LIB
    if(pServer != nullptr){
        delete pServer;
        pServer = nullptr;
    }
#endif
}


void BleStream::initDevice(const char * deviceName){
    // Create the BLE Device
    BLEDevice::init(deviceName);
}

void BleStream::begin(){
    // Create the BLE Server
    pServer = BLEDevice::createServer();
    // pServer->setCallbacks(this);

    begin(pServer);

    // Start advertising
    // pServer->getAdvertising()->start();
    pServer->startAdvertising();
}


void BleStream::begin(BLEServer * server){
    this->pServer = server;

    this->uart.setOnData([this](const std::string & data){
        this->receiveData(data);
    });
    this->uart.begin(server);


//     // Create the BLE Service
//     BLEService *pService = pServer->createService(Uuids::serviceId);

//     // Create a BLE Characteristic
//     pTxCharacteristic = pService->createCharacteristic(
//                                         Uuids::txCharacteristic,
//                                         PROPERTY_NOTIFY
//                                     );

// #ifndef USE_NIM_BLE_ARDUINO_LIB
//     //In NimBLE, a 2902 descriptor will be created automatically if
//     //notifications or indications are enabled on a characteristic.
//     pTxCharacteristic->addDescriptor(new BLE2902());
// #endif

//     BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
//                                         Uuids::rxCharacteristic,
//                                         PROPERTY_WRITE
//                                     );

//     pRxCharacteristic->setCallbacks(this);

//     // Start the service
//     pService->start();

//     // Start advertising
//     // pServer->getAdvertising()->start();
//     pServer->startAdvertising();
}


bool BleStream::isConnected() const{
    if(this->pServer == nullptr){
        return false;
    }
    return pServer->getConnectedCount() > 0;
}


// --------------------------- callbacks ---------------------------------------

void BleStream::onWrite(BLECharacteristic *pCharacteristic) {
    receiveData(pCharacteristic->getValue());
}

void BleStream::receiveData(const std::string & data){
    rcvBuffer.append(data);
}

// void BleStream::onConnect(BLEServer* pServer) {
//     deviceConnected = true;
// };

// void BleStream::onDisconnect(BLEServer* pServer) {
//     deviceConnected = false;

//     //restart advertising
//     if(pServer){
//         pServer->startAdvertising();
//     }
// }

// --------------------------- read --------------------------------------------
int BleStream::available() {
    return rcvBuffer.size();
}
int BleStream::available() const{
    return rcvBuffer.size();
}

int BleStream::peek() {
    if(!available()) return -1;

    return rcvBuffer.front();
}

int BleStream::read() {
    auto b = peek();
    if(!rcvBuffer.empty()){
        rcvBuffer.erase(0, 1); //remove first char
    }

    return b;
}


size_t BleStream::readBytes (uint8_t* buffer, size_t len) {
    auto readedNum = readAvailable(buffer, len);
    if(readedNum < len){
        auto newPos = buffer + readedNum;
        auto remaining = len - readedNum;

        //read with timeout, the remaining bytes
        return readedNum + Stream::readBytes(newPos, remaining);
    }

    return readedNum;
}

int BleStream::readAvailable (uint8_t* buffer, int len){
    auto copiedCount = rcvBuffer.copy((char *) buffer, len);
    rcvBuffer.erase(0, copiedCount); //remove copied bytes

    return copiedCount;
}


// --------------------------- write -----------------------------------------

size_t BleStream::write(uint8_t byte){
    return write(&byte, 1);
}

size_t BleStream::write(const uint8_t *buffer, size_t size){
    //TO-DO: handle sizes too big

    sndBuffer.append((const char *) buffer, size);

    if(sndBuffer.length() >= bufferingLimit){
        Serial.println("reach bufferingLimit(" + String(bufferingLimit) + "), flushing.");
        flush();
    }

    return size;
}

void BleStream::flush(){
    if(sndBuffer.empty()){
        return;
    }
    send((const uint8_t *)sndBuffer.data(), sndBuffer.length());

    sndBuffer.clear();
}

size_t BleStream::send(const uint8_t *buffer, size_t size){
    return uart.send(buffer, size);
}

// size_t BleStream::send(const uint8_t *buffer, size_t size){
//     auto sentBytes = std::min(size, BLE_MAX_WRITE_SIZE);

//     Serial.println("Sending " + String(sentBytes) + " of " + size);

//     pTxCharacteristic->setValue((uint8_t *)buffer, sentBytes);
//     pTxCharacteristic->notify();

//     if(sentBytes < size){
//         auto remainingData = buffer + sentBytes;
//         auto remainingSize = size - sentBytes;

//         sentBytes += send(remainingData, remainingSize);
//     }

//     return sentBytes;
// }