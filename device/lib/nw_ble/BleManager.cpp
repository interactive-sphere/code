#include "BleManager.h"

bool BleManager::isConnected() const{
    return server()->getConnectedCount() > 0;
}

void BleManager::begin(const char * deviceName){
    BLEDevice::init(deviceName);
}
void BleManager::startAdvertising(bool keepAdvertising){
    this->keepAdvertising = keepAdvertising;

    if(keepAdvertising){
        server()->setCallbacks(this);
    }

    beginAdvertising();
}

void BleManager::beginAdvertising(){
    server()->startAdvertising();
}

void BleManager::stopAdvertising(){
    this->keepAdvertising = false;

    server()->getAdvertising()->stop();
}

void BleManager::onDisconnect(BLEServer* pServer){
    if(this->keepAdvertising){
        beginAdvertising();
    }
}

BLEServer * BleManager::server() const{
    if(_server == nullptr){
        _server = BLEDevice::createServer();
    }
    return _server;
}