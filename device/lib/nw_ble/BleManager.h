#pragma once

#include "BleStack.h"

class BleManager: public BLEServerCallbacks{
public:
    bool isConnected() const;

    void begin(const char * deviceName);
    void startAdvertising(bool keepAdvertising = true);
    void stopAdvertising();

public:
    BLEServer * server() const;

protected:
    void beginAdvertising();

protected: //callbacks
    void onDisconnect(BLEServer* pServer) override;

protected:
    bool keepAdvertising = false;
    mutable BLEServer * _server = nullptr;
};