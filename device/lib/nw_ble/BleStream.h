#pragma once

#include "BleUartService.h"

#ifndef BLE_STREAM__BUF__LIMIT
#define BLE_STREAM__BUF__LIMIT 240
#endif


class BleStream:    public Stream,
                    public BLECharacteristicCallbacks
{
public:
    class Uuids{
    public:
        static constexpr const char * serviceId =
            "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
        static constexpr const char * rxCharacteristic =
            "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
        static constexpr const char * txCharacteristic =
            "6E400003-B5A3-F393-E0A9-E50E24DCCA9E";
    };
public:
    BleStream(size_t bufferingLimit=BLE_STREAM__BUF__LIMIT)
        : bufferingLimit(bufferingLimit)
    {}

    ~BleStream();

public:
    virtual void initDevice(const char * deviceName);
    virtual void begin();
    virtual void begin(BLEServer * server);
    bool isConnected() const;

    virtual operator bool() { return isConnected(); }
    virtual operator bool() const { return isConnected(); }

public: //write
    size_t write(uint8_t byte) override;
    size_t write(const uint8_t *buffer, size_t size) override;
    void flush() override;

protected:
    virtual size_t send(const uint8_t * buffer, size_t size);

public: //read
    int available() override;
    virtual int available() const;
    int peek() override;
    int read() override;
    size_t readBytes (uint8_t* buffer, size_t len) override;

protected:
    /**
        read what can be read, immediate exit on unavailable data.
    **/
    virtual int readAvailable (uint8_t* buffer, int len);

    virtual void receiveData(const std::string & data);

protected: //calbacks
    void onWrite(BLECharacteristic *pCharacteristic);
    // void onConnect(BLEServer* pServer) override;
    // void onDisconnect(BLEServer* pServer) override;


public:
    BleUartService uart;
protected:
    size_t bufferingLimit;


    BLEServer * pServer = nullptr;
    // BLECharacteristic * pTxCharacteristic = nullptr;
    bool deviceConnected = false;

    std::string rcvBuffer, sndBuffer;
};