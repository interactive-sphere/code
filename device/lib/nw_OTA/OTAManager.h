#ifndef OTAMANAGER_H
#define OTAMANAGER_H

#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

// Default parameters
//
#ifndef OTA_PASS
#define OTA_PASS "password123"
#endif
#ifndef MDNS_HOST
#define MDNS_HOST "esp32"
#endif


class OtaManager{
public:
  bool begin(String OTA_password, String mdns_hostname);
  bool begin(
    const char * OTA_password=OTA_PASS,
    const char * mdns_hostname=MDNS_HOST);

  void update() {
    ArduinoOTA.handle();
  }

protected:
  void setupOTAEvents();
};

#endif // OTAMANAGER_H
