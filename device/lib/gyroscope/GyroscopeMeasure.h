#ifndef GYROSCOPE_MEASURE_H
#define GYROSCOPE_MEASURE_H

#include <cstdint>

class GyroscopeMeasure {
public:

  uint8_t * buffer(){ return fifoBuffer; }
  const
  uint8_t * buffer() const { return fifoBuffer; }

  std::size_t size() const{
    return _size;
  }

  void size(std::size_t s){
    this->_size = s;
  }

protected:
  uint8_t fifoBuffer[64];
  std::size_t _size;
};

#endif
