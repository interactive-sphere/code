#ifndef TEAPOT_PRINTER_H
#define TEAPOT_PRINTER_H

#include "GyroscopeMeasure.h"

#include "ArduinoFramework.h"

class TeapotPrinter {
public:
    TeapotPrinter();

    void outputTeapot(const GyroscopeMeasure & measure);
    void outputTeapot(const GyroscopeMeasure & measure, Print & out);

protected:
    uint8_t teapotPacketCounter;
};

#endif
