#include "BaseGyroscopeServices.h"

#include "BoardConfig.h"
#include "BoardPinout.h"

#include <ArduinoFramework.h>

#include "3dmath/Vector6D.hpp"

#include "log.h"

using namespace std::placeholders;


BaseGyroscopeServices::BaseGyroscopeServices (Gyroscope* gyro)
    : gyroscope(gyro)
    , currentFormat(OrientationFormat::Quaternion)
    , listener({})
    , notifyInterval(0)
{}


bool BaseGyroscopeServices::ready() const{
    return gyroscope && gyroscope->ready();
}
void BaseGyroscopeServices::begin(){
    if(!gyroscope) return;

    gyroscope->begin();
}
void BaseGyroscopeServices::update(){
    if(!gyroscope) return;

    gyroscope->update();
}

void BaseGyroscopeServices::calibrate(){
    this->calibrate({DEFAULT_GRAVITY_X, DEFAULT_GRAVITY_Y, DEFAULT_GRAVITY_Z});
}

void BaseGyroscopeServices::calibrate(const Vector3D<float> & gravity){
    if(!gyroscope) return;

    this->gyroscope->calibrate(gravity);
}


void BaseGyroscopeServices::
    listenOrientation(OrientationListener listener, ListenOrientationConfig config)
{
    if(!gyroscope) return;

    this->listener = listener;
    this->currentFormat = config.format;
    this->notifyInterval.setTime(std::max(config.timeInterval, GYROSCOPE_MINIMAL_INTERVAL));

    this->gyroscope->listen(std::bind(&BaseGyroscopeServices::onMeasure, this, _1));
}

void BaseGyroscopeServices::removeListener(){
    this->listener = {};
    this->gyroscope->listen({});
}

void BaseGyroscopeServices::onMeasure(const GyroscopeMeasure & m){
    if (!listener || !notifyInterval.completed()){
        return;
    }

    receiveMeasure(m, listener, currentFormat);

    notifyInterval.reset();
}

void BaseGyroscopeServices::receiveMeasure(
    const GyroscopeMeasure & m,
    OrientationListener listener, OrientationFormat format)
{
    if (!listener){
        return;
    }

    switch (format){
        case OrientationFormat::Quaternion:{
            Quat q;
            listener(gyroscope->getQuaternion(m, q));
            break;
        }
        case OrientationFormat::Euler:{
            Vector3D<float> vec3;
            listener(gyroscope->getEuler(m, vec3));
            break;
        }
        case OrientationFormat::YawPitchRow:{
            Vector3D<float> vec3;
            listener(gyroscope->getYawPitchRow(m, vec3));
            break;
        }
        case OrientationFormat::AccelGyro:{
            Vector6D<float> vec6;
            Vector3D<int16_t> accel, gyro;
            gyroscope->getRealAccel(m, accel);
            gyroscope->getRawGyro(m, gyro);
            vec6.set(accel.data(), 3);
            vec6.set(gyro.data(), 3, 3);

            listener(vec6);
            break;
        }
    }

}
