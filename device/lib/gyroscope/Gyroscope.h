#pragma once

#include "ArduinoFramework.h"


#include <functional>

#include "GyroscopeMeasure.h"
#include "3dmath.hpp"

using GyroscopeListener = std::function<void(const GyroscopeMeasure &)>;

class Gyroscope{

public:
    virtual ~Gyroscope() = default;

    virtual bool update()=0;

    virtual bool begin()=0;
    virtual bool ready() const=0;

    virtual void calibrate(int rounds=14)=0;
    virtual void calibrate(const Vector3D<float> & gravity, int rounds=14)=0;

    virtual void listen(GyroscopeListener listener)=0;
    virtual void stopListening()=0;

public: //read measure
    virtual Quat            & getQuaternion(const GyroscopeMeasure & measure, Quat & out)=0;
    virtual Vector3D<float> & getEuler(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true)=0;
    virtual Vector3D<float> & getYawPitchRow(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true)=0;

    /** real acceleration, adjusted to remove gravity */
    virtual Vector3D<int16_t> & getRealAccel(const GyroscopeMeasure & measure,
                                     Vector3D<int16_t>      & out)=0;

    virtual Vector3D<int16_t> & getWorldAccel(const GyroscopeMeasure & measure,
                                      Vector3D<int16_t>      & out)=0;

public: //raw
    virtual Vector3D<int16_t> & getRawAccel(const GyroscopeMeasure & measure,
                                    Vector3D<int16_t>      & out)=0;

    virtual Vector3D<int16_t> & getRawGyro(const GyroscopeMeasure & measure,
                                   Vector3D<int16_t>      & out)=0;

    virtual void getRawAccelGyro(const GyroscopeMeasure & measure,
                                 Vector3D<int16_t>  & accelOut,
                                 Vector3D<int16_t>  & gyroOut)
    {
        this->getRawAccelGyro(measure, &accelOut, &gyroOut);
    }
    virtual void getRawAccelGyro(const GyroscopeMeasure & measure,
                                 Vector3D<int16_t>  * accelOut = nullptr,
                                 Vector3D<int16_t>  * gyroOut = nullptr)=0;
public:
    virtual void radiansToDegree(Vector3D<float> & vec)=0;
};
