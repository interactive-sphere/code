#pragma once

#include "OrientationServices.h"
#include "Gyroscope.h"
#include "TimeCounter.h"

class BaseGyroscopeServices: public OrientationServices {
public:
    BaseGyroscopeServices (Gyroscope * gyro);

    bool ready() const;
    void begin() override;
    void update() override;

    void calibrate() override;
    void calibrate(const Vector3D<float> & gravity) override;

    void listenOrientation(OrientationListener listener, ListenOrientationConfig config) override;
    void removeListener() override;

protected:
    void onMeasure(const GyroscopeMeasure & m);
    void receiveMeasure(const GyroscopeMeasure & m, OrientationListener listener, OrientationFormat format);

protected:
    Gyroscope * gyroscope;

    OrientationFormat currentFormat;
    OrientationListener listener;
    TimeCounter notifyInterval;
};
