#include "SphereDevice.h"

#include "BoardSensors.h"
#include "BoardPinout.h"
#include "BoardConfig.h"


SphereDevice::SphereDevice(Stream * stream)
    : gyroscopeServices()
    , pressureServices(std::move(boardSensors()))
    , buzzer(BUZZER_PIN, BUZZER_CHANNEL)
    , vibrationMotor(VIBRATION_PIN)
    , ledServices(LEDRGB_COUNT, LEDRGB_PIN)
    , sphereServices({
        &ledServices,
        &vibrationMotor,
        &buzzer,
        &gyroscopeServices,
        &pressureServices
    })
    , sphereApi(stream, &sphereServices)
{

}

void SphereDevice::begin() {
    sphereServices.begin();

    pressureServices.stateNotifier(&ledServices.stateNotifier());
    pressureServices.calibrateFor(calibrationTime);

    sphereApi.setup();
}

void SphereDevice::setConnected(bool connected){
    if(this->connected == connected) return;

    this->connected = connected;
    if(!connected){//Disconnected
        sphereApi.disconnected();
    }
}

void SphereDevice::update() {
    sphereServices.update();

    if (connected){
        sphereApi.update();
    }
}