#pragma once

#include <ArduinoFramework.h>

#include <SPI.h> //required by NeoPixelsBus

#include "CapPressureServices.h"
#include "Mpu6050Services.h"
#include "BuzzerSoundServices.h"
#include "VibrationMotor.h"
#include "NeoPixelLedServices.h"

#include "SphereServices.h"
#include "SphereApi.h"


class SphereDevice{
public:
    SphereDevice(Stream * stream);

    void begin();
    void setConnected(bool connected);
    void update();

protected:
    Mpu6050Services gyroscopeServices;
    CapPressureServices pressureServices;
    BuzzerSoundServices buzzer;
    VibrationMotor vibrationMotor;
    NeoPixelLedServices<> ledServices;

    SphereServices sphereServices;
    api::SphereApi sphereApi;

    long calibrationTime = 3000;
    bool connected=false;
};