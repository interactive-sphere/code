#include "VibrationMotor.h"

#include "ArduinoFramework.h"

VibrationMotor::VibrationMotor(int motorPin)
  : motorPin(motorPin)
  , vibrationEnabled(false)
{}

void VibrationMotor::begin(){
    pinMode(motorPin, OUTPUT);

    stop();
}

void VibrationMotor::update(){
  if (vibrationEnabled && vibrationCounter.completed()) {
    stop();
  }
}

void VibrationMotor::vibrate(int durationMillis){
  vibrationCounter.setTime(durationMillis);
  vibrationCounter.reset();
  vibrationEnabled = true;

  digitalWrite(motorPin, HIGH);
}

bool VibrationMotor::isVibrating(){
  return vibrationEnabled;
}

void VibrationMotor::stop(){
  digitalWrite(motorPin, LOW);

  vibrationEnabled = false;
}
