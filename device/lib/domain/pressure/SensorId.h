#ifndef PRESSURE_SENSOR_ID_H
#define PRESSURE_SENSOR_ID_H

namespace pressure {
    using SensorId = int;
} /* pressure */

#endif
