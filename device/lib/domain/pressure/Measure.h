#ifndef PRESSURE_MEASURE_H
#define PRESSURE_MEASURE_H

#include "SensorId.h"


#include "ArduinoFramework.h"

namespace pressure {

    class Measure {
    public:
        static Measure invalidMeasure(){
            return {-1, 0, 0};
        }
    public:
        Measure() : Measure(-1, 0, 0)
        {}
        Measure (SensorId sensorId, float percent, long measure)
            : Measure(sensorId, percent, measure, measure)
        {}
        Measure (SensorId sensorId, float percent, long measure, long rawValue)
            : sensorId(sensorId)
            , percentValue(percent)
            , measure(measure)
            , rawValue(rawValue)
        {}
        Measure (SensorId sensorId, const Measure & other)
            : Measure(sensorId,
                      other.percentValue, other.measure, other.rawValue)
        {}

    public:
        void setValues(float percentValue, long measure, long rawValue){
            this->percentValue = percentValue;
            this->measure = measure;
            this->rawValue = rawValue;
        }

    public:
        String toString() const{
            return String("{")
                + "\t" "sensorId:" + sensorId + ","
                + "\t" "percentValue:" + percentValue + ","
                + "\t" "measure:" + measure + ","
                + "\t" "rawValue:" + rawValue
                + String("}");
        }

    public:
        SensorId sensorId;
        float percentValue;
        long measure;
        long rawValue;
    };

} /* pressure */

#endif
