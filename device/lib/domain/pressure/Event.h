#ifndef PRESSURE_EVENT_H
#define PRESSURE_EVENT_H

#include "SensorId.h"
#include "EventType.h"
#include "Measure.h"

#include "ArduinoFramework.h"


namespace pressure {

    class Event: public Measure {
    public:
        Event (SensorId sensorId, EventType evtType, const Measure & measure)
            : Event(sensorId, evtType,
                    measure.percentValue, measure.measure, measure.rawValue)
        {}

        Event (SensorId sensorId, EventType evtType,
               float percent, long measure, long rawValue)
            : Measure(sensorId, percent, measure, rawValue)
            , evtType(evtType)
        {}

    public:
      String toString() const{
          return String("{ id: ") + this->sensorId
                + ", type: "    + this->evtType.c_str()
                + ", percent: " + this->percentValue
                + ", measure: " + this->measure
                + ", raw: " + this->rawValue
                + "}";
      }

    public:
        EventType evtType;
    };

} /* pressure */

#endif
