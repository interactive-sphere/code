#ifndef EVENT_LISTENER_H
#define EVENT_LISTENER_H

#include <functional>

#include "Event.h"

namespace pressure{
    using EventListener = std::function<void(const pressure::Event &)>;
}

#endif
