#ifndef PRESSURE_SENSOR_DESCRIPTIONS_H
#define PRESSURE_SENSOR_DESCRIPTIONS_H

#include "SensorDescription.h"
#include <unordered_map>

using SensorDescriptions = std::unordered_map<pressure::SensorId,
                                              pressure::SensorDescription>;


#endif
