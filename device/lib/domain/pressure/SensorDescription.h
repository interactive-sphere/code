#ifndef SENSOR_DESCRIPTION_H
#define SENSOR_DESCRIPTION_H

#include "SensorId.h"

#include <ArduinoFramework.h>

namespace pressure {
    class SensorDescription{
    public:
        SensorDescription(SensorId id=0, String name={}, String description={})
            : _id(id)
            , _name(std::move(name))
            , _description(std::move(description))
        {}

    public:
        inline SensorId  id()   const { return _id;}

        inline
        const  String   & name() const { return _name;}
        inline String   & name()       { return _name;}

        inline
        const  String   & description() const { return _description;}
        inline String   & description()       { return _description;}

    protected:
        SensorId _id;
        String _name;
        String _description;
    };
} /* pressure */

#endif
