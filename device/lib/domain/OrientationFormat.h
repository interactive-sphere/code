#ifndef ORIENTATION_FORMAT_H
#define ORIENTATION_FORMAT_H

class OrientationFormat{
public:
    enum Value{
        Quaternion, Euler, YawPitchRow,
        AccelGyro
    };

    OrientationFormat() = default;
    constexpr OrientationFormat(Value value) : value(value) { }

    operator Value() const { return value; }

public:
    const char * c_str() const{
        switch (value){
        case Quaternion:   return "Quaternion";
        case Euler:        return "Euler";
        case YawPitchRow:  return "YawPitchRow";
        case AccelGyro:    return "AccelGyro";
        default:           return "Unknown format";
        }
    }

protected:
    Value value;
};

#endif
