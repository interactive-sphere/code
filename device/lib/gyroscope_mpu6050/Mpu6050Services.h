#pragma once

#include "OrientationServices.h"
#include "Gyroscope.h"
#include "TimeCounter.h"

#include "BaseGyroscopeServices.h"

#include "GyroscopeMpu6050.h"

class Mpu6050Services: public BaseGyroscopeServices {
public:
    Mpu6050Services ();
    Mpu6050Services (int sclPin, int sdaPin, int interruptPin);

public:
    Gyroscope & gyro();

protected:
    GyroscopeMpu6050 _gyro;
};
