#pragma once

#include "ArduinoFramework.h"

#include "MPU6050.h"

#include <functional>

#include "Gyroscope.h"
#include "GyroscopeMeasure.h"
#include "3dmath.hpp"

#if defined(GYROSCOPE_NO_IRAM) || !defined(IRAM_ATTR)
// No IRAM
#define GYRO_IRAM
#else
//enable IRAM - avoid problem of acessing files while interrupt occur
#define GYRO_IRAM IRAM_ATTR
#endif

using GyroscopeListener = std::function<void(const GyroscopeMeasure &)>;

class GyroscopeMpu6050: public Gyroscope{
protected:
    static volatile bool mpuInterrupt;
    static void GYRO_IRAM dmpDataReady();

public:
    GyroscopeMpu6050(int sclPin, int sdaPin, int interruptPin);
    virtual ~GyroscopeMpu6050() = default;

    virtual bool update();

    virtual bool begin();
    virtual bool ready() const;

    virtual void calibrate(int rounds=14);
    virtual void calibrate(const Vector3D<float> & gravity, int rounds=14);

    virtual void listen(GyroscopeListener listener);
    virtual void stopListening();

public: //read measure
    Quat            & getQuaternion(const GyroscopeMeasure & measure, Quat & out);
    Vector3D<float> & getEuler(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true);
    Vector3D<float> & getYawPitchRow(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true);

    /** real acceleration, adjusted to remove gravity */
    Vector3D<int16_t> & getRealAccel(const GyroscopeMeasure & measure,
                                     Vector3D<int16_t>      & out);

    Vector3D<int16_t> & getWorldAccel(const GyroscopeMeasure & measure,
                                      Vector3D<int16_t>      & out);

public: //raw
    virtual Vector3D<int16_t> & getRawAccel(const GyroscopeMeasure & measure,
                                    Vector3D<int16_t>      & out);

    virtual Vector3D<int16_t> & getRawGyro(const GyroscopeMeasure & measure,
                                   Vector3D<int16_t>      & out);

    virtual void getRawAccelGyro(const GyroscopeMeasure & measure,
                                 Vector3D<int16_t>  & accelOut,
                                 Vector3D<int16_t>  & gyroOut);
    virtual void getRawAccelGyro(const GyroscopeMeasure & measure,
                                 Vector3D<int16_t>  * accelOut = nullptr,
                                 Vector3D<int16_t>  * gyroOut = nullptr);
public:
    void radiansToDegree(Vector3D<float> & vec);

public:
    MPU6050 & getMPU();

protected:
    bool setupPins();
    bool initDMP();
    void setupDefaultOffsets();
    void enableGyro();

    void readMeasures();
    void notifyMeasure(const GyroscopeMeasure & measure);

protected:
    int sclPin;
    int sdaPin;
    int interruptPin;

    // MPU control/status vars
    bool dmpReady;  // set true if DMP init was successful
    uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
    uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
    uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
    uint16_t fifoCount;     // count of all bytes currently in FIFO

    GyroscopeMeasure measure;
    GyroscopeListener listener;

    MPU6050 mpu;
};

