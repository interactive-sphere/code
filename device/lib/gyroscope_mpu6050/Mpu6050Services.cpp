#include "Mpu6050Services.h"

#include "BoardPinout.h"
#include "BoardConfig.h"

using namespace std::placeholders;

Mpu6050Services::Mpu6050Services()
    : Mpu6050Services(GYRO_SCL_PIN, GYRO_SDA_PIN, GYRO_INTERRUPT_PIN)
{}

Mpu6050Services::Mpu6050Services (int sclPin, int sdaPin, int interruptPin)
    : BaseGyroscopeServices(&_gyro)
    , _gyro(sclPin, sdaPin, interruptPin)
{}

Gyroscope & Mpu6050Services::gyro(){
    return _gyro;
}