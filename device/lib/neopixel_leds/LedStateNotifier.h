#pragma once

#include "StateNotifier.h"

#include "FadeAnimation.h"


class LedStateNotifier: public StateNotifier{
protected:
    FadeAnimation fadeAnimation;
    RgbColor calibrationColor;
public:
    LedStateNotifier(LedAnimator * animator)
        : fadeAnimation(animator)
        , calibrationColor(RgbColor(216, 131, 4))
    {}

    void onStartCalibration() override{
        fadeAnimation.start(calibrationColor, 1000, 0.8f);
    }
    void onStopCalibration() override{
        fadeAnimation.stop();
    }
};