#include "FadeAnimation.h"

const RgbColor FadeAnimation::black = RgbColor(0);


FadeAnimation::FadeAnimation(LedAnimator * animator)
    : animator(animator)
{}

void FadeAnimation::start(
    const RgbColor & fadeColor, Millis fadeTime, float transitionPercent)
{
    this->fadeColor = fadeColor;
    this->fadeTime = fadeTime;
    this->transitionPercent = transitionPercent;

    startAnimation(fadeColor);
}

void FadeAnimation::stop(){
    if(animator){
        setPixelsTo(black);
        animator->stop();
    }
}


uint16_t FadeAnimation::pixelCount() const{
    return animator ? animator->numPixels() : 0;
}

void FadeAnimation::changeColor(const RgbColor & newColor){
    fadeColor = newColor;

    if(fadeToColor){
        flipFade();
    }
}


void FadeAnimation::changeFadeTime(Millis fadeTime){
    this->fadeTime = fadeTime;

    flipFade();
}

bool FadeAnimation::isAnimating() const{
    return animator != nullptr && animator->isAnimating();
}


void FadeAnimation::flipFade(){
    const auto & color = (fadeToColor ? fadeColor : black);
    startAnimation(color);

    // toggle to the next effect state
    fadeToColor = !fadeToColor;
}

void FadeAnimation::startAnimation(const RgbColor & target)
{
    if(!animator) return;

    animationState.StartingColor = animator->getPixelColor(0);
    animationState.EndingColor = target;

    animator->startAnimation(this->fadeTime, [this](const AnimationParam& param){
        this->blendAnimUpdate(param);
    });
}

// simple blend function
void FadeAnimation::blendAnimUpdate(const AnimationParam& param)
{
    float progress = mapFloat(param.progress,
                            0, transitionPercent, 0, 1.0f);

    if(progress > 1.0f){
        progress = 1.0f;
    }

    updateColors(progress);

    if(param.progress >= 1.0f){
        flipFade();
    }
}

void FadeAnimation::updateColors(float progress){
    // progress will start at 0.0 and end at 1.0
    // we use the blend function on the RgbColor to mix
    // color based on the progress given to us in the animation
    RgbColor updatedColor = RgbColor::LinearBlend(
        animationState.StartingColor,
        animationState.EndingColor,
        progress);

    setPixelsTo(updatedColor);
}


void FadeAnimation::setPixelsTo(const RgbColor & color){
    // apply the color to the strip
    for (uint16_t pixel = 0; pixel < this->pixelCount(); pixel++)
    {
        animator->setPixelColor(pixel, color);
    }
}

