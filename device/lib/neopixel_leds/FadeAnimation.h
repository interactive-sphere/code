#pragma once

#include "LedAnimator.h"


class FadeAnimation{
public:
    using Millis = long;

    // what is stored for state is specific to the need, in this case, the colors.
    // basically what ever you need inside the animation update function
    struct FadeState
    {
        RgbColor StartingColor;
        RgbColor EndingColor;
    };

protected:
    static const RgbColor black;

protected:
    boolean fadeToColor = true;  // store effect state
    RgbColor fadeColor;
    Millis fadeTime = 2000;
    float transitionPercent = 0.65f;

    FadeState animationState;

    LedAnimator * animator = nullptr;

public:
    FadeAnimation(LedAnimator * animator);

    void start(const RgbColor & fadeColor, Millis fadeTime=2000, float transitionPercent=0.65f);
    void stop();

    void changeColor(const RgbColor & newColor);
    void changeFadeTime(Millis fadeTime);

protected:
    bool isAnimating() const;


protected:
    uint16_t pixelCount() const;
    void flipFade();

protected:
    void startAnimation(const RgbColor & target);

    // simple blend function
    void blendAnimUpdate(const AnimationParam& param);

    void updateColors(float progress);
    void setPixelsTo(const RgbColor & color);

    float mapFloat(
        float x, float in_min, float in_max, float out_min, float out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
};