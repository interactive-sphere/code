#pragma once


#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>


class LedAnimator{
public:
    virtual ~LedAnimator(){}

    virtual bool isAnimating() const=0;

    virtual void startAnimation(
        uint16_t duration, AnimUpdateCallback animUpdate)=0;

    virtual void stop() = 0;

    virtual unsigned numPixels() const=0;
    virtual RgbColor getPixelColor(int pixelIndex)=0;
    virtual void setPixelColor(int pixelIndex, const RgbColor & color)=0;
};