#pragma once

#include "LedAnimator.h"

template<typename PixelBusT>
class NeoPixelLedAnimator: public LedAnimator{
protected:
    unsigned pixelCount;
    PixelBusT * strip;
    NeoPixelAnimator animations; // NeoPixel animation management object

public:
    NeoPixelLedAnimator(PixelBusT * strip, unsigned pixelCount)
        : pixelCount(pixelCount)
        , strip(strip)
        , animations(1)
    {

    }

    void update(){
        if (isAnimating())
        {
            // the normal loop just needs these two to run the active animations
            animations.UpdateAnimations();
            show();
        }
    }

    bool isAnimating() const override{
        return animations.IsAnimating();
    }

    void startAnimation( uint16_t duration, AnimUpdateCallback animUpdate){
        animations.StartAnimation(0, duration, animUpdate);
    }

    void stop(){
        animations.StopAll();
        show();
    }


    unsigned numPixels() const{
        return pixelCount;
    }


    RgbColor getPixelColor(int pixelIndex){
        return strip->GetPixelColor(pixelIndex);
    }

    void setPixelColor(int pixelIndex, const RgbColor & color){
        strip->SetPixelColor(pixelIndex, color);
    }

protected:
    void show(){
        strip->Show();
    }
};