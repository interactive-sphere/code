#pragma once
#ifndef NW__OTA_BLE_MANAGER_H
#define NW__OTA_BLE_MANAGER_H

#include <ArduinoBleOTA.h>
#include <BleOtaMultiservice.h>

class OtaBleManager{
public:
    void init(const char * deviceName);
    bool begin();
    bool begin(const char * deviceName);
    void update();

protected:
    bool startAdvertising();
};


#endif
