#include "OtaBleManager.h"


void OtaBleManager::init(const char * deviceName){
    initBle(deviceName);
    BLEDevice::setPower(ESP_PWR_LVL_P7);
}

bool OtaBleManager::begin(){
    return ArduinoBleOTA.begin(InternalStorage)
        && startAdvertising();
}

bool OtaBleManager::startAdvertising(){
    auto * server = BLEDevice::createServer();
    auto* advertising = server->getAdvertising();
    advertising->setScanResponse(true);
    advertising->setMinPreferred(0x06); // functions that help with iPhone connections issue
    advertising->setMaxPreferred(0x12);

    return advertising->start();
}

bool OtaBleManager::begin(const char * deviceName){
    return ArduinoBleOTA.begin(deviceName, InternalStorage);
}

void OtaBleManager::update(){
#if defined(BLE_PULL_REQUIRED)
    BLE.poll();
#endif
    ArduinoBleOTA.pull();
}
