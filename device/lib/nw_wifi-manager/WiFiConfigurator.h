#ifndef WIFI_CONFIGURATOR_H
#define WIFI_CONFIGURATOR_H

#include <WiFiManager.h>

class WiFiConfigurator{
public:
    WiFiConfigurator();

public:
    void update();

public:
    bool autoConnect(const char * ap_ssid=nullptr, const char * ap_pass=nullptr);

    void resetSettings();
    // TO-DO: expand interface

protected:
    WiFiManager manager;
};

#endif
