#include "WiFiConfigurator.h"

WiFiConfigurator::WiFiConfigurator()
{
    this->manager.setConfigPortalBlocking(false);
    this->manager.setConfigPortalTimeout(60);
}


void WiFiConfigurator::update(){
    this->manager.process();
}

bool WiFiConfigurator::autoConnect(const char * ap_ssid, const char * ap_pass)
{
    String ssid = (ap_ssid)? String(ap_ssid) : this->manager.getDefaultAPName();

    return this->manager.autoConnect(ssid.c_str(), ap_pass);
}


void WiFiConfigurator::resetSettings(){
    this->manager.resetSettings();
}
