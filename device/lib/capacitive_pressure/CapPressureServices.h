#ifndef CAP_PRESSURE_SERVICES_H
#define CAP_PRESSURE_SERVICES_H

#include "PressureServices.h"
#include "StateNotifier.h"

#include "PressureSensor.h"
#include "MuxReader.h"
#include "reader/CapSenseReader.h"

#include "CapPressureServicesConfig.h"

#include "TimeCounter.h"


class CapPressureServices : public PressureServices{
public:
    using Config = CapPressureServicesConfig;
    using MuxReaderArray = std::array<MuxReader, MULTIPLEXER_CHANNELS>;
    using SensorsArray = std::array<PressureSensor, MULTIPLEXER_CHANNELS>;
public:
    CapPressureServices (Config config);

    void begin() override;
    void update() override;

    void calibrateFor(unsigned long millis);
    void startCalibration();
    void stopCalibration();
    bool isCalibrating() const;

    const SensorDesc & getSensorDescription(pressure::SensorId sensor) override;
    const SensorDescriptions & getSensorDescriptions() override;
    void listenPressureEvents(PressureListener listener) override;
    void removeListener() override;

public:
    StateNotifier * stateNotifier(){ return _stateNotifier;}
    const
    StateNotifier * stateNotifier() const{ return _stateNotifier;}
    void stateNotifier(StateNotifier * notifier);

public:
    inline std::size_t sensorsCount() const{ return this->numSensors; }

    pressure::Measure readSensor(const pressure::SensorId sensorId);
    const
    pressure::Measure & getMeasure(const pressure::SensorId sensorId) const;

protected:
    void initSensors(Config & config);
    void configSensors();

    void onSensorEvent(const pressure::Event & evt);

protected:
    Multiplexer mux;
    CapSenseReader capReader;
    SensorDescriptions sensorDescriptions;
    std::size_t numSensors;
    MuxReaderArray muxReaders;
    SensorsArray pressureSensors;

    pressure::EventListener listener;

    TimeCounter calibrationTime;
    bool calibrating=false;

    StateNotifier * _stateNotifier = nullptr;
};

#endif
