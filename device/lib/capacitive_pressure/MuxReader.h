#ifndef MUX_READER_H
#define MUX_READER_H

#include "reader/Multiplexer.h"
#include "reader/CapReader.h"

class MuxReader: public CapReader {
public:
    MuxReader (int channel=0, Multiplexer * mux = nullptr, CapReader * reader = nullptr);

    long read() override;

public:
    int getChannel() const;
    Multiplexer * getMux() const;
    CapReader   * getReader() const;

    void setChannel(int channel);
    void setMux(Multiplexer * mux);
    void setReader(CapReader * reader);

protected:
    void select();

protected:
    int channel;
    Multiplexer * mux;
    CapReader * reader;
};

#endif
