#include "PressureSensor.h"

using namespace pressure;

PressureSensor::PressureSensor()
    : PressureSensor(0, nullptr, 0, 0)
{}


PressureSensor::PressureSensor (
    pressure::SensorId id,
    CapReader * reader,
    PressureSensor::Config conf
)
    : reader(reader, conf.medianWindowSize, conf.averageWindowSize,
                conf.boundaryFactor)
    , detector(id, conf.releaseThreshold, conf.pressThreshold, conf.debounceThreshold)
    , current(id, Measure::invalidMeasure())
{}

PressureSensor::PressureSensor (
    pressure::SensorId id,
    CapReader * reader,
    int medianWindowSize,
    int averageWindowSize,

    float releaseThreshold,
    float pressThreshold,
    int debounceThreshold,
    float boundaryFactor
)
    : PressureSensor(id, reader, {
        medianWindowSize, averageWindowSize,
        releaseThreshold, pressThreshold, debounceThreshold,
        boundaryFactor
    })
{
}

bool PressureSensor::update(){
    bool readerUpdated = reader.update();

    this->current.setValues(
      this->reader.relative(),
      this->reader.current(),
      this->reader.currentUnfiltered()
    );

    return readerUpdated
        && detector.update(this->current);
}

void PressureSensor::listenPressureEvents(EventListener listener){
    detector.onPressureEvent(listener);
}

void PressureSensor::clearEventsListener(){
    detector.clearEventsListener();
}


pressure::Measure PressureSensor::readMeasure(){
    update();

    return currentMeasure();
}

const pressure::Measure & PressureSensor::currentMeasure() const{
    return this->current;
}

void PressureSensor::startCalibration(){
    this->reader.startCalibration();
}
void PressureSensor::stopCalibration(){
    this->reader.stopCalibration();
}