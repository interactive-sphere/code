#include "PressureEventDetector.h"

#include "log.h"

namespace pressure{

EventDetector::EventDetector(
    SensorId id,
    float releaseThreshold, float pressThreshold,
    int debounceThreshold
    )
    : _id(id)
    , levelTrigger(releaseThreshold, pressThreshold)
    , debounce(debounceThreshold)
    , currentState(EventType::None)
{}

void EventDetector::reset(){
    debounce.reset();
    debounce.state(false);
    levelTrigger.state(false);
}

bool EventDetector::update(const Measure & measure){
    levelTrigger.update(measure.percentValue);

    if (!levelTrigger.isMiddle()){
        auto changed = debounce.update(levelTrigger.state());

        // log_info("(%f, %ld) - levelTrigger: %d, debounce: %d, changed: %d\n",
        //     measure.percentValue, measure.measure,
        //     levelTrigger.state(), debounce.state(), changed
        // );

        if(changed){
            changeState(debounce.state(), measure);
            return true;
        }
    }

    return false;
}

void EventDetector::changeState(bool pressed, const Measure &  measure){
    currentState = (pressed ? EventType::Press : EventType::Release);
    if (listener){
        listener(Event{id(), currentState, measure});
    }
}

void EventDetector::onPressureEvent(EventListener listener){
    this->listener = listener;
}

void EventDetector::clearEventsListener(){
    this->listener = nullptr;
}

EventType EventDetector::state() const{
    return currentState;
}


int EventDetector::debounceThreshold() const{
    return debounce.countThreshold();
}

int EventDetector::debounceThreshold(int threshold){
    return debounce.countThreshold(threshold);
}

float EventDetector::lowerLevelThreshold(float threshold){
    return levelTrigger.lowerThreshold(threshold);
}
float EventDetector::lowerLevelThreshold() const{
    return levelTrigger.lowerThreshold();
}

float EventDetector::upperLevelThreshold(float threshold){
    return levelTrigger.upperThreshold(threshold);
}
float EventDetector::upperLevelThreshold() const{
    return levelTrigger.upperThreshold();
}

}//namespace
