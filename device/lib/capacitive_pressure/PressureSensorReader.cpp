#include "PressureSensorReader.h"

#include <utility> //std::move


PressureSensorReader::PressureSensorReader (
    CapReader * reader,
    int medianWindowSize,
    int averageWindowSize,
    float boundaryFactor)
    :
    reader(reader)
    , runningMedian(medianWindowSize)
    , runningAverage(averageWindowSize)
    , baselineFilter(boundaryFactor)
{
    _pipeline.addFilter(&baselineFilter);
    _pipeline.addFilter(&runningMedian);
    _pipeline.addFilter(&runningAverage);
    _pipeline.addFilter(&rangeFilter);
}

PressureSensorReader::PressureSensorReader(const PressureSensorReader & other)
    : PressureSensorReader(other.reader, other.medianWindowSize(), other.averageWindowSize())
{
    *this = other;
}
PressureSensorReader::PressureSensorReader(PressureSensorReader && other)
    : PressureSensorReader(other.reader, other.medianWindowSize(), other.averageWindowSize())
{
    *this = std::move(other);
}

PressureSensorReader &
PressureSensorReader::operator=(const PressureSensorReader & other){
    this->reader = other.reader;
    this->runningMedian = other.runningMedian;
    this->runningAverage = other.runningAverage;
    this->baselineFilter = other.baselineFilter;
    this->rangeFilter = other.rangeFilter;

    this->lastReading = other.lastReading;

    /* FIXME: copy pipeline, without copying pointers to "other" local variables. */

    return *this;
}

PressureSensorReader &
PressureSensorReader::operator=(PressureSensorReader && other){
    this->reader         = other.reader;
    this->runningMedian  = std::move(other.runningMedian);
    this->runningAverage = std::move(other.runningAverage);
    this->baselineFilter = std::move(other.baselineFilter);
    this->rangeFilter    = std::move(other.rangeFilter);

    this->lastReading = other.lastReading;

    /* FIXME: copy pipeline, without copying pointers to "other" local variables. */

    return *this;
}

long PressureSensorReader::read(){
    while(!update()){}

    return current();
}

bool PressureSensorReader::update(){
    if (!reader){
        return false;
    }

    lastReading = reader->read();
    return _pipeline.update(lastReading);
}


bool PressureSensorReader::isCalibrating() const{
    return baselineFilter.calibrating();
}
void PressureSensorReader::startCalibration(){
    this->baselineFilter.calibrationStart();
}
void PressureSensorReader::stopCalibration(){
    this->baselineFilter.calibrationStop();
    resetData();
}
void PressureSensorReader::resetCalibration(){
    //Maybe: integrate this method with resetData(??)

    this->baselineFilter.resetCalibration();
}
void PressureSensorReader::resetData(){
    runningAverage.reset();

    rangeFilter = RangeFilter<long>();
    runningMedian = FastRunningMedian<long>(runningMedian.windowSize());
}

long PressureSensorReader::baselineThreshold() const {
    return baselineFilter.baselineThreshold();
}

long PressureSensorReader::current() const{
    return _pipeline.current();
}

long PressureSensorReader::currentUnfiltered() const{
    return lastReading;
}

float PressureSensorReader::relative() const{
    return rangeFilter.relative();
}

long PressureSensorReader::minReadedValue() const{
  return rangeFilter.min();
}
long PressureSensorReader::maxReadedValue() const{
  return rangeFilter.max();
}

int PressureSensorReader::medianWindowSize() const{
    return runningMedian.windowSize();
}
int PressureSensorReader::averageWindowSize() const{
    return runningAverage.windowSize();
}
