#ifndef PRESSURE_SENSOR_READER_H
#define PRESSURE_SENSOR_READER_H

#include "BoardConfig.h"

#include "FastRunningMedian.h"
#include "RunningAverage.h"
#include "RangeFilter.h"
#include "AverageFilter.h"
#include "BaselineFilter.h"

#include "reader/CapReader.h"

#include "FilterPipeline.h"


class PressureSensorReader: public CapReader{
public:
    PressureSensorReader (
        CapReader * reader = nullptr,
        int medianWindowSize  = MEDIAN_WINDOW_SIZE,
        int averageWindowSize = AVERAGE_WINDOW_SIZE,
        float boundaryFactor = 0.0f);

    PressureSensorReader(const PressureSensorReader & other);
    PressureSensorReader(PressureSensorReader && other);

    PressureSensorReader & operator=(const PressureSensorReader & other);
    PressureSensorReader & operator=(PressureSensorReader && other);

public:
    virtual bool update();
    long read() override;
    long current() const;
    long currentUnfiltered() const;
    float relative() const;

public:
    bool isCalibrating() const;
    void startCalibration();
    void stopCalibration();
    void resetCalibration();
    void resetData();

    long baselineThreshold() const;

public:
    long minReadedValue() const;
    long maxReadedValue() const;

public:
    int medianWindowSize() const;
    int averageWindowSize() const;

protected:
    const
    FilterPipeline & pipeline() const{ return _pipeline;}
    FilterPipeline & pipeline(){ return _pipeline;}

public:
    CapReader * reader;
    FastRunningMedian<long> runningMedian;
    AverageFilter<long> runningAverage;
    BaselineFilter baselineFilter;
    RangeFilter<long> rangeFilter;

protected:
    FilterPipeline _pipeline;
    long lastReading = 0;
};

#endif
