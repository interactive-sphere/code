#include "CapPressureServices.h"

#include "log.h"

#include <functional>

using namespace pressure;
using namespace std::placeholders;

using CapService     = CapPressureServices;
using MuxReaderArray = CapService::MuxReaderArray;
using SensorsArray   = CapService::SensorsArray;

CapService::CapPressureServices (Config c)
    : mux(c.muxPins()[0],
          c.muxPins()[1],
          c.muxPins()[2],
          c.muxPins()[3])
    , capReader(c.capSenseRcvPin(), c.capSenseSendPin())
    , sensorDescriptions(std::move(c.sensors()))

{
  numSensors = sensorDescriptions.size();
  initSensors(c);
}

void CapService::initSensors(Config & config){
    for(auto i=0; i < numSensors; ++i){
        this->muxReaders[i]      = MuxReader(i, &this->mux, &this->capReader);
        this->pressureSensors[i] = PressureSensor(
            i,
            &this->muxReaders[i],
            config.medianWindowSize(),
            config.averageWindowSize(),
            config.releaseThreshold(),
            config.pressThreshold()
        );
    }
}


void CapService::stateNotifier(StateNotifier * notifier){
    this->_stateNotifier = notifier;
}

void CapService::begin(){
    configSensors();
}

void CapService::configSensors(){
    auto listener = std::bind(&CapService::onSensorEvent, this, _1);
    for (int i = 0; i < numSensors; ++i){
        auto & sensor = this->pressureSensors[i];
        sensor.listenPressureEvents(listener);
    }
}

void CapService::calibrateFor(unsigned long millis){
    calibrationTime.resetTimer(millis);
    startCalibration();
}
void CapService::startCalibration(){
    calibrating = true;
    for(auto & sens: pressureSensors){
        sens.startCalibration();
    }

    if(stateNotifier()){
        stateNotifier()->onStartCalibration();
    }
}
void CapService::stopCalibration(){
    for(auto & sens: pressureSensors){
        sens.stopCalibration();
    }
    calibrating = false;

    if(stateNotifier()){
        stateNotifier()->onStopCalibration();
    }
}
bool CapService::isCalibrating() const{
    return calibrating;
}


void CapService::update(){
    if(isCalibrating() && calibrationTime.completed()){
        stopCalibration();
    }

    for (int i = 0; i < numSensors; ++i){
        pressureSensors[i].update();
    }
}

void CapService::onSensorEvent(const pressure::Event & evt){
    if (listener){
        listener(evt);
    }
}

const SensorDescription &
    CapService::getSensorDescription(SensorId sensorId)
{
    return sensorDescriptions[sensorId];
}

const SensorDescriptions & CapService::getSensorDescriptions(){
    return sensorDescriptions;
}


Measure CapService::readSensor(const pressure::SensorId sensorId){
    if(sensorId < numSensors && sensorId >= 0){
        return pressureSensors[sensorId].readMeasure();
    }

    return Measure::invalidMeasure();
}

const pressure::Measure &
  CapService::getMeasure(const pressure::SensorId sensorId) const
{
  if(sensorId < numSensors && sensorId >= 0){
      return pressureSensors[sensorId].currentMeasure();
  }

  static Measure invalidMeasure;

  return invalidMeasure;
}

void CapService::listenPressureEvents(PressureListener listener){
    this->listener = listener;
}

void CapService::removeListener(){
    this->listener = {};
}
