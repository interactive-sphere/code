#ifndef PRESSURE_EVENT_DETECTOR_H
#define PRESSURE_EVENT_DETECTOR_H

#include "pressure/Event.h"
#include "pressure/EventListener.h"

#include "BoardConfig.h"

#include "DebounceFilter.h"
#include "LevelTrigger.h"

namespace pressure{

class EventDetector {
public:
    EventDetector (
        SensorId id = 0,
        float releaseThreshold=PRESSURE_RELEASE_THRESHOLD,
        float pressThreshold=PRESSURE_PRESS_THRESHOLD,
        int debounceThreshold=PRESSURE_DEBOUNCE_THRESHOLD);

    void reset();
    bool update(const Measure & measure);
    void onPressureEvent(EventListener listener);
    void clearEventsListener();

    EventType state() const;

    SensorId id() const      { return _id; }
    SensorId id(SensorId id) { return _id = id; }

public:
    int debounceThreshold() const;
    int debounceThreshold(int threshold);

    float lowerLevelThreshold() const;
    float lowerLevelThreshold(float threshold);

    float upperLevelThreshold() const;
    float upperLevelThreshold(float threshold);

protected:
    void changeState(bool newState, const Measure &  measure);

protected:
    SensorId _id;
    LevelTrigger<float> levelTrigger;
    DebounceFilter debounce;
    EventType currentState;
    EventListener listener;
};

}//namespace

using PressureEventDetector = pressure::EventDetector;

#endif
