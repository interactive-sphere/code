#ifndef PRESSURE_SENSOR_H
#define PRESSURE_SENSOR_H

#include "PressureSensorReader.h"
#include "PressureEventDetector.h"
#include "pressure/EventListener.h"

class PressureSensor{
public:
    class Config{
    public:
        Config(
            int medianWindowSize  = MEDIAN_WINDOW_SIZE,
            int averageWindowSize = AVERAGE_WINDOW_SIZE,

            float releaseThreshold = PRESSURE_RELEASE_THRESHOLD,
            float pressThreshold   = PRESSURE_PRESS_THRESHOLD,
            int debounceThreshold  = PRESSURE_DEBOUNCE_THRESHOLD,
            float boundaryFactor   = BOUNDARY_FACTOR)
            : medianWindowSize(medianWindowSize)
            , averageWindowSize(averageWindowSize)
            , releaseThreshold(releaseThreshold)
            , pressThreshold(pressThreshold)
            , debounceThreshold(debounceThreshold)
            , boundaryFactor(boundaryFactor)
        {}

    public:
        int medianWindowSize;
        int averageWindowSize;

        float releaseThreshold;
        float pressThreshold;
        int debounceThreshold;
        float boundaryFactor;
    };
public:
    PressureSensor();

    PressureSensor (
        pressure::SensorId id,
        CapReader * reader,
        Config config
    );

    /** deprecated */
    PressureSensor (
        pressure::SensorId id,
        CapReader * reader,
        int medianWindowSize  = MEDIAN_WINDOW_SIZE,
        int averageWindowSize = AVERAGE_WINDOW_SIZE,

        float releaseThreshold = PRESSURE_RELEASE_THRESHOLD,
        float pressThreshold   = PRESSURE_PRESS_THRESHOLD,
        int debounceThreshold  = PRESSURE_DEBOUNCE_THRESHOLD,
        float boundaryFactor   = BOUNDARY_FACTOR
    );

    inline
    pressure::SensorId id() const{ return detector.id(); }

    bool update();
    void listenPressureEvents(pressure::EventListener listener);
    void clearEventsListener();

    /** Updates readings and get current measure. */
    pressure::Measure readMeasure();

    /**
      Get the last readings (current measure).
      Should call update before, to ensure a valid reading.
    */
    const pressure::Measure & currentMeasure() const;

public:
    void startCalibration();
    void stopCalibration();

public:
    PressureSensorReader reader;
    PressureEventDetector detector;

protected:
    pressure::Measure current;
};

#endif
