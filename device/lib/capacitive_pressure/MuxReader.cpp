#include "MuxReader.h"

#include "ArduinoFramework.h"

MuxReader::MuxReader (int channel, Multiplexer * mux, CapReader * reader)
    : channel(channel)
    , mux(mux)
    , reader(reader)
{}


int  MuxReader::getChannel() const{ return channel;}
void MuxReader::setChannel(int c) { this->channel = c;}

Multiplexer * MuxReader::getMux() const{
    return mux;
}
CapReader   * MuxReader::getReader() const{
    return reader;
}

void MuxReader::setMux(Multiplexer * mux){
    this->mux = mux;
}
void MuxReader::setReader(CapReader * reader){
    this->reader = reader;
}

void MuxReader::select(){
    if (mux){
        mux->channel(channel);
    }
}

long MuxReader::read(){
    select();

    if (reader){
        return reader->read();
    }

    return -1;
}
