#ifndef BOARD_SENSORS_H
#define BOARD_SENSORS_H

#include "pressure/SensorDescriptions.h"

SensorDescriptions boardSensors();

#endif
