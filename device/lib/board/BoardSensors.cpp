#include "BoardSensors.h"

#include <vector>

using namespace pressure;

void addSensor(SensorDescriptions & sensors, SensorDescription && s);

SensorDescriptions boardSensors(){
    SensorDescriptions sensors;

    SensorId id = 0;

    addSensor(sensors, { id++, "front"});
    addSensor(sensors, { id++, "frontUp"});
    addSensor(sensors, { id++, "frontDown"});
    addSensor(sensors, { id++, "frontLeft"});
    addSensor(sensors, { id++, "frontRight"});
    addSensor(sensors, { id++, "top"});
    addSensor(sensors, { id++, "backTop"});
    addSensor(sensors, { id++, "backMiddleUp"});
    addSensor(sensors, { id++, "backMiddleDown"});
    addSensor(sensors, { id++, "backBottom"});
    addSensor(sensors, { id++, "?"});
    addSensor(sensors, { id++, "?"});
    addSensor(sensors, { id++, "?"});
    addSensor(sensors, { id++, "?"});
    addSensor(sensors, { id++, "?"});
    addSensor(sensors, { id++, "?"});

    return std::move(sensors);
}

void addSensor(SensorDescriptions & sensors, SensorDescription && s){
    sensors[s.id()] = std::move(s);
}
