#ifndef BOARD_PINOUT_H
#define BOARD_PINOUT_H

// Gyroscope
#define GYRO_SDA_PIN 21
#define GYRO_SCL_PIN 22
#define GYRO_INTERRUPT_PIN 23

#define DEFAULT_GRAVITY_X 1
#define DEFAULT_GRAVITY_Y 0
#define DEFAULT_GRAVITY_Z 0

// LedRgb
#define LEDRGB_PIN 14
#define LEDRGB_COUNT 3

// Vibration motor
#define VIBRATION_PIN 25

// Buzzer
#define BUZZER_PIN 32
#define BUZZER_CHANNEL 0

// Multiplexer
#define MUX_S0_PIN 17
#define MUX_S1_PIN 5
#define MUX_S2_PIN 18
#define MUX_S3_PIN 19
#define MUX_SIG_PIN 4

// capacitive pressure sensor
#define CAPSENSE_RCV_PIN MUX_SIG_PIN
#define CAPSENSE_SND_PIN 15


#endif
