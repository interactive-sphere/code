#ifndef LEVEL_TRIGGER_H
#define LEVEL_TRIGGER_H

template<typename T>
class LevelTrigger {
public:
    LevelTrigger (T lowerThreshold, T upperThreshold)
        : _lowerThreshold(lowerThreshold)
        , _upperThreshold(upperThreshold)
        , _current((T)((lowerThreshold+upperThreshold)/2))
        , _state(false)
    {
        if (_current < lowerThreshold)
        {
            _current = lowerThreshold;
        }
    }

    bool update(T value){
        _current = value;
        if (isHigh()){
            this->_state = true;
            return true;
        }
        else if(isLow()){
            this->_state = false;
            return true;
        }

        return false;
    }

    bool isHigh()   const{ return current() >= upperThreshold(); }
    bool isLow()    const{ return current() <= lowerThreshold(); }
    inline
    bool isMiddle() const{ return !isHigh() && !isLow();}

    inline T current() const { return _current; }

    inline bool state() const{
        return _state;
    }
    bool state(bool newState){
        return _state = newState;
    }

    bool on() const{
        return _state;
    }

    bool off() const{
        return !on();
    }

    T upperThreshold(T newValue){
        return _upperThreshold = newValue;
    }
    T upperThreshold() const{
        return _upperThreshold;
    }

    T lowerThreshold(T newValue){
        return _lowerThreshold = newValue;
    }
    T lowerThreshold() const{
        return _lowerThreshold;
    }

protected:
    T _lowerThreshold, _upperThreshold, _current;
    bool _state;
};

#endif
