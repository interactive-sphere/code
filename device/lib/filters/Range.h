#ifndef RANGE_H
#define RANGE_H

#include <limits>

template<typename T>
class Range{
public:
    Range (
        T minValue=std::numeric_limits<T>::max(),
        T maxValue=std::numeric_limits<T>::lowest())
        : _minValue(minValue)
        , _maxValue(maxValue)
    {}

    void update(T value){
        if (value < _minValue){
            this->_minValue = value;
        }

        if (value > _maxValue){
            this->_maxValue = value;
        }
    }

    float relative(const T & value) const{
        if (range() == 0){
            return 0.0f;
        }

        return diff(value)/(float)range();
    }

    T diff(const T & value) const{
        return value - min();
    }

    T range() const{
        return _maxValue - _minValue;
    }

    T min() const{ return _minValue;}
    T max() const{ return _maxValue;}

    void set(T min, T max){
        this->_minValue = min;
        this->_maxValue = max;
    }

    bool isInRange(T value) const{
        return min() <= value && value <= max();
    }

protected:
    T _minValue, _maxValue;
};

#endif
