#include "Baseline.h"
#include "Range.h"


Baseline::Baseline()
{
    resetCalibration();
}

void Baseline::resetCalibration(){
    _baselineRange = MeasureRange(
            std::numeric_limits<long>::max(), //minimum
            0 // starting max with 0 to avoid smaller values
        );
}


float Baseline::boundaryFactor() const{
    return _boundaryFactor;
}
float Baseline::boundaryFactor(float factor){
    this->_boundaryFactor = factor;
    return _boundaryFactor;
}


long Baseline::baselineThreshold() const{
    auto boundaryThreshold =
        (long) baselineRange().range() * boundaryFactor();

    return baselineRange().max() + boundaryThreshold;
}

void Baseline::calibrationStart(){
    this->calibrating = true;
}

void Baseline::calibrationStop(){
    this->calibrating = false;
}


bool Baseline::isHigh(long measure) const{
    return measure > baselineThreshold();
}

void Baseline::update(long measure){
    if(calibrating){
        this->_baselineRange.update(measure);
    }
}
