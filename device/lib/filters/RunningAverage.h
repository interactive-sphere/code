#ifndef RUNNING_AVERAGE
#define RUNNING_AVERAGE

#include <vector>
#include <cstddef>

#include "Filter.h"

class RunningAverage: public Filter<float>{
    int measuresLenght;
    float currentSum;
    int counter;
    int index;
    std::vector<float> measures;
public:
    RunningAverage(int lenght=10);

    void reset();
    bool update(float value) override;

public:
    float current() const override;
    float average() const;
    float sum() const;
    int count() const;

public:
    int windowSize() const;

protected:
    void updateAverage(float newValue, float removedValue);
};

#endif
