#ifndef FILTERS__FILTER_PIPELINE_H
#define FILTERS__FILTER_PIPELINE_H


#include "Filter.h"

#include <cstddef>
#include <vector>

class FilterPipeline: public Filter<long>{
public:
    using FilterT = Filter<long>;

public:
    std::size_t size() const{ return filters.size(); }
    FilterT *   get(std::size_t idx) const{
        return filters[idx];
    }

    void addFilter(FilterT * filter);
    void insertFilter(FilterT * filter, int wanted_index);
    void clear(){
        filters.clear();
    }

public:
    bool update(long value) override;
    long current() const  override{ return _current;}

protected:
    std::vector<FilterT *> filters;
    long _current=0;
};

#endif