#ifndef FILTERS__BASELINE_H
#define FILTERS__BASELINE_H

#include "Range.h"


class Baseline{
public:
    using MeasureRange = Range<long>;

public:
    Baseline();

public:
    void calibrationStart();
    void calibrationStop();
    bool isCalibrating() const { return calibrating; }
    void resetCalibration();

    void update(long measure);

    bool isHigh(long measure) const;
    bool isLow(long measure) const { return !isHigh(measure); }

public:
    float boundaryFactor() const;
    float boundaryFactor(float factor);
    long baselineThreshold() const;

public:
    const MeasureRange  & baselineRange() const { return _baselineRange; }

protected:
    MeasureRange _baselineRange;
    float _boundaryFactor = 0.0f;
    bool calibrating = false;
};

#endif
