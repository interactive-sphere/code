#include "BaselineFilter.h"


BaselineFilter::BaselineFilter(float boundaryFactor)
{
    this->boundaryFactor(boundaryFactor);
}


float BaselineFilter::boundaryFactor() const{
    return baseline().boundaryFactor();
}
float BaselineFilter::boundaryFactor(float factor){
    return _baseline.boundaryFactor(factor);
}

void BaselineFilter::calibrationStart(){
    this->_baseline.calibrationStart();
}

void BaselineFilter::calibrationStop(){
    this->_baseline.calibrationStop();
}

void BaselineFilter::resetCalibration(){
    this->_baseline.resetCalibration();
}


bool BaselineFilter::update(long measure){
    if(calibrating()){
        this->_baseline.update(measure);
    }

    lastMeasure = measure;

    return !calibrating();
}

long BaselineFilter::current() const{
    return filterValue(lastMeasure);
}

long BaselineFilter::filterValue(long measure) const{
    if(_baseline.baselineRange().max() <= 0){
        //not calibrated
        return measure;
    }

    if(_baseline.isLow(measure)){
        return 0;
    }

    return measure - _baseline.baselineThreshold();
}


bool BaselineFilter::calibrating() const{
    return _baseline.isCalibrating();
}