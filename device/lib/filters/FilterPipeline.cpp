#include "FilterPipeline.h"

bool FilterPipeline::update(long value){
    if(filters.empty()){
        return true;
    }

    long chainedValue = value;

    for(auto * filter: filters){
        if(!filter->update(chainedValue)){
            return false;
        }

        chainedValue = filter->current();
    }

    _current = chainedValue;
    return true;
}

void FilterPipeline::addFilter(FilterT *filter){
    if(filter != nullptr){
        this->filters.push_back(filter);
    }
}

void FilterPipeline::insertFilter(FilterT * filter, int wanted_index){
    if(filter == nullptr) return; //maybe should throw

    if(wanted_index > filters.size()){
        //wanted_index too high, so add at the end
        addFilter(filter);
        return;
    }

    int index = (wanted_index  < 0) ? 0 : wanted_index;
    this->filters.insert(filters.begin() + index, filter);
}