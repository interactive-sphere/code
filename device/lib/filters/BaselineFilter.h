#ifndef FILTERS__BASELINE_FILTER_H
#define FILTERS__BASELINE_FILTER_H

#include "Range.h"

#include "Filter.h"
#include "Baseline.h"


class BaselineFilter: public Filter<long>{
public:
    using MeasureRange = Range<long>;

public:
    BaselineFilter() = default;
    BaselineFilter(float boundaryFactor);

public:
    bool calibrating() const;
    void calibrationStart();
    void calibrationStop();
    void resetCalibration();

public:
    bool update(long measure) override;
    long current() const override;

public:
    long filterValue(long measure) const;

public:
    const Baseline & baseline() const { return _baseline; }

    long baselineThreshold() const {return baseline().baselineThreshold();}
    float boundaryFactor() const;
    float boundaryFactor(float factor);

protected:
    long lastMeasure = 0;
    Baseline _baseline;
};

#endif
