#ifndef RANGE_FILTER_H
#define RANGE_FILTER_H

#include "Filter.h"
#include "Range.h"

#include <limits>

template<typename T>
class RangeFilter: public Filter<T> {
public:
    RangeFilter (
        T default_value=0,
        T minValue=std::numeric_limits<T>::max(),
        T maxValue=std::numeric_limits<T>::lowest())
        : _current(default_value)
        , _range(minValue, maxValue)
    {}

    bool update(T value){
        this->_current = value;

        this->_range.update(value);

        return true;
    }

    T current() const override{
        return _current;
    }

    float relative() const{
        return _range.relative(current());
    }

    T diff() const{
        return _range.diff(current());
    }

    T range() const{
        return _range.range();
    }

    T min() const{ return _range.min();}
    T max() const{ return _range.max();}

    bool isInRange(T value) const{
        return _range.isInRange(value);
    }

protected:
    T _current;
    Range<T> _range;
};

#endif
