#ifndef RUNNING_MEDIAN_H
#define RUNNING_MEDIAN_H

#include "Filter.h"

template <typename T>
class RunningMedian: public Filter<T> {
public:
    virtual ~RunningMedian(){}

    virtual T getMedian() const = 0;
    virtual int getCounter() const=0;
    virtual int windowSize() const=0;

    virtual void addValue(T new_value)=0;

    T current() const override{
        return getMedian();
    }
    bool update(T value) override{
        this->addValue(value);
        return true;
    }
};

#endif
