#ifndef FILTERS_AVERAGE_FILTER_H
#define FILTERS_AVERAGE_FILTER_H

#include "Filter.h"
#include "RunningAverage.h"


template<typename T>
class AverageFilter: public Filter<T>{
public:

    AverageFilter(int lenght=10)
        : runningAverage(lenght)
    {}


    bool update(T value) override{
        return runningAverage.update(static_cast<float>(value));
    };
    T current() const override{
        return static_cast<T>(runningAverage.current());
    }

public:
    void reset(){runningAverage.reset();}
    float average() const{ return runningAverage.average(); }
    int windowSize() const{return runningAverage.windowSize();}

protected:


protected:
    RunningAverage runningAverage;
};

#endif