#include "RunningAverage.h"


RunningAverage::RunningAverage(int lenght)
    : measuresLenght(lenght)
    , currentSum(0)
    , counter(0)
    , index(0)
{
    measures.resize(measuresLenght, 0);
}

void RunningAverage::reset(){
    index = 0;
    counter = 0;
    currentSum = 0;

    measures.resize(measuresLenght, 0);
    for (size_t i = 0; i < measuresLenght; i++) {
        measures[i] = 0;
    }
}

bool RunningAverage::update(float value){
    index = (index + 1) % measuresLenght;

    this->updateAverage(value, measures[index]);

    measures[index] = value;

    if (counter < measuresLenght){
        ++counter;
    }

    return true;
}

void RunningAverage::updateAverage(float newValue, float removedValue){
    currentSum += newValue - removedValue;
}


int RunningAverage::windowSize() const{
    return measuresLenght;
}

float RunningAverage::current() const{
    return average();
}

float RunningAverage::average() const{
    if (count() == 0){
        return 0;
    }
    return currentSum / count();
}

float RunningAverage::sum() const{
    return currentSum;
}

int RunningAverage::count() const{
    return counter;
}