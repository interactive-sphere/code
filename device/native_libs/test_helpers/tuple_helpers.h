#ifndef TEST_HELPERS__TUPLE_HELPERS_H
#define TEST_HELPERS__TUPLE_HELPERS_H

#include <functional>
#include <tuple>
#include <utility>

namespace test_helpers {

    namespace tuple_helpers {
        template<typename T, T...>
        struct integer_sequence { };

        template<std::size_t N, std::size_t... I>
        struct gen_indices : gen_indices<(N - 1), (N - 1), I...> { };

        template<std::size_t... I>
        struct gen_indices<0, I...> : integer_sequence<std::size_t, I...> { };


        template<typename Ret, typename... T, std::size_t... I>
        Ret call_with(
            std::function<Ret(T...)> func,
            const std::tuple<T...>& tup,
            integer_sequence<std::size_t, I...>)
        {
            return func(std::get<I>(tup)...);
        }

    }

    template<typename Ret, typename... T>
    Ret call_with(std::function<Ret(T...)> func, const std::tuple<T...>& tup)
    {
        return tuple_helpers::call_with(
            func, tup, tuple_helpers::gen_indices<sizeof...(T)>{});
    }

} //namespace


#endif