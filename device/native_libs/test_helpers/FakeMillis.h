#pragma once

class FakeMillis{
public:
    static void useSystemTime();
    static long getSystemMillis();
};