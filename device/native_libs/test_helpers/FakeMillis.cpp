#include "FakeMillis.h"

#include "ArduinoFake.h"
#include <chrono>

using namespace fakeit;
using namespace std::chrono;

void FakeMillis::useSystemTime(){
    When(Method(ArduinoFake(), millis)).AlwaysDo([](){
        return FakeMillis::getSystemMillis();
    });
}

long FakeMillis::getSystemMillis(){
    //snippet from: https://stackoverflow.com/a/2834294

    auto time = system_clock::now(); // get the current time

    auto since_epoch = time.time_since_epoch(); // get the duration since epoch

    auto millis = duration_cast<milliseconds>(since_epoch);

    return millis.count();
}