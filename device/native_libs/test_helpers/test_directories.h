#ifndef TEST_HELPERS__TEST_DIRECTORIES_H
#define TEST_HELPERS__TEST_DIRECTORIES_H

#ifndef TEST_DIR
#define TEST_DIR "."
#endif

#include "ArduinoFramework.h"

class TestDirectories{
public:
    static String testDir(){ return TEST_DIR;}
    static String testDataDir(){ return testDir() + "/../test_data";}
};

#endif