#ifndef TEST_HELPERS__TO_STRING_H
#define TEST_HELPERS__TO_STRING_H

#include <iostream>
#include <string>
#include <tuple>
#include <utility>

#include "tuple_helpers.h"


namespace test_helpers {

    namespace impl {

        inline std::string to_string_simple(const char * c_str){
            return std::string(c_str);
        }

        inline std::string to_string_simple(const std::string & str){
            return str;
        }

        template<typename T>
        std::string to_string_simple(T && value){
            return std::to_string(std::forward<T>(value));
        }

        template<typename H>
        std::string& to_string_tuple(std::string& s, H&& h){
            s += to_string_simple(std::forward<H>(h));

            return s;
        }

        template<typename H, typename... T>
        std::string& to_string_tuple(std::string& s, H&& h, T&&... t){
            s += to_string_simple(std::forward<H>(h));
            return to_string_tuple(s, std::forward<T>(t)...);
        }
    }


    template<typename... T, std::size_t... I>
    std::string to_string(
        const std::tuple<T...>& tup, tuple_helpers::integer_sequence<std::size_t, I...>)
    {
        std::string result;
        return impl::to_string_tuple(result, std::get<I>(tup)...);
    }

    template<typename... T>
    std::string to_string(const std::tuple<T...>& tup){
        return to_string(tup, tuple_helpers::gen_indices<sizeof...(T)>{});
    }

    template<typename... T>
    std::string to_string(const T&... elements){
        return to_string(std::make_tuple(elements...));
    }

    namespace impl {

        template<typename T>
        std::string& join_str_impl(
            std::string& out, const std::string & sep, T&& t)
        {
            out += to_string_simple(std::forward<T>(t));
            return out;
        }

        template<typename H, typename... T>
        std::string & join_str_impl(
            std::string& out, const std::string & sep, H&& h, T&&... others)
        {
            out += to_string_simple(std::forward<H>(h)) + sep;
            return impl::join_str_impl(out, sep, std::forward<T>(others)...);
        }
    }

    template<typename... T>
    std::string join_str(const std::string & sep, T&&... elements){
        std::string result;
        return impl::join_str_impl(result, sep, std::forward<T>(elements)...);
    }

} //namespace


#endif