#ifndef TEST_HELPERS__CLASS_TESTS_H
#define TEST_HELPERS__CLASS_TESTS_H


#include "unity.h"
#include <exception>
#include <functional>
#include <utility>
#include <vector>

#include "to_string.h"


#define CONCAT_(x,y) x##y
#define CONCAT(x,y) CONCAT_(x,y)
#define CONCAT_SEP(x,sep,y) CONCAT(x,CONCAT(sep, y))

#define CAPTURE_TEST_ERROR(x) \
    try{ \
        x; \
    } \
    catch(const std::exception & e){ \
        TEST_FAIL_MESSAGE(e.what()); \
    } \
    catch(...){ \
        TEST_FAIL_MESSAGE("Failed due to unknown exception."); \
    }

#define MAKE_TEST_LAMBDA(code) \
    [](){                      \
        CAPTURE_TEST_ERROR(    \
            code;              \
        );                     \
    }

#define MAKE_TEST_LAMBDA_CAPTURE(code) \
    [=](){                      \
        CAPTURE_TEST_ERROR(     \
            code;               \
        );                      \
    }

#define MAKE_CLASS_CALL(class_name, method_name, ...) \
    class_name().method_name(__VA_ARGS__)


#define MAKE_STR(value) #value
#define EXPAND_AND_QUOTE(str) MAKE_STR(str)

// ------------------------------------------------------------------------

namespace test_helpers {
    inline void runTest(
        UnityTestFunction func, const std::string & testName, const char * filename, const int lineNum)
    {
        UnitySetTestFile(filename);
        UnityDefaultTestRun(func, testName.c_str(), lineNum);
    }

    template<typename FuncT, typename ...T>
    inline void runTestFunctional(
        FuncT&& func, const std::string & testName,
        const char * filename,
        const int lineNum, T&&... params)
    {
        UnitySetTestFile(filename);

        Unity.CurrentTestName = testName.c_str();
        Unity.CurrentTestLineNumber = (UNITY_LINE_TYPE)lineNum;
        Unity.NumberOfTests++;
        UNITY_CLR_DETAILS();
        UNITY_EXEC_TIME_START();
        if (TEST_PROTECT())
        {
            setUp();
            func(std::forward<T>(params)...);
        }
        if (TEST_PROTECT())
        {
            tearDown();
        }
        UNITY_EXEC_TIME_STOP();
        UnityConcludeTest();
    }

    template<typename ...T>
    std::string paramsSuffix(T&&... params){
        return "<" + test_helpers::join_str(
                        ",",  std::forward<T>(params)...) + ">";
    }

    template<typename C, typename ...T>
    void runTestsWithParams(
        const std::string & baseTestName,
        std::function<C(void)> builder,
        void (C::*method)(T...),
        const std::vector<std::tuple<T...>> argsList,
        const char * filename,
        int lineNum)
    {
        std::function<void(T...)> callMethod = [&](T&&... funcArgs){
            CAPTURE_TEST_ERROR(
                //Creates class and call method with args
                (builder().*method)(
                    std::forward<T>(funcArgs)...
                );
            );
        };

        std::function<void(T...)> runTest = [&](T&&... funcArgs){
            auto testName = baseTestName
                                + paramsSuffix(std::forward<T>(funcArgs)...);

            runTestFunctional(callMethod, testName, filename, lineNum, std::forward<T>(funcArgs)...);
        };

        for (auto&& argsTuple: argsList) {
            //Call runner, unpacking args from tuple
            test_helpers::call_with(
                runTest, argsTuple
            );
        }
    }
}

#define CLASS_TEST_NAME(class_name, method_name) \
    CONCAT_SEP(class_name, _, method_name)

#define CLASS_TEST_NAME_STR(class_name, method_name) \
    EXPAND_AND_QUOTE(CLASS_TEST_NAME(class_name, method_name))

#define CLASS_TEST_NAME_PARAMS_STR(class_name, method, ...)     \
    CLASS_TEST_NAME_STR(class_name, method) "<" \
        + test_helpers::join_str(",",  __VA_ARGS__ ) + ">"


// -----------------------------------------------------------------------

#define RUN_CLASS_TEST(class_name, method_name) \
    test_helpers::runTest(MAKE_TEST_LAMBDA(           \
        MAKE_CLASS_CALL(class_name,method_name)), \
        CLASS_TEST_NAME_STR(class_name, method_name), \
        __FILE__, __LINE__);

#define RUN_CLASS_TEST_PARAMS(class_name, method_name, ...) \
    test_helpers::runTestFunctional(           \
        MAKE_TEST_LAMBDA_CAPTURE( \
            MAKE_CLASS_CALL(class_name, method_name, __VA_ARGS__)), \
        CLASS_TEST_NAME_PARAMS_STR(class_name, method_name, __VA_ARGS__), \
        __FILE__, __LINE__);


#define RUN_CLASS_PARAMETERIZED(class_name, method_name, ...) \
    test_helpers::runTestsWithParams(                   \
        CLASS_TEST_NAME_STR(class_name, method_name),   \
        std::function<class_name()>(                    \
            [](){ return class_name();}                 \
        ),                                              \
        &class_name::method_name,                       \
        {__VA_ARGS__},               \
        __FILE__, __LINE__);

#endif