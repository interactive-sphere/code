#ifndef TEST_HELPERS__COLLECTIONS_HELPERS_H
#define TEST_HELPERS__COLLECTIONS_HELPERS_H

#include <cstddef>

namespace test_helpers{

template<typename Coll>
Coll split(const Coll & src, std::size_t  startIdx, std::size_t endIdx){
    return Coll(src.begin() + startIdx, src.begin() + endIdx);
}


} //namespace

#endif