#ifndef TEST_HELPERS__UNITY_TEST_ASSERTIONS_H
#define TEST_HELPERS__UNITY_TEST_ASSERTIONS_H

#include <string>
#include <vector>
#include <sstream>

#include "unity.h"


namespace test_helpers {

    template <typename StringVec>
    std::vector<const char *> toCstrVec(const StringVec & values)
    {
        std::vector<const char *> out;

        for(const auto & str: values){
            out.push_back(str.c_str());
        }

        return out;
    }

    template<typename StringVec, typename StringT>
    void assertStringVecsAreEquals(
        const StringVec & expected,
        const StringVec & values,
        const StringT & msg)
    {
        auto expec_cstr  = toCstrVec(expected);
        auto values_cstr = toCstrVec(values);

        TEST_ASSERT_EQUAL_STRING_ARRAY_MESSAGE(
            expec_cstr.data(), values_cstr.data(),
            expec_cstr.size(),
            msg.c_str()
        );
    }

    template<typename Vec, typename Str=std::string>
    void hasSameSize(
        const Vec & expected, const Vec & values, const Str & msg="")
    {
        std::stringstream err_msg;
        err_msg << "Collection has not the same size. " << msg;

        TEST_ASSERT_EQUAL_MESSAGE(
            expected.size(), values.size(),
            err_msg.str().c_str()
        );
    }

    template<typename Vec=std::vector<double>, typename Str=std::string>
    void floatVectorsAreEquals(
        const Vec & expected,
        const Vec & values,
        const Str & msg = "")
    {
        hasSameSize(expected, values);

        std::stringstream err_msg;

        for (auto i=0; i < expected.size(); ++i) {
            err_msg << "Collections differ at index " << i
                    << " (" << expected[i] << " != " << values[i] << "). "
                    << msg;

            #ifdef UNITY_INCLUDE_DOUBLE
                TEST_ASSERT_EQUAL_DOUBLE_MESSAGE(
                    expected[i], values[i], err_msg.str().c_str()
                );
            #else
                TEST_ASSERT_EQUAL_FLOAT_MESSAGE(
                    (float)expected[i], (float)values[i], err_msg.str().c_str()
                );
            #endif

            err_msg.str(""); //clear
        }
    }


};//namespace


#endif