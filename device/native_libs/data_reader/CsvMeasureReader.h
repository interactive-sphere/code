#ifndef CSV_MEASURE_READER_H
#define CSV_MEASURE_READER_H

#include <vector>
#include <string>

class CsvMeasureReader{
public:
    std::vector<double> readDoubleMeasures(
        const std::string & file,
        const std::string & measureColumn="measure",
        const std::string & delim="\t");

    template<typename T=double>
    std::vector<T> readMeasures(
        const std::string & file,
        const std::string & measureColumn="measure",
        const std::string & delim="\t")
    {
        auto doubleMeasures = readDoubleMeasures(file, measureColumn, delim);

        std::vector<T> out;
        for(auto value: doubleMeasures){
            out.push_back((T)value);
        }

        return std::move(out);
    }
};

#endif