#include "CsvReader.h"
#include <string>

using namespace std;

using StringVec = CsvReader::StringVec;

CsvReader::~CsvReader(){
    close();
}


void CsvReader::open(const String & path, const String & delim){
    this->open(path.c_str(), delim.c_str());
}
void CsvReader::open(const std::string & path, const std::string & delim){
    this->open(path.c_str(), delim.c_str());
}

void CsvReader::open(const char * path, const char * delim){
    _csvFile.open(path);
    _delimiter = delim;

    this->_columns = readline();
    for (int i=0; i<_columns.size(); ++i) {
        columnsToIndex[_columns[i]] = i;
    }
}

void CsvReader::close(){
    if(_csvFile.is_open()){
        _csvFile.close();
    }
}


bool CsvReader::hasColumn(const std::string & col) const{
    return this->columnsToIndex.find(col) != columnsToIndex.end();
}


long CsvReader::getLong(const std::string & col) const{
    auto valueStr = getString(col);
    return std::stol(getString(col));
}

double CsvReader::getDouble(const std::string & col) const{
    auto valueStr = getString(col);
    return std::stod(getString(col));
}

std::string CsvReader::getString(const std::string & col) const{
    auto idx = columnsToIndex.find(col);

    if(idx == columnsToIndex.end()){
        return {}; //maybe should throw
    }

    return values()[idx->second];
}

bool CsvReader::next(){
    return readlineInto(this->_values);
}

StringVec CsvReader::readline(){
    StringVec values;

    readlineInto(values);

    return values;
}

bool CsvReader::readlineInto(StringVec & out){
    out.clear();

    string line;
    if(std::getline(_csvFile, line)){
        out = split(line, _delimiter);

        for (auto & value: out) {
            trim(value);
        }

        return true;
    }

    return false;
}

// Adapted from: https://stackoverflow.com/a/46931770
//
StringVec CsvReader::split(const std::string & s, const std::string & delimiter)
{
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

// Adapted from: https://stackoverflow.com/a/6500499
//
std::string & CsvReader::trim(std::string& str)
{
    str.erase(str.find_last_not_of(' ')+1);         //suffixing spaces
    str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
    return str;
}
