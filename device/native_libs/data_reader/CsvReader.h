#ifndef DATAREADER__CSVREADER_H
#define DATAREADER__CSVREADER_H

#include "ArduinoFramework.h"

#include <string>
#include <vector>
#include <fstream>
#include <map>

class CsvReader{
public:
    using StringVec = std::vector<std::string>;
public:
    virtual ~CsvReader();
public:
    void open(const String & path, const String & delim=",");
    void open(const std::string & path, const std::string & delim=",");
    void open(const char * path, const char * delim=",");
    void close();

public:
    bool next();
    const StringVec & values() const{ return _values;}

    long getLong(const std::string & col) const;
    double getDouble(const std::string & col) const;
    std::string getString(const std::string & col) const;

public:
    const StringVec & columns() const{  return _columns; }
    bool hasColumn(const std::string & col) const;

protected:
    StringVec readline();
    bool readlineInto(StringVec & out);
    StringVec split(const std::string & s, const std::string & delimiter);
    std::string & trim(std::string& str);

protected:
    StringVec _columns, _values;
    std::map<std::string, int> columnsToIndex;

    std::string _delimiter;
    std::ifstream _csvFile;
};

#endif