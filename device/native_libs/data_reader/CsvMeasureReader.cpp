#include "CsvMeasureReader.h"
#include "CsvReader.h"


std::vector<double> CsvMeasureReader::readDoubleMeasures(
    const std::string & file, const std::string & measureColumn, const std::string & delim)
{
    CsvReader csv;

    csv.open(file, delim);

    std::vector<double> measures;

    if(!csv.hasColumn(measureColumn)){
        return {}; //should throw
    }

    while(csv.next()){
        auto value = csv.getDouble(measureColumn);

        measures.push_back(value);
    }

    return measures;
}