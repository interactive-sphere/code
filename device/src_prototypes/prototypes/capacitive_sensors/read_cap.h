#include "Arduino.h"
#include <CapacitiveSensor.h>

#define TIMEOUT_MILLIS 30

#if defined(ARDUINO_ARCH_ESP8266)
#define SEND_PIN D2
#define RCV_PIN D1

#elif defined(ARDUINO_ARCH_ESP32)
#define SEND_PIN 22
#define RCV_PIN 23

#endif

CapacitiveSensor c_sense = CapacitiveSensor(SEND_PIN,RCV_PIN); // 10 megohm resistor between pins 4 & 2, pin 2 is sensor pin, add wire, foil

long minValue = 0x0FFFFFFFL;
long maxValue = -minValue;

uint count = 0;
long valueSum = 0;
long timeSum = 0;
float percentSum = 0;

long startTimer = 0;

void setup(){
  //c_sense.set_CS_AutocaL_Millis(0xFFFFFFFF); // turn off autocalibrate on channel 1 - just as an example
  Serial.begin(9600);
  Serial.println("Start!");

  c_sense.set_CS_Timeout_Millis(TIMEOUT_MILLIS);
}

void printValues(long deltaTime, long value, long delta, float percent){
    Serial.print(deltaTime); // check on performance in milliseconds
    Serial.print('\t');

    Serial.print("\tr"); // tab character for debug window spacing
    Serial.print(value); // print sensor output 1

    Serial.print("\td");
    Serial.print(delta);

    Serial.print('\t');
    Serial.print(percent * 100);

    Serial.println();
}

void loop(){
  long start = millis();
  // if (count == 0) {
  //   startTimer = millis();
  // }

  long value = c_sense.capacitiveSensorRaw(30);
  long deltaTime = millis() - start;

  if (value < minValue) {
    minValue = value;
  }

  if (value > maxValue) {
    maxValue = value;
  }

  long range = (maxValue - minValue);
  long delta = (value - minValue);
  float percent = delta/(float)range;


  valueSum += value;
  timeSum += deltaTime;
  percentSum += percent;

  if (++count == 10) {

    printValues(
      timeSum,
      valueSum/count,
      (valueSum/count) - minValue,
      percentSum/count);

    count = 0;
    timeSum = 0;
    valueSum = 0;
    percentSum = 0;
  }



  delay(10); // arbitrary delay to limit data to serial port

}
