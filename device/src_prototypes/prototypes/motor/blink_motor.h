#include <Arduino.h>

#define TIME 500

//#define MOTOR_PIN LED_BUILTIN
// #define MOTOR_PIN D2
#define MOTOR_PIN 32

void setup() {
  pinMode(MOTOR_PIN, OUTPUT);
}

void loop() {
  // turn the LED on (HIGH is the voltage level)
  digitalWrite(MOTOR_PIN, HIGH);

  // wait for a second
  delay(TIME);

  // turn the LED off by making the voltage LOW
  digitalWrite(MOTOR_PIN, LOW);

   // wait for a second
  delay(TIME*3);
}
