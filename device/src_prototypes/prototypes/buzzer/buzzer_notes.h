#include "Arduino.h"

//reference: https://techtutorialsx.com/2017/07/01/esp32-arduino-controlling-a-buzzer-with-pwm/

#define OUT_PIN 12

int freq = 2000;
int channel = 0;
int resolution = 8;

#define NOTE_SPACE NOTE_MAX

const int num_notes = 8;
const note_t melody[] = {NOTE_C, NOTE_G, NOTE_A, NOTE_G, NOTE_SPACE, NOTE_A, NOTE_B, NOTE_C};
const int octaves[] = {4, 3, 3, 3, 3, 0, 3, 4};

// int melody[] = {
//   NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
// };

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

void stopNote(int channel){
    ledcWrite(channel, 0);
}

void playNote(int channel, note_t note, int octave, int duration){
    if (note != NOTE_SPACE) {
        ledcWriteNote(channel, note, octave);
    }
    else{
        stopNote(channel);
    }
    delay(duration);
    stopNote(channel);
}


void play_melody(int channel) {
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < num_notes; thisNote++) {

    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    playNote(channel, melody[thisNote], octaves[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    stopNote(channel);
  }
}

void setup() {
  Serial.begin(115200);
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(OUT_PIN, channel);
}

void loop() {


    // for(uint8_t octave = 1; octave < 8; ++octave){
    //
    //
    // for (int i = 0; i < num_notes; ++i){
    //     note_t n = notes[i];
    //
    //     Serial.println(n);
    //
    //     ledcWriteNote(channel, n, octave);
    //     delay(500);
    //     ledcWrite(channel, 0);
    //     delay(500);
    // }
    //
    //     delay(1000);
    // }

    play_melody(channel);
}
