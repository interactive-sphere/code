# Circuit to use buzzer (5V)

## ESP32

### Components

| Component label  | Description |
| ---------------  | ------------ |
| T1               |   An NPN transistor (used 2N2222A transistor). |
| B1               |   A (passive) buzzer.         |
| R1               |   A resistor (tested with 1 KOhm) used to protect the ESP from over-voltage (in the transistor base).|
| R2               |   A resistor to limit the current to the buzzer (tested with 120 Ohm). Maybe unnecessary (reduce volume). |


### Circuit:

```
D12* -->   R1           -> T1.base
VIN  -->   R2   -> +B1- -> T1.collector
                           T1.emitter   -> GND
```

> \*   **Note**:  Can be any output pin (below 34).


### Pins connection

| ESP32 pins  | components            | Transistor
| ----------  | --------------------- | ------------- |
| D12*        |   R1                  |  T1.base      |
| VIN         |   R2 -> +B1-          |  T1.collector |
| GND         |                       |  T1.emmiter   |
