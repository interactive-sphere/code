#include <Arduino.h>

#define TIME 2000

//#define LED_PIN LED_BUILTIN
// #define LED_PIN D2
#define LED_PIN 2

void setup() {
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  // turn the LED on (HIGH is the voltage level)
  digitalWrite(LED_PIN, HIGH);

  // wait for a second
  delay(TIME*2);

  // turn the LED off by making the voltage LOW
  digitalWrite(LED_PIN, LOW);

   // wait for a second
  delay(TIME);
}
