#include <ArduinoFramework.h>

#include <BluetoothSerial.h>

#include "SphereDevice.h"
#include "log.h"

BluetoothSerial btSerial;
SphereDevice device{&btSerial};

void setup() {
    Serial.begin(115200);
    Serial.println("starting...");

    // Serial.setDebugOutput(true);
    // esp_log_level_set("*", ESP_LOG_VERBOSE);

    if(btSerial.begin(DEFAULT_DEVICE_NAME)){
        Serial.println("Bluetooth API started");
    }

    device.begin();
}

void loop() {
    device.setConnected(btSerial.hasClient());
    device.update();
}
