#include <unity.h>

#include "ArduinoFramework.h"

#include "PressureSensorTests.h"

void setup(){
    UNITY_BEGIN();
    RUN_TEST(test_raw_reader);
    RUN_TEST(test_mux_reader);

    RUN_TEST(test_pressure_sensor);
    UNITY_END();
}

void loop()
{}
