#ifndef PRESSURE_SENSOR_TESTS_H
#define PRESSURE_SENSOR_TESTS_H

#include "unity.h"

#include "BoardPinout.h"

#include "reader/CapSenseReader.h"
#include "MuxReader.h"
#include "PressureSensorReader.h"

#include "TimeCounter.h"
#include "FakeCapReader.h"

#include <cmath>

#define DEFAULT_MUX_CHANNEL 2

class PressureSensorReaderTests{
public:
    PressureSensorReaderTests()
    : mux(MUX_S0_PIN, MUX_S1_PIN, MUX_S2_PIN, MUX_S3_PIN)
    , capSenseReader(CAPSENSE_RCV_PIN, CAPSENSE_SND_PIN)
    , muxReader(DEFAULT_MUX_CHANNEL, &mux, &capSenseReader)
    {}

    void test_raw_reader(){
        mux.channel(DEFAULT_MUX_CHANNEL);

        auto readedValue = capSenseReader.read();

        Serial.print("CapSense raw value (channel ");
        Serial.print(DEFAULT_MUX_CHANNEL);
        Serial.print("): ");
        Serial.println(readedValue);

        TEST_ASSERT_GREATER_THAN_MESSAGE(0, readedValue, "CapSenseRead is invalid!");
    }

    void test_mux_reader(){
        auto readedValue = muxReader.read();

        mux.channel(DEFAULT_MUX_CHANNEL);
        auto otherValue = capSenseReader.read();

        Serial.print("MuxReader raw value (channel ");
        Serial.print(muxReader.getChannel());
        Serial.print("): ");
        Serial.println(readedValue);

        TEST_ASSERT_GREATER_THAN_MESSAGE(0, readedValue, "MuxReader read value is invalid!");

        auto diff = abs(readedValue - otherValue);
        float relativeDiff = diff/readedValue;

        TEST_ASSERT_LESS_THAN_MESSAGE(5.0f, relativeDiff, "Readed value should be near the expected");
    }

    void test_pressure_sensor(){
        FakeCapReader fakeReader({
            112, 29, 91, 38, 95, 75, 43, 88, 51, 102, 31,
            83, 31, 89, 81, 87, 32, 105, 30, 90, 117, 97,

            270, 225, 271, 280, 324, 213, 277, 278, 257,
            196, 254, 240, 274, 206, 302, 213, 260, 210,

            84, 34, 83, 55, 92, 24, 80, 32, 90, 40, 91,
            31, 130, 36, 90, 91, 38, 93, 31, 72, 28, 83});

        int averageWindow = AVERAGE_WINDOW_SIZE;
        int medianWindow  = MEDIAN_WINDOW_SIZE;
        PressureSensorReader sensor(&fakeReader,
            medianWindow,
            averageWindow
        );

        TEST_ASSERT_FALSE_MESSAGE(
            sensor.update(), "Update should return true only when data is ready");
        TEST_ASSERT_EQUAL_MESSAGE(1, fakeReader.count,
            "Update should read a value");
        TEST_ASSERT_EQUAL_MESSAGE(0, sensor.current(),
            "Should return invalid value until data is ready");


        auto readValue = sensor.read();
        TEST_ASSERT_EQUAL_MESSAGE(
            std::max(medianWindow, averageWindow), fakeReader.count,
            "Read should call update until data is ready");
        TEST_ASSERT_EQUAL_MESSAGE(readValue, sensor.current(),
            "Read should return current value");
        TEST_ASSERT_GREATER_THAN_MESSAGE(0, sensor.current(),
            "Current value is invalid");

        TEST_ASSERT_LESS_THAN_MESSAGE(fakeReader.values.size(), fakeReader.count,
            "Readed values should not have exceeded the maximum");

        int remaining = fakeReader.values.size() - fakeReader.count;
        for (int i=0; i < remaining; ++i){
            TEST_ASSERT_TRUE(sensor.update());
            // Serial.print(sensor.current());
            // Serial.print("\t");
            // Serial.print(sensor.relative());
            // Serial.print("%");
            // Serial.print("\t(min: ");
            // Serial.print(sensor.rangeFilter.min());
            // Serial.print(", max: ");
            // Serial.print(sensor.rangeFilter.max());
            // Serial.print(")");
            // Serial.println();

            TEST_ASSERT_LESS_OR_EQUAL_MESSAGE(1.0f, sensor.relative(),
                "Relative value is greater than maximum");
            TEST_ASSERT_GREATER_OR_EQUAL_MESSAGE(0.0f, sensor.relative(),
                "Relative value is less than minimum");
        }

        TEST_ASSERT_EQUAL_MESSAGE(
            fakeReader.values.size(), fakeReader.count,
            "All sensors values should have been readed"
        );
    }

    void showValue(PressureSensorReader & sensor, int timeout=100){
        TimeCounter c{timeout};
        while(!c.completed()){
            Serial.print(sensor.read());
            // Serial.print(", ");
            Serial.print("\t");
            Serial.print(sensor.relative());
            Serial.print("%");
            Serial.println();
        }
        Serial.println();
    }

protected:
    Multiplexer mux;
    CapSenseReader capSenseReader;
    MuxReader muxReader;
};

void test_raw_reader(){
    PressureSensorReaderTests().test_raw_reader();
}

void test_mux_reader(){
    PressureSensorReaderTests().test_mux_reader();
}

void test_pressure_sensor(){
    PressureSensorReaderTests().test_pressure_sensor();
}

#endif
