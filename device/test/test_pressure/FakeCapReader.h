#ifndef FAKE_CAPREADER_H
#define FAKE_CAPREADER_H

#include "reader/CapReader.h"

#include <vector>

class FakeCapReader: public CapReader {
public:
    FakeCapReader (std::vector<long> values)
        : values(values)
        , count(0)
    {}

    long read() override{
        int idx = count % values.size();
        ++count;

        return values[idx];
    }

public:
    std::vector<long> values;
    unsigned int count;
};

#endif
