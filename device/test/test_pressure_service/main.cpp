#include "unity.h"

#include "Arduino.h"

#include "TimeCounter.h"
#include "CapPressureServices.h"

#include "log.h"

#define TIMEOUT 15000 //15 s

TimeCounter timeCounter{TIMEOUT};
CapPressureServices pressureServices{{{
    { 15, {15}},
    { 13, {13}},
    {  0, {0}}
}}};

void test_setup();
void test_events();

void setup(){
    UNITY_BEGIN();
    test_setup();
    test_events();
    UNITY_END();
}

void loop(){}

int count_press_events=0;
int count_release_events=0;

void onEvent(const pressure::Event & evt){
    switch (evt.evtType){
    case pressure::EventType::Press:
        log_info("Pressed: ");
        ++count_press_events;
        break;
    case pressure::EventType::Release:
        log_info("Released: ");
        ++count_release_events;
        break;
    default:
        log_info("Unknown event type: ");
    }
    log_info("sensor: %d, raw: %ld, relative: %f%%\n",
        evt.sensorId,
        evt.rawValue,
        evt.percentValue
    );
}

void test_setup(){
    pressureServices.listenPressureEvents(onEvent);

    pressureServices.begin();
}
void test_events(){
    timeCounter.reset();
    while(!timeCounter.completed()){
        pressureServices.update();
    }
}
