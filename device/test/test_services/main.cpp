#include <unity.h>

#include "ArduinoFramework.h"

#include "SphereServices.h"
#include "NeoPixelLedServices.h"
#include "VibrationMotor.h"
#include "BuzzerSoundServices.h"

#include "BoardPinout.h"

#include "TimeCounter.h"

#include "tests.h"

NeoPixelLedServices<> ledServices{LEDRGB_COUNT, LEDRGB_PIN};
VibrationMotor vibration{VIBRATION_PIN};
BuzzerSoundServices buzzer{BUZZER_PIN, BUZZER_CHANNEL};

SphereServices services{{
  &ledServices,
  &vibration,
  &buzzer
}};

//tests


void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);

  services.begin();

  UNITY_BEGIN();
  RUN_TEST(test_services_config);
  RUN_TEST(test_ledrgb);
  RUN_TEST(test_vibration);
  vibration.stop();
  RUN_TEST(test_buzzer_simple);
  RUN_TEST(test_buzzer_melody);

  UNITY_END();
}

void loop(){
  test_ledrgb();
  delay(1000);
}

void test_services_config(){
  TEST_ASSERT_EQUAL(&ledServices, services.led());
  TEST_ASSERT_EQUAL(&vibration  , services.vibration());
}

void test_ledrgb(){
  TEST_ASSERT_NOT_NULL(services.led());

  services.led()->off();

  services.led()->changeColor({255, 0, 0});
  delay(500);
  services.led()->changeColor({0, 255, 0});
  delay(500);
  services.led()->changeColor({0, 0, 255});
  delay(500);

  services.led()->off();
}

void test_vibration(){
  auto v = services.vibration();
  TEST_ASSERT_NOT_NULL(v);

  //Start vibration
  v->vibrate(200);
  TEST_ASSERT_TRUE_MESSAGE(v->isVibrating(), "Should be vibrating");

  //Wait time complete
  delay(210);
  TEST_ASSERT_TRUE_MESSAGE(v->isVibrating(), "Still should be vibrating");

  //Update
  v->update();
  TEST_ASSERT_FALSE_MESSAGE(v->isVibrating(), "The vibration should have finished");
}
