#include "unity.h"
#include "ArduinoFramework.h"

#include "BoardPinout.h"

#include "GyroscopeServices.h"

#include "Gyroscope.h"
#include "TimeCounter.h"

using namespace std::placeholders;

void test_gyroscope();
void test_gyroscope_services();

void setup(){
  UNITY_BEGIN();
  RUN_TEST(test_gyroscope);
  RUN_TEST(test_gyroscope_services);
  UNITY_END();
}

void loop(){}

class GyroscopeFixture {
public:
  GyroscopeFixture()
    : gyroServices{GYRO_SCL_PIN, GYRO_SDA_PIN, GYRO_INTERRUPT_PIN}
    , receivedMeasure(false)
  {}

  void test_gyroscope(){
    configure();
    test_measure();
  }

  void test_gyroscope_services(){
      if (!gyroServices.ready()){
          configure();
      }

      test_listening_orientation();
  }

  void configure(){
    gyroServices.begin();
    TEST_ASSERT_TRUE_MESSAGE(gyroServices.ready(), "Failed initialization");

    // gyro.calibrate();
    gyroServices.calibrate({0,0,-1});
    gyroServices.gyro().getMPU().PrintActiveOffsets();
  }

  void test_measure(){
    gyroServices.gyro().listen(std::bind(&GyroscopeFixture::onMeasure, this, _1));

    waitMeasure(2000);

    printMeasure();
  }

  void test_listening_orientation(){
      waitOrientationValue(OrientationFormat::Quaternion, 2000);
      checkQuaternion();

      waitOrientationValue(OrientationFormat::Euler, 2000);
      check3DAngle("Euler");

      waitOrientationValue(OrientationFormat::YawPitchRow, 2000);
      check3DAngle("YawPitchRow");
  }

  void onMeasure(const GyroscopeMeasure & measure){
    this->measure = measure;
    this->receivedMeasure = true;

    Serial.println("Got measure!");
  }

  void onOrientationChange(const Vector<float> & values){
      receivedData.assign(values.data(), values.data()+values.size());
      receivedMeasure = true;
  }

  void waitMeasure(int timeout){
    Serial.print("Waiting until ");
    Serial.print(timeout);
    Serial.println("ms for gyroscope measure");

    receivedMeasure = false;

    TimeCounter counter;
    counter.resetTimer(timeout);

    while(!counter.completed() && !receivedMeasure){
      gyroServices.update();
      delay(1);
    }

    TEST_ASSERT_TRUE_MESSAGE(receivedMeasure, "Timeout occurred before receive gyroscope measure");
  }

  void waitOrientationValue(OrientationFormat format, int timeout){
      gyroServices.listenOrientation(
        std::bind(&GyroscopeFixture::onOrientationChange, this, _1),
        format);

      waitMeasure(timeout);
  }

public:
    void checkQuaternion(){
        TEST_ASSERT_EQUAL_MESSAGE(4, receivedData.size(),
            "Quaternion data is invalid. Should have 4 values");

        Quat q{receivedData.data()};
        Serial.print("quat: "); Serial.println(q.toString());
    }

    void check3DAngle(const String & format){
        auto msg = format + " data is invalid. Should have 3 values";
        TEST_ASSERT_EQUAL_MESSAGE(3, receivedData.size(), msg.c_str());

        Vector3D<float> vec{receivedData.data()};
        Serial.print(format + ": "); Serial.println(vec.toString());
    }

public:
  void printMeasure(){
    if (!receivedMeasure) {
      return;
    }

    Quat quaternion;
    Vector3D<float> euler, yawPitchRow;
    Vector3D<int16_t> realAccel, worldAccel;

    auto & gyro = gyroServices.gyro();

    gyro.getQuaternion(measure, quaternion);
    gyro.getEuler(measure, euler);
    gyro.getYawPitchRow(measure, yawPitchRow);
    gyro.getRealAccel(measure, realAccel);
    gyro.getWorldAccel(measure, worldAccel);

    Serial.print("raw measure: ");
    PrintHex8(measure.buffer(), measure.size());

    Serial.print("quaternion\t");
    Serial.println(quaternion.toString());

    Serial.print("euler\t");
    Serial.println(euler.toString());

    Serial.print("yaw pitch roll\t");
    Serial.println(yawPitchRow.toString());

    Serial.print("real accel\t");
    Serial.println(realAccel.toString());

    Serial.print("world accel\t");
    Serial.println(worldAccel.toString());
  }

  //from: https://forum.arduino.cc/index.php?topic=38107.msg282342#msg282342
  void PrintHex8(uint8_t *data, uint8_t length){
     char tmp[length*2+1];
     byte first;
     byte second;
     for (int i=0; i<length; i++) {
           first = (data[i] >> 4) & 0x0f;
           second = data[i] & 0x0f;
           // base for converting single digit numbers to ASCII is 48
           // base for 10-16 to become lower-case characters a-f is 87
           // note: difference is 39
           tmp[i*2] = first+48;
           tmp[i*2+1] = second+48;
           if (first > 9) tmp[i*2] += 39;
           if (second > 9) tmp[i*2+1] += 39;
     }
     tmp[length*2] = 0;
     Serial.println(tmp);
   }

protected:
  GyroscopeServices gyroServices;

  bool receivedMeasure;
  GyroscopeMeasure measure;

  std::vector<float> receivedData;
};

void test_gyroscope(){
  GyroscopeFixture().test_gyroscope();
}

void test_gyroscope_services(){
  GyroscopeFixture().test_gyroscope_services();
}
