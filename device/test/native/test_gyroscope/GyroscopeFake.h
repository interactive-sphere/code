#pragma once

#include "Gyroscope.h"

#include <vector>
#include <list>

class GyroscopeFake: public Gyroscope{
public:
    using Vec3DInt = Vector3D<int16_t>;

public:
    GyroscopeFake();

public:

public:
    void notifyMeasure();

    void nextRealAccels(const std::vector<Vec3DInt> & nextMeasures);
    void nextRawAccels(const std::vector<Vec3DInt> & nextMeasures);
    void nextRawGyro(const std::vector<Vec3DInt> & nextMeasures);

public:
    bool update(){return true;}
    bool begin(){ return true;}
    bool ready() const{ return true;}
    void calibrate(int rounds=14){}
    void calibrate(const Vector3D<float> & gravity, int rounds=14){}
public:

    void listen(GyroscopeListener listener){
        this->listener = listener;
    }
    void stopListening(){listen({});};

    Quat & getQuaternion(const GyroscopeMeasure & measure, Quat & out){
        return out;
    }
    Vector3D<float> & getEuler(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true)
    {
        return out;
    }
    Vector3D<float> & getYawPitchRow(const GyroscopeMeasure & measure,
                              Vector3D<float>        & out,
                              bool                     inDegrees=true)
    {
        return out;
    }

    /** real acceleration, adjusted to remove gravity */
    Vector3D<int16_t> & getRealAccel(const GyroscopeMeasure & measure,
                                     Vector3D<int16_t>      & out)
    {
        return getNextReading(realAccels, out);
    }

    Vector3D<int16_t> & getWorldAccel(
        const GyroscopeMeasure & measure, Vector3D<int16_t> & out)
    {
        return out;
    }

    Vector3D<int16_t> & getRawAccel(const GyroscopeMeasure & measure,
                                    Vector3D<int16_t>      & out)
    {
        return getNextReading(rawAccels, out);
    }

    Vector3D<int16_t> & getRawGyro(const GyroscopeMeasure & measure,
                                   Vector3D<int16_t>      & out)
    {
        return getNextReading(this->rawGyro, out);
    }

    void getRawAccelGyro(const GyroscopeMeasure & measure,
                                 Vector3D<int16_t>  * accelOut = nullptr,
                                 Vector3D<int16_t>  * gyroOut = nullptr)
    {
        if(accelOut != nullptr){
            getRawAccel(measure, *accelOut);
        }
        if(gyroOut != nullptr){
            getRawGyro(measure, *gyroOut);
        }
    }
    void radiansToDegree(Vector3D<float> & vec){}


protected:
    template<typename SrcList, typename OutList>
    void extend(OutList & vec, const SrcList & data){
        for(auto & value: data){
            vec.push_back(value);
        }
    }

    template<typename SrcList, typename Vec>
    Vec & getNextReading(SrcList & src, Vec & out){
        if(!src.empty()){
            out = src.front();
            src.pop_front();
        }

        return out;
    }

protected:
    std::list<Vec3DInt> rawAccels, realAccels, rawGyro;

protected:
    GyroscopeListener listener;
    GyroscopeMeasure tmpMeasure;
};

