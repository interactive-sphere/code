#pragma once


#include "unity.h"


#include "BaseGyroscopeServices.h"

#include "GyroscopeFake.h"
#include "OrientationListenerHelper.h"

#include <vector>


class GyroscopeServicesTests {
public:
    using Vec = OrientationListenerHelper::Vec;

public:
    static void runTests();

public:
    GyroscopeServicesTests();

    void test_listen_AccelGyro();

public:
    void checkVectorEqual(const Vec & actual, const Vec & expected);

public:
    GyroscopeFake gyro;
    BaseGyroscopeServices gyroService;
    OrientationListenerHelper  listener;
};

