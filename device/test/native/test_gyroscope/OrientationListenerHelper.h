#pragma once

#include "OrientationServices.h"

#include "3dmath/DynamicVector.hpp"
#include <list>


class OrientationListenerHelper{
public:
    using Vec = DynamicVector<float>;

public:
    OrientationListener orientationListener(){
        return [this](const Vector<float> & values){
            this->onOrientation(values);
        };
    }

    bool hasReadings() const{ return !readings.empty(); }
    Vec nextReading(){
        if(!hasReadings()) return {};

        auto value = readings.front();
        readings.pop_front();

        return value;
    }

protected:
    void onOrientation(const Vector<float> & values){
        readings.push_back(values);
    }

public:
    std::list<Vec> readings;
};