#include "GyroscopeServicesTests.h"

#include "ArduinoFramework.h"
#include <vector>

#include "log.h"
#include "unity_class_tests.h"

#include "ArduinoFake.h"

#include "FakeMillis.h"

using namespace std;
using namespace fakeit;

using TestClass=GyroscopeServicesTests;


void TestClass::runTests(){
    FakeMillis::useSystemTime();

    RUN_CLASS_TEST(GyroscopeServicesTests, test_listen_AccelGyro);
}

TestClass::GyroscopeServicesTests()
    : gyro()
    , gyroService(&gyro)
{
}


void TestClass::test_listen_AccelGyro(){
    gyroService.listenOrientation(
        listener.orientationListener(), {OrientationFormat::AccelGyro});

    gyro.nextRealAccels({{1, 2, 3}});
    gyro.nextRawGyro({{4, 5, 6}});
    gyro.notifyMeasure();

    TEST_ASSERT_TRUE_MESSAGE(listener.hasReadings(), "No measure received");

    auto measure = listener.nextReading();
    checkVectorEqual(measure, {{1, 2, 3, 4, 5, 6}});
}


void TestClass::checkVectorEqual(const Vec & actual, const Vec & expected){
    TEST_ASSERT_EQUAL_MESSAGE(expected.size(), actual.size(), "Vectors should have same size");

    TEST_ASSERT_EQUAL_FLOAT_ARRAY_MESSAGE(
        expected.data(), actual.data(), expected.size(),
        "Vectors should be equal"
    );
}

