#include "GyroscopeFake.h"

using namespace fakeit;

GyroscopeFake::GyroscopeFake(){
}


void GyroscopeFake::notifyMeasure(){
    if(listener){
        listener(tmpMeasure);
    }
}


void GyroscopeFake::nextRealAccels(const std::vector<Vec3DInt> & nextMeasures){
    extend(realAccels, nextMeasures);
}

void GyroscopeFake::nextRawAccels(const std::vector<Vec3DInt> & nextMeasures){
    extend(rawAccels, nextMeasures);
}
void GyroscopeFake::nextRawGyro(const std::vector<Vec3DInt> & nextMeasures){
    extend(rawGyro, nextMeasures);
}