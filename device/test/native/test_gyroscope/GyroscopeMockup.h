#pragma once

#include "ArduinoFake.h"


#include "Gyroscope.h"


#include <vector>

class GyroscopeMockup{
public:
    // using Mock<T> = fakeit::Mock<T>;
    // using namespace fakeit;
    using Vec3DInt = Vector3D<int16_t>;

public:
    GyroscopeMockup();

public:
    fakeit::Mock<Gyroscope> & mock(){ return gyro; }
    Gyroscope               & get (){ return gyro.get(); }

public:
    void nextRawAccels(const std::vector<Vec3DInt> & nextMeasures);
    void nextRawGyro(const std::vector<Vec3DInt> & nextMeasures);

protected:
    void setup();

    template<typename T>
    void extend(std::vector<T> & vec, const std::vector<T> & data){
        for(auto & value: data){
            vec.push_back(value);
        }
    }

protected:
    std::vector<Vec3DInt> rawAccels, rawGyro;

protected:
    fakeit::Mock<Gyroscope> gyro;
    GyroscopeListener listener;
};

