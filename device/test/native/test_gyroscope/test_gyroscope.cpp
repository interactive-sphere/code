#include <unity.h>

#include <ArduinoFake.h>
#include "ArduinoFramework.h"

#include "GyroscopeServicesTests.h"


int main()
{
    UNITY_BEGIN();

    GyroscopeServicesTests::runTests();

    UNITY_END();
}
