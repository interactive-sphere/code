#include "GyroscopeMockup.h"

using namespace fakeit;

GyroscopeMockup::GyroscopeMockup(){
    setup();
}

void GyroscopeMockup::setup(){
    When(Method(gyro, listen)).AlwaysDo([this](GyroscopeListener listener){
        this->listener = listener;
    });
}


void GyroscopeMockup::nextRawAccels(const std::vector<Vec3DInt> & nextMeasures){
    extend(rawAccels, nextMeasures);
}
void GyroscopeMockup::nextRawGyro(const std::vector<Vec3DInt> & nextMeasures){
    extend(rawGyro, nextMeasures);
}