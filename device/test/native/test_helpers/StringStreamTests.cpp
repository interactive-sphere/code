#include "StringStreamTests.h"

#include "log.h"

#include "StringStream.h"


void run_test_print_string()
{ StringStreamTests().test_print_string();}


void StringStreamTests::runTests(){
    RUN_TEST(run_test_print_string);
}

StringStreamTests::StringStreamTests()
{}


void StringStreamTests::test_print_string(){
    String hello = "hello";
    StringStream stream;

    stream.print(hello);

    TEST_ASSERT_EQUAL_STRING(hello.c_str(), stream.outStr().c_str());
}
