#include <unity.h>

#include <ArduinoFake.h>
#include "ArduinoFramework.h"

#include "StringStreamTests.h"

int main()
{
    UNITY_BEGIN();

    StringStreamTests::runTests();

    UNITY_END();
}
