#ifndef HELPERS_STRINGSTREAM_TESTS_H
#define HELPERS_STRINGSTREAM_TESTS_H

#include "unity.h"

#include "ArduinoFake.h"


class StringStreamTests {
public:
    static void runTests();

public:
    StringStreamTests();

public:
    void test_print_string();
};

#endif
