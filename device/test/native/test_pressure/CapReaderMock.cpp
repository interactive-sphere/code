#include "CapReaderMock.h"

#include "log.h"


using namespace fakeit;


CapReaderMock::CapReaderMock()
{
    setupReader();
}

void CapReaderMock::setupReader(){
    When(Method(reader, read)).AlwaysDo([this](){
        return nextReading();
    });
}


void CapReaderMock::setNextReadings(ReadingsVec readings){
    this->sensorReadings = std::move(readings);
    this->readingsCount = 0;
}

void CapReaderMock::verifyReadCalled(){
    Verify(Method(reader, read)).AtLeastOnce();
}

long CapReaderMock::nextReading(){
    if(hasReadings()){
        auto value = sensorReadings[readingsCount];
        ++readingsCount;

        // log_info("CapReaderMock::nextReading: return value: %ld\n", value);

        return value;
    }

    return 0;
}

bool CapReaderMock::hasReadings() const{
    return this->readingsCount < sensorReadings.size();
}
