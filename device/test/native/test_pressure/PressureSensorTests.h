#ifndef PRESSURE_SENSOR_TESTS_H
#define PRESSURE_SENSOR_TESTS_H

#include "unity.h"

#include "ArduinoFake.h"

#include "PressureSensor.h"
#include "CapReaderMock.h"


using fakeit::Mock;

class PressureSensorTests {
public:
    static void runTests();

public:
    PressureSensorTests();

public:
    void test_read_measure();

protected:
    CapReaderMock reader;
    PressureSensor sensor;
};

#endif
