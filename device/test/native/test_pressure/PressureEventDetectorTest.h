#ifndef PRESSURE_EVENT_DETECTOR_TESTS_H
#define PRESSURE_EVENT_DETECTOR_TESTS_H

#include "unity.h"

#include "PressureEventDetector.h"

#include <vector>


class PressureEventDetectorTests {
public:
    static void runTests();

public:
    PressureEventDetectorTests();

public:
    void test_press();
    void test_press_and_release__from_measure();

protected:
  void setThresholds(float releaseThreshold, float pressThreshold);
  void setThresholds(float releaseThreshold, float pressThreshold,
    int debounceRepetitions);

  void afterMeasures(const std::vector<pressure::Measure> & measures);

  void stateShouldBe(pressure::EventType evtType) const;
  void shouldDetect(const std::vector<pressure::EventType> & expectedEvts) const;

protected:
  std::vector<const char*> eventTypesStrings(
    const std::vector<pressure::Event> & events) const;
  std::vector<const char*> eventTypesStrings(
    const std::vector<pressure::EventType> & eventTypes) const;

  std::vector<pressure::Measure> makeMeasuresFromRelative(
      const std::vector<float> & measures,
      long measuresRange = 600,
      long measuresMinimum = 7650) const;

protected:
    PressureEventDetector detector;
    std::vector<pressure::Event> lastEvents;
};

#endif
