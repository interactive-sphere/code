#include "PressureEventDetectorTest.h"

#include "data/readings.hpp"
#include "log.h"

#include "ArduinoFramework.h"
#include <vector>

using namespace std;
using namespace pressure;

using TestClass=PressureEventDetectorTests;


void run_test_press()
{ TestClass().test_press();}

void run_test_press_and_release__from_measure()
{ TestClass().test_press_and_release__from_measure();}


void TestClass::runTests(){
    RUN_TEST(run_test_press);
    RUN_TEST(run_test_press_and_release__from_measure);
}

TestClass::PressureEventDetectorTests()
  : detector()
{
    detector.onPressureEvent([this](const Event & evt){
      this->lastEvents.push_back(evt);
    });
}


// #define PRESSURE_RELEASE_THRESHOLD 0.4
// #define PRESSURE_PRESS_THRESHOLD 0.65
// #define PRESSURE_DEBOUNCE_THRESHOLD 30

void TestClass::test_press(){
    setThresholds(
      0.4f, //release
      0.55f, //press
      4 //count threshold
    );

    auto measures = makeMeasuresFromRelative({
      0.1f, 0.2f, 0.3f, 0.4f, //idle
      //press:
      0.55f, 0.56f, 0.6f, 0.7f, 0.6f
    });

    afterMeasures(measures);

    stateShouldBe(EventType::Press);
    shouldDetect({EventType::Press});
}

void TestClass::test_press_and_release__from_measure(){
    setThresholds(
      /*release = */ 0.4,
      /*press = */ 0.65,
      /*debounce = */ 8);

    auto measures = makeMeasuresFromRelative(
      pressure::press_release_paper_sensor__rel);

    afterMeasures(measures);

    shouldDetect({EventType::Press, EventType::Release});
    stateShouldBe(EventType::Release);
}


void TestClass::setThresholds(float releaseThreshold, float pressThreshold){
  setThresholds(
    releaseThreshold, pressThreshold, detector.debounceThreshold());
}

void TestClass::setThresholds(
    float releaseThreshold, float pressThreshold, int debounceRepetitions)
{
  this->detector.lowerLevelThreshold(releaseThreshold);
  this->detector.upperLevelThreshold(pressThreshold);
  this->detector.debounceThreshold(debounceRepetitions);
}

void TestClass::afterMeasures(const vector<Measure> & measures){
    for(const auto & m: measures){
        detector.update(m);
    }
}


void TestClass::stateShouldBe(pressure::EventType evtType) const{
    auto state = detector.state();

    TEST_ASSERT_EQUAL_STRING_MESSAGE(evtType.c_str(), state.c_str(),
      "Current state does not match the expected");
}

void TestClass::shouldDetect(const vector<EventType> & expectedEvts) const{
    TEST_ASSERT_EQUAL_INT_MESSAGE(expectedEvts.size(), lastEvents.size(),
      "Number of events detected is not the expected");

    auto expectedEvtsString = eventTypesStrings(expectedEvts);
    auto lastEvtsString = eventTypesStrings(lastEvents);

    for(auto i=0; i < expectedEvtsString.size(); ++i){
      auto actual = lastEvtsString[i];
      auto expected = expectedEvtsString[i];
      auto msg = String("Detected event '") + actual + "' "
                    + "does not match the expected('" + expected + "'. "
                    + "At index: " + i;

      TEST_ASSERT_EQUAL_STRING_MESSAGE(expected, actual, msg.c_str());
    }
}

vector<const char*>
  TestClass::eventTypesStrings(const vector<Event> & events) const
{
    vector<EventType> out;

    for(const auto & evt: events){
      out.push_back(evt.evtType);
    }

    return eventTypesStrings(out);
}

vector<const char*>
  TestClass::eventTypesStrings(const vector<EventType> & events) const
{
    vector<const char*> out;

    for(const auto & evt: events){
      out.push_back(evt.c_str());
    }

    return out;
}

std::vector<Measure>  TestClass::makeMeasuresFromRelative(
    const std::vector<float> & relativeMeasures,
    long measuresRange,
    long measuresMinimum
  ) const
{
  std::vector<Measure> measures;

  for(auto & relValue: relativeMeasures){
    measures.push_back({
      0, //sensorId
      relValue,
      (long)((relValue * measuresRange) + measuresMinimum)
    });
  }

  return std::move(measures);
}
