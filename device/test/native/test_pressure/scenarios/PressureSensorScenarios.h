#ifndef PRESSURE_SENSOR_SCENARIOS_H
#define PRESSURE_SENSOR_SCENARIOS_H

#include "unity.h"

#include "ArduinoFake.h"

#include "PressureSensor.h"
#include "CapReaderMock.h"

#include <vector>
#include <string>

using fakeit::Mock;


class PressureSensorScenarios {
public:
    static void runTests();
    static void runParameterizedTests();

public:
    PressureSensorScenarios();

public:
    void test_filter_noise_after_calibration();
    void test_detect_pressure_after_calibration(
                                        const char * button, bool long_press);
    void test_detect_pressure_on_first_press(
                                        const char * button, bool long_press);

    void test_detect_pressure(
            const char * button,
            const std::string & train_type,
            const std::string & press_type);

    void test_detect_pressure__old_sensor();

protected:
    void check_detect_pressure(
        const char * button, bool withTrain=true,
        bool long_press = false, bool verbose=false);

protected:
    void reconfigureSensor(const PressureSensor::Config & config);

protected:
    void startListeningEvents();
    void stopListeningEvents();

    void configureSensor();
    void calibrateUsing(const std::vector<long> & measures);
    std::vector<long> loadMeasures(const String & filename);
    std::vector<long> loadMeasures(const std::string & filename);
    std::vector<long> loadMeasures(const char * filename);
    void receiveMeasures(const std::vector<long> & measures);
    void usingMeasures(const std::vector<long> & measures);

protected:
    void verifyEventDetection(
        const std::vector<long> & measures,
        const std::vector<pressure::EventType> & expectedEvents,
        bool verbose = false);
    void eventTypeEquals(
        pressure::EventType expected, pressure::EventType found);

protected:
    CapReaderMock reader;
    PressureSensor sensor;
    std::vector<pressure::Event> receivedEvents;
};

#endif
