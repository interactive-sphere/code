#include "fmt/core.h"


#include "scenarios/PressureSensorScenarios.h"

#include <functional>
#include <utility>
#include <vector>


#include "PressureSensorScenarios.h"
#include "log.h"
#include "ArduinoFramework.h"

#include "unity_class_tests.h"
#include "tuple_helpers.h"
#include "collections_helpers.h"
#include "test_directories.h"

#include "CsvReader.h"
#include "CsvMeasureReader.h"

using namespace fakeit;

using namespace pressure;
using namespace std;

static const std::string WITH_TRAIN = "with_train";
static const std::string FIRST_TIME = "first_time";

static const std::string SHORT_PRESS = "short_press";
static const std::string LONG_PRESS = "long_press";


void PressureSensorScenarios::runTests(){
    RUN_CLASS_TEST(PressureSensorScenarios, test_filter_noise_after_calibration);

    RUN_CLASS_TEST(PressureSensorScenarios, test_detect_pressure__old_sensor);

    runParameterizedTests();
}

void PressureSensorScenarios::runParameterizedTests(){
    for (const char * button: vector<const char *>{{
                    "top_back", "index", "middle",
                    // {"ring"}, -> not captured (not working)
                    "bottom", "topf", "center",
                    "up", "dir", "left", "down"}}){
        for (string trainType: vector<string>{{WITH_TRAIN, FIRST_TIME}}){
            for (string pressType: vector<string>{{SHORT_PRESS, LONG_PRESS}}){
                if(strcmp(button, "down") == 0
                    && trainType == FIRST_TIME
                    && pressType == SHORT_PRESS)
                {
                     //skip this combination
                    continue;
                }

                RUN_CLASS_PARAMETERIZED(
                        PressureSensorScenarios,
                        test_detect_pressure,
                        {button, trainType, pressType}
                );
            }
        }
    }
}

using TestClass = PressureSensorScenarios;

TestClass::PressureSensorScenarios()
  : reader()
  , sensor(123, &reader.get())
{
}



void TestClass::reconfigureSensor(const PressureSensor::Config & config){
    this->sensor = PressureSensor(
        sensor.id(),
        &reader.get(),
        config
    );
}

void TestClass::test_filter_noise_after_calibration(){
    auto idleMeasures = loadMeasures("pressure-recordings/center-idle.csv");
    calibrateUsing(idleMeasures);

    usingMeasures(idleMeasures);

    for (int i=0; i < idleMeasures.size(); ++i) {
        auto m = sensor.readMeasure();

        TEST_ASSERT_EQUAL_MESSAGE(
          0, m.measure,
          "for any measure in the calibrated range, should measure 0"
        );
    }
}

void TestClass::test_detect_pressure_after_calibration(
    const char * button, bool long_press)
{
    check_detect_pressure(button);
}
void TestClass::test_detect_pressure_on_first_press(const char * button, bool long_press){
    check_detect_pressure(button, false, long_press, true);
}

void TestClass::test_detect_pressure(
        const char * button,
        const std::string & train_type,
        const std::string & press_type)
{
    bool withTrain = (train_type != FIRST_TIME);
    bool longPress = (press_type == LONG_PRESS);

    check_detect_pressure(button, withTrain, longPress);
}

void TestClass::check_detect_pressure(
    const char * button, bool withTrain, bool long_press, bool verbose)
{
    log_info("testing for: %s\n", button);

    configureSensor();

    const char * press_type = long_press ? "press_long" : "press";

    // auto button = "index";
    auto idle_m = loadMeasures(fmt::format(
                        "pressure-recordings/{}-idle.csv", button));
    auto press_m = loadMeasures(fmt::format(
                    "pressure-recordings/{}-idle_{}.csv", button, press_type));

    calibrateUsing(idle_m);

    if(withTrain){
        //"train" thresholds with first press
        receiveMeasures(press_m);
    }

    verifyEventDetection(press_m, {
        EventType::Press, EventType::Release
    }, verbose);
}

void TestClass::test_detect_pressure__old_sensor()
{
    configureSensor();

    auto measures = loadMeasures("pressure_sensor_measures.csv");

    auto train = test_helpers::split(measures, 0, 300);
    auto test = test_helpers::split(measures,  300, measures.size());

    calibrateUsing(train);
    verifyEventDetection(test, {
        EventType::Press, EventType::Release,
        EventType::Press, EventType::Release,
    }, true);
}


// ---------------------------------------------------------------------------


void TestClass::configureSensor(){
    reconfigureSensor(PressureSensor::Config{
        1,    //medianWindowSize
        5,    //averageWindowSize
        0.3f, //release threshold
        0.5f, //press threshold
        3,    //debounce threshold
        0.12f //boundary factor
    });
}

void TestClass::verifyEventDetection(
    const std::vector<long> & measures,
    const std::vector<pressure::EventType> & expectedEvents,
    bool verbose)
{
    startListeningEvents();
    usingMeasures(measures);

    // log_info("loaded %ld measures\n", press_m.size());

    while(reader.hasReadings()){
        auto m = sensor.readMeasure();
        if(verbose){
            log_info("%s\n", m.toString().c_str());
        }
    }

    TEST_ASSERT_EQUAL_MESSAGE(
        expectedEvents.size(), receivedEvents.size(),
        "Number of received events."
    );

    for (int i = 0; i < expectedEvents.size(); ++i) {
        eventTypeEquals(expectedEvents[i],  receivedEvents[i].evtType);
    }

    stopListeningEvents();
}

void TestClass::eventTypeEquals(
    pressure::EventType expected, pressure::EventType found)
{
    TEST_ASSERT_EQUAL_MESSAGE(expected, found,
        (String("Expected EventType = ") + expected.c_str()
        + "; found = " + found.c_str()).c_str()
    );
}

void TestClass::startListeningEvents(){
    sensor.listenPressureEvents([this](const pressure::Event & evt){
        this->receivedEvents.push_back(evt);
    });
}

void TestClass::stopListeningEvents(){
    sensor.clearEventsListener();
}

void TestClass::calibrateUsing(const std::vector<long> & measures){
    sensor.startCalibration();
    receiveMeasures(measures);
    sensor.stopCalibration();
}



std::vector<long> TestClass::loadMeasures(const String & filename){
    return loadMeasures(filename.c_str());
}
std::vector<long> TestClass::loadMeasures(const std::string & filename){
    return loadMeasures(filename.c_str());
}
std::vector<long> TestClass::loadMeasures(const char * filename){
    auto filepath = fmt::format("{}/{}",
                            TestDirectories::testDataDir().c_str(), filename);

    log_info("load measures from file: %s\n", filepath.c_str());

    CsvMeasureReader measureReader;

    auto measures = measureReader.readMeasures<long>(filepath.c_str(), "raw");


    auto msg = fmt::format("No measure loaded in \"{}\"", filename);
    TEST_ASSERT_GREATER_THAN_MESSAGE(0, measures.size(), msg.c_str());

    return measures;
}


void TestClass::receiveMeasures(const std::vector<long> & measures)
{
    usingMeasures(measures);

    for (int i=0; i < measures.size(); ++i) {
        sensor.update();
    }
}

void TestClass::usingMeasures(const std::vector<long> & measures)
{
    reader.setNextReadings(measures);
}