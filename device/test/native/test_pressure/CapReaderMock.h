#ifndef CAPREADER_MOCK_H
#define CAPREADER_MOCK_H

#include "ArduinoFake.h"

#include <vector>

#include "reader/CapReader.h"


class CapReaderMock{
public:
    using ReadingsVec = std::vector<long>;

public:
    CapReaderMock();

public:
    fakeit::Mock<CapReader> & mock(){ return reader; }
    CapReader               & get (){ return reader.get(); }

public:
    void setNextReadings(ReadingsVec readings);
    bool hasReadings() const;

    void verifyReadCalled();

protected:
    void setupReader();
    long nextReading();

protected:
    fakeit::Mock<CapReader> reader;

    std::vector<long> sensorReadings;
    int readingsCount = 0;
};

#endif
