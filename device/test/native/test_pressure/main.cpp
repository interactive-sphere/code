#include <unity.h>

#include <ArduinoFake.h>
#include "ArduinoFramework.h"

#include "PressureTriggerTests.h"
#include "PressureSensorReaderTests.h"
#include "PressureSensorTests.h"
#include "PressureEventDetectorTest.h"
#include "scenarios/PressureSensorScenarios.h"

int main()
{
    UNITY_BEGIN();

    PressureTriggerTests::runTests();
    PressureSensorReaderTests::runTests();
    PressureSensorTests::runTests();
    PressureEventDetectorTests::runTests();
    PressureSensorScenarios::runTests();

    UNITY_END();
}
