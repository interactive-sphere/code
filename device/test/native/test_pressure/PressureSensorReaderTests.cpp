#include "PressureSensorReaderTests.h"

#include <vector>

#include "log.h"
#include "unity_class_tests.h"

using namespace fakeit;


void PressureSensorReaderTests::runTests(){
    RUN_CLASS_TEST(PressureSensorReaderTests, test_read_unfiltered);
    RUN_CLASS_TEST(PressureSensorReaderTests, test_first_reading);
    RUN_CLASS_TEST(PressureSensorReaderTests, test_calibrate);
    RUN_CLASS_TEST(PressureSensorReaderTests,
                        test_calibration_should_reset_filters_data);
}

using TestClass = PressureSensorReaderTests;

TestClass::PressureSensorReaderTests()
  : reader()
  , sensorReader(&reader.get())
{}


void TestClass::resetReader(int medianWindowSize, int averageWindowSize){
    sensorReader = PressureSensorReader(
        &reader.get(), medianWindowSize, averageWindowSize
    );
}

void TestClass::test_read_unfiltered(){
    std::vector<long> expected{{1, 2, 3}};
    reader.setNextReadings(expected);

    std::vector<long> results;
    for(int i=0; i < 3; ++i){
        sensorReader.update();
        auto value = sensorReader.currentUnfiltered();

        results.push_back(value);
    }

    TEST_ASSERT_EQUAL_INT64_ARRAY_MESSAGE(
      &expected[0], &results[0], expected.size(),
      "Readed unfiltered values does not match expectation.");
}

void TestClass::test_first_reading(){
    auto value = 10;
    auto m = readNext(value);

    TEST_ASSERT_EQUAL_MESSAGE(value, m.measure, "Measure");
    TEST_ASSERT_EQUAL_MESSAGE(value, m.rawValue, "Raw value");
    TEST_ASSERT_EQUAL_MESSAGE(value, sensorReader.maxReadedValue(),
        "Max reading should be the first"
    );
    TEST_ASSERT_EQUAL_MESSAGE(value, sensorReader.minReadedValue(),
        "Min reading should be the first"
    );
}

void TestClass::test_calibrate(){
    calibrateWith({10, 5, 2});

    auto m = readNext(6);
    TEST_ASSERT_EQUAL_MESSAGE(6, m.rawValue,
        "Raw value should keep the same"
    );
    TEST_ASSERT_EQUAL_MESSAGE(0, m.measure,
        "When reading any measure in the calibrated range, should measure 0"
    );
}

void TestClass::test_calibration_should_reset_filters_data(){
    resetReader(
        1, //median window size
        1 //average window size
    );
    updateWith({10, 10, 20});

    TEST_ASSERT_EQUAL_MESSAGE(10, sensorReader.minReadedValue(), "minValue");
    TEST_ASSERT_EQUAL_MESSAGE(20, sensorReader.maxReadedValue(), "maxValue");

    calibrateWith({1, 2, 3});
    updateWith({3, //3 - baseline -> 0
                3,
                5} //5 - baseline -> 2
                );

    TEST_ASSERT_EQUAL_MESSAGE(0, sensorReader.minReadedValue(), "minValue");
    TEST_ASSERT_EQUAL_MESSAGE(2, sensorReader.maxReadedValue(), "maxValue");
}

// -------------------------------------------------------------------

void TestClass::calibrateWith(const Readings & measures){
    sensorReader.startCalibration();

    updateWith(measures);

    sensorReader.stopCalibration();
}


void TestClass::updateWith(const Readings & measures){
    reader.setNextReadings(measures);
    while(reader.hasReadings()){
        sensorReader.update();
    }
}

pressure::Measure TestClass::readNext(long measure){
    updateWith({measure});

    return currentMeasure();
}

pressure::Measure TestClass::currentMeasure() const{
    return {
        0, //sensorId
        sensorReader.relative(), //percent
        sensorReader.current(), //measure
        sensorReader.currentUnfiltered() //rawValue
    };
}