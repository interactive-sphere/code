#include "PressureSensorTests.h"

#include <vector>

#include "log.h"
#include "ArduinoFramework.h"

using namespace fakeit;


void run_test_read_measure()
{ PressureSensorTests().test_read_measure();}


void PressureSensorTests::runTests(){
    RUN_TEST(run_test_read_measure);
}

PressureSensorTests::PressureSensorTests()
  : reader()
  , sensor(123, &reader.get())
{
}

void PressureSensorTests::test_read_measure(){
    std::vector<long> measures{{1, 4, 2}};
    reader.setNextReadings(measures);

    for(auto i = 0; i < measures.size(); ++i){
        auto measure = sensor.readMeasure();

        log_info("measure: {sensorId: %d, measure: %ld, percentValue: %f}\n", measure.sensorId, measure.measure, measure.percentValue);

        TEST_ASSERT_EQUAL_INT_MESSAGE(sensor.id(), measure.sensorId,
          "Measure sensor id mismatch");

        TEST_ASSERT_EQUAL_INT_MESSAGE(measures[i], measure.rawValue,
          "Raw value mismatch");

        TEST_ASSERT_EQUAL_INT_MESSAGE(sensor.reader.current(), measure.measure,
          "Measure value mismatch");
    }
}
