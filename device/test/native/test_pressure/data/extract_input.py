import sys
import os


class ExtractInput(object):
    def __init__(self):
        self.var_measures = {}

    def run(self):
        self.process_args()
        self.read_input()
        self.process_input()
        self.output_result()

    def process_args(self):
        if len(sys.argv) < 3:
            print("Usage: script <readings_file> <var_name>")
            exit(-1)

        self.input_file = sys.argv[1]
        self.var_to_extract = sys.argv[2]

    def read_input(self):
        with open(self.input_file, 'r') as f:
            for line in f:
                self.process_line(line)

    def process_line(self, line):
        variables = [v.strip() for v in line.split(',')]

        for v in variables:
            var_name, value = v.split(':', 1)

            self.add_measure(var_name, value)

    def add_measure(self, var_name, value):
        measures = self.var_measures.get(var_name, None)

        if measures is None:
            self.var_measures[var_name] = measures = []

        measures.append(value)

    def process_input(self):
        pass

    def output_result(self):
        measures = self.var_measures.get(self.var_to_extract)

        if not measures:
            print('No measure found for "{}"'.format(self.var_to_extract))

        # quoted_measures = ['"{}"'.format(m) for m in measures]

        print(", ".join(measures))


if __name__ == "__main__":
    ExtractInput().run()
