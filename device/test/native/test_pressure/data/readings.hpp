#ifndef DATA_PRESSURE_READINGS_H
#define DATA_PRESSURE_READINGS_H

#include <vector>

namespace pressure{
  static const std::vector<float> press_release_paper_sensor__rel = {
    0.17, 0.17, 0.17, 0.17, 0.18, 0.18, 0.18, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.17, 0.19, 0.23, 0.29, 0.36, 0.42, 0.48, 0.53, 0.57, 0.60, 0.62, 0.65, 0.66, 0.67, 0.68, 0.68, 0.68, 0.68, 0.67, 0.66, 0.62, 0.56, 0.48, 0.40, 0.33, 0.28, 0.25, 0.23, 0.23, 0.22, 0.22, 0.21, 0.20, 0.20, 0.20, 0.20, 0.21, 0.21, 0.21
  };
}

#endif
