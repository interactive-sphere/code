#ifndef PRESSURE_SENSOR_READER_TESTS_H
#define PRESSURE_SENSOR_READER_TESTS_H

#include <vector>
#include "unity.h"
#include "ArduinoFake.h"

#include "PressureSensorReader.h"
#include "CapReaderMock.h"
#include "pressure/Measure.h"

using fakeit::Mock;


class PressureSensorReaderTests {
public:
    using Readings = CapReaderMock::ReadingsVec;

public:
    static void runTests();

public:
    PressureSensorReaderTests();

public:
    void test_read_unfiltered();
    void test_first_reading();
    void test_calibrate();
    void test_calibration_should_reset_filters_data();

protected:
    void calibrateWith(const Readings & measures);
    void updateWith(const Readings & measures);
    pressure::Measure readNext(long measure);
    pressure::Measure currentMeasure() const;

protected:
    void resetReader(int medianWindowSize, int averageWindowSize);

protected:
    CapReaderMock reader;
    PressureSensorReader sensorReader;
};

#endif
