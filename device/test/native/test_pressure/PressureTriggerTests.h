#ifndef PRESSURE_TRIGGER_TESTS_H
#define PRESSURE_TRIGGER_TESTS_H

#include "unity.h"

#include "ArduinoFake.h"

#include <vector>
#include <string>
#include <cstddef>
#include <utility>

#include "PressureEventDetector.h"
#include "DebounceFilter.h"
#include "pressure/Measure.h"

using pressure::Measure;


class SensorMeasures {
public:
    using MeasurePair = std::pair<long, float>;

public:
    SensorMeasures() = default;
    SensorMeasures(const std::vector<MeasurePair> & measurePairs)
    {
      for(const auto & measure: measurePairs){
        this->add(measure.first, measure.second);
      }
    }
public:
    void add(long measure, float relativeValue){
        _measures.push_back({0, relativeValue, measure});
    }

    std::size_t size() const{
        return _measures.size();
    }

    long measureValue(std::size_t i) const{
        return _measures[i].measure;
    }

    float relative(std::size_t i) const{
        return _measures[i].percentValue;
    }

    const Measure & measure(std::size_t i) const{
        return _measures[i];
    }

protected:
    std::vector<Measure> _measures;
};

class PressureTriggerTests {
public:
    static void runTests();

public:
    PressureTriggerTests();

public:
    void test_level_trigger();
    void test_debounce();
    void test_simple_detection();

protected:
    void checkDebounceChangeState(int countThreshold, int timeoutMillis=0);

    void checkDetector(const SensorMeasures & measures,
        int expectedPresses,
        int expectedReleases);

protected:
    void resetDetector();
    void onPressureEvent(const pressure::Event & evt);

protected:
    DebounceFilter debounce;
    pressure::EventDetector detector;
    int count_press_events, count_release_events;
};

#endif
