#include "ApiRouterTests.h"

#include "unity.h"
#include "unity_class_tests.h"

#include "log.h"

using namespace api;

void ApiRouterTests::runTests(){
    RUN_CLASS_TEST(ApiRouterTests, test_router);
    RUN_CLASS_TEST(ApiRouterTests, test_default_route);
    RUN_CLASS_TEST(ApiRouterTests, testMethodRoutes);
}

void ApiRouterTests::test_router(){
    router.clear();
    configureSomeRoutes();

    routeRequest({123, Methods::Get, "/vibration/vibrate"});

    checkRouteCalled({"/vibration/vibrate"});
}

void ApiRouterTests::test_default_route(){
    router.clear();
    configureSomeRoutes();
    router.defaultRoute(makeRoute("/404"));

    routeRequest({123, Methods::Get, "/invalid/route"});

    checkRouteCalled({"/404"});
}


void ApiRouterTests::testMethodRoutes(){
    router.clear();

    String path = "/vibration/vibrate";

    addRoute(path);
    addRoute(path, Methods::Post);
    addRoute(path, Methods::Put);


    routeRequest({986, Methods::Get, path});
    checkRouteCalled({path});

    routeRequest({987, Methods::Post, path});
    checkRouteCalled({path, Methods::Post});

    routeRequest({988, Methods::Put, path});
    checkRouteCalled({path, Methods::Put});
}

void ApiRouterTests::configureSomeRoutes(){
    addRoute("/");
    addRoute("/vibration");
    addRoute("/vibration/vibrate");
    addRoute("/vibration/other");
}

void ApiRouterTests::addRoute(const String & path, api::Methods m){
    router.putRoute({path, m}, makeRoute(path, m));
}

api::Route ApiRouterTests::makeRoute(const String & path, api::Methods m){
    return [this,path,m](Request & req, Response & res){
        this->routeCalled = true;
        this->routePathCalled = {path, m};
    };
}

void ApiRouterTests::routeRequest(api::Request req){
    routeCalled = false;

    Response res;

    router.route(req, res);
}

void ApiRouterTests::checkRouteCalled(const api::RoutePath & expected){
    TEST_ASSERT_TRUE_MESSAGE(routeCalled, "Route should have been called");

    // log_info("expected:%s == called:%s\n",
    //     expected.toString().c_str(), routePathCalled.toString().c_str());

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expected.path.c_str(), routePathCalled.path.c_str(),
        "Called route should match the expected");
    TEST_ASSERT_EQUAL_MESSAGE(expected.method, routePathCalled.method,
        "Called route method should match the expected");
}
