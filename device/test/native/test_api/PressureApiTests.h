#pragma once

#include <ArduinoFake.h>

#include "BaseApiTests.h"
// #include "SphereServices.h"
// #include "SphereApi.h"
// #include "ApiParser.h"
// #include "Response.h"

// #include "StringStream.h"

using fakeit::Mock;

using pressure::SensorDescription;

class PressureApiTests: public BaseApiTests{
public:
    static void runTests();

public:
    PressureApiTests();

public:
    void test_disconnected();

    void test_get_sensors();
    void test_listen_unlisten_sensors();

protected:
    void mockupPressureServices();

protected:
    void givenSensors(std::vector<SensorDescription> sensors);
    void checkNotifyPressureEvent(int expectedMessageId);

protected:
    PressureListener    pressureListener;
    SensorDescriptions sensors;
};