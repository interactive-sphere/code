#ifndef PARSE_TESTS_H
#define PARSE_TESTS_H

#include "ApiParser.h"
#include "Request.h"
#include "Response.h"

class ParseTests {
public:
    static void runTests();

public:
    void testSerializeResponse();
    void testSerializePressureSensor();
    void testSerializePressureSensors();
    void testSerializePressureEvent();
    void testSerializeRoutes();
    void testColorConversion();
    void testParseOrientationFormat(
        const char * value, OrientationFormat expected);

protected:
    void checkParseResponse(const api::Response & response, const String & expected);
protected:
    api::Parser parser;
};

#endif
