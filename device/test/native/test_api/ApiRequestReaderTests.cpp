#include "ApiRequestReaderTests.h"

#include "unity.h"

#include "log.h"

using namespace api;

void test_read_request(){ApiRequestReaderTests().testReadRequest();}

void ApiRequestReaderTests::runTests(){
    RUN_TEST(test_read_request);
}

ApiRequestReaderTests::ApiRequestReaderTests()
{}

void ApiRequestReaderTests::testReadRequest(){
    checkReadRequest("LISTEN /pressure/sensors 123\n\n",
        Request{
            123,
            Methods::Listen,
            "/pressure/sensors"
        }
    );

    checkReadRequest("POST /vibration/vibrate 10\ntime: 200\n\n",
        Request{
            10,
            Methods::Post,
            "/vibration/vibrate",
            {{"time", "200"}}
        }
    );

    checkReadRequest(
R"(
Put /led/color 1024
color: #66ff66

)",
        Request{
            1024,
            Methods::Put,
            "/led/color",
            {{"color", "#66ff66"}}
        }
    );

    checkReadRequest(
        {"post /orientation/calibrate 921\n",
        "gravity: [1, 0, 0]\n\n"},
        Request{
            921,
            Methods::Post,
            "/orientation/calibrate",
            {{"gravity", "[1, 0, 0]"}}
        }
    );
}

void ApiRequestReaderTests::checkReadRequest(String reqData, const Request & expected){
    checkReadRequest(std::vector<String>{std::move(reqData)}, expected);
}

void ApiRequestReaderTests::
    checkReadRequest(std::vector<String> dataParts, const api::Request & expected)
{
    StringStream stream;
    reader.stream(&stream);

    for (int i = 0; i < dataParts.size(); ++i){
        // log_info("checkReadRequest: update(\"%s\")\n", dataParts[i].c_str());

        stream.inStr(std::move(dataParts[i]));

        bool found = reader.update();
        if (i < dataParts.size() - 1)
        {
            TEST_ASSERT_FALSE_MESSAGE(found, "Should not have found request yet");
        }
        else{
            TEST_ASSERT_TRUE_MESSAGE(found, "Should have found a request");
        }
    }

    auto & req = reader.request();

    TEST_ASSERT_EQUAL_MESSAGE(expected.method, req.method,
        "Extracted method does not match the expected");

    TEST_ASSERT_EQUAL_STRING_MESSAGE(
        expected.target.c_str(), req.target.c_str(),
        "Extracted target does not match the expected");

    TEST_ASSERT_EQUAL_MESSAGE(expected.id, req.id,
        "Extracted id does not match the expected");

    TEST_ASSERT_EQUAL_MESSAGE(expected.arguments.size(), req.arguments.size(),
        "Parsed request arguments does not match the expected");

    for(auto & pair: expected.arguments){
        auto found = req.arguments.find(pair.first);

        String msg_not_found = "Key \"" + pair.first + "\" not found";
        TEST_ASSERT_TRUE_MESSAGE((found!=req.arguments.end()), msg_not_found.c_str());

        String msg_not_match =
            "(" + pair.first  + ", " + pair.second + ") does not match "
            "(" + found->first + ", " + found->second + ")";

        TEST_ASSERT_TRUE_MESSAGE((pair == *found), msg_not_match.c_str());
    }
}
