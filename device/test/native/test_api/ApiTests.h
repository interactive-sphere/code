#ifndef API_TESTS_H
#define API_TESTS_H

#include <ArduinoFake.h>

#include "SphereServices.h"
#include "SphereApi.h"
#include "ApiParser.h"
#include "Response.h"

#include "StringStream.h"

using fakeit::Mock;

class ApiTests {
public:
    static void runTests();

public:
    ApiTests();

public:
    void testSetup();
    void testDisconnected();
    void test_led_services();
    void test_vibration_services();
    void test_sound_services();

protected:
    void mockupServices();

    template<typename T>
    void mockupService(Mock<T> & mock){
        mockupServiceMock((Mock<Service> &)mock);
    }
    void mockupServiceMock(Mock<Service> & mock);

    void callApi(String header);
    void checkResponse(const api::Response & expected);

protected:
    Mock<LedServices> ledServices;
    Mock<VibrationServices> vibrationServices;
    Mock<SoundServices> soundServices;
    Mock<OrientationServices> orientationServices;
    Mock<PressureServices> pressureServices;

    SphereServices services;

    StringStream stream;
    api::SphereApi api;
    api::Parser parser;

    OrientationListener orientationListener;
};


#endif
