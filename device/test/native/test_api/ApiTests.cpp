#include "ApiTests.h"

#include "unity.h"

#include "StringStream.h"
#include "log.h"
#include "3dmath/Quaternion.hpp"
#include "3dmath/Vector3D.hpp"

#include "unity_class_tests.h"

#include <exception>
#include <array>

using namespace fakeit;
using namespace api;

void ApiTests::runTests(){
    RUN_CLASS_TEST(ApiTests, testSetup);
    RUN_CLASS_TEST(ApiTests, testDisconnected);
    RUN_CLASS_TEST(ApiTests, test_led_services);
    RUN_CLASS_TEST(ApiTests, test_vibration_services);
    RUN_CLASS_TEST(ApiTests, test_sound_services);
}

ApiTests::ApiTests()
    : ledServices()
    , vibrationServices()
    , soundServices()
    , orientationServices()
    , pressureServices()
    , services({
        &ledServices.get(),
        &vibrationServices.get(),
        &soundServices.get(),
        &orientationServices.get(),
        &pressureServices.get()
     })
     , stream()
     , api(&stream, &services)
{
    api.setup();

    mockupServices();
}

void ApiTests::mockupServices(){
    mockupService(ledServices);
    mockupService(orientationServices);
    mockupService(vibrationServices);
    mockupService(soundServices);

    Fake(Method(ledServices, begin));
    Fake(Method(ledServices, update));
    Fake(Method(ledServices, changeColor));
    Fake(Method(ledServices, off));

    Fake(Method(orientationServices, removeListener));
    Fake(OverloadedMethod(orientationServices, calibrate, void()));
    Fake(OverloadedMethod(orientationServices, calibrate, void(const Vector3D<float> &)));
    When(Method(orientationServices, listenOrientation))
        .AlwaysDo([this](OrientationListener listener, ListenOrientationConfig config){
            this->orientationListener = listener;
        });

    Fake(Method(pressureServices, removeListener));

}


void ApiTests::mockupServiceMock(Mock<Service> & mock){
    Fake(Method(mock, begin));
    Fake(Method(mock, update));
}

void ApiTests::testSetup(){
    TEST_ASSERT_NOT_NULL(services.led());
    TEST_ASSERT_NOT_NULL(services.vibration());
    TEST_ASSERT_NOT_NULL(services.sound());
    TEST_ASSERT_NOT_NULL(services.orientation());
}

void ApiTests::testDisconnected(){
    this->api.disconnected();

    Verify(Method(orientationServices, removeListener));
}

void ApiTests::test_led_services(){
    callApi("PUT /led/color 123\ncolor: #66ff66\n\n");
    Verify(Method(ledServices, changeColor).Using(ColorDef(0x66,0xff,0x66))).Once();
    checkResponse(Response{123, Response::Status::Ok});

    callApi("POST /led/off 1\n\n");
    Verify(Method(ledServices, off)).Once();
    checkResponse(Response{1, Response::Status::Ok});
}

void ApiTests::test_vibration_services(){
}
void ApiTests::test_sound_services(){

}

void ApiTests::callApi(String header){
    log_info("Calling api: '%s'\n", header.c_str());

    stream.inStr(std::move(header));

    log_info("updating api\n");
    this->api.update();

    log_info("finished\n");
}

void ApiTests::checkResponse(const api::Response & expected){
    auto responseStr = std::move(stream.outStr());
    auto expectedStr = parser.serializeResponse(expected);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedStr.c_str(), responseStr.c_str(),
        "Response does not match the expected");
}
