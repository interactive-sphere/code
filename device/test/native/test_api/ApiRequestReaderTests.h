#ifndef API_REQUEST_READER_TESTS_H
#define API_REQUEST_READER_TESTS_H

#include "ApiRequestReader.h"
#include "StringStream.h"

#include <vector>

class ApiRequestReaderTests {
public:
    static void runTests();

public:
    ApiRequestReaderTests();

public:
    void testReadRequest();

protected:
    void checkReadRequest(String reqData, const api::Request & expected);
    void checkReadRequest(std::vector<String> dataParts, const api::Request & expected);

protected:
    api::RequestReader reader;
};


#endif
