#include "OrientationApiTests.h"

#include "unity.h"

#include "log.h"
#include "3dmath/Quaternion.hpp"
#include "3dmath/Vector3D.hpp"
#include "3dmath/Vector6D.hpp"

#include "unity_class_tests.h"


using namespace fakeit;
using namespace api;


using TestClass = OrientationApiTests;

void TestClass::runTests(){
    RUN_CLASS_TEST(OrientationApiTests, test_orientation_api_calibrate);

    RUN_CLASS_TEST(OrientationApiTests, test_listen_orientation);
    RUN_CLASS_TEST(OrientationApiTests, test_listen_euler);
    RUN_CLASS_TEST(OrientationApiTests, test_listen_yawpitchrow);
    RUN_CLASS_TEST(OrientationApiTests, test_listen_accelgyro);
    RUN_CLASS_TEST(OrientationApiTests, test_listen_interval);
    RUN_CLASS_TEST(OrientationApiTests, test_unlisten);
}

void TestClass::test_orientation_api_calibrate(){
    callApi("POST /orientation/calibrate 654\n\n");
    Verify(OverloadedMethod(orientationServices, calibrate, void())).Once();
    checkResponse(Response{654, Response::Status::Ok});

    callApi("POST /orientation/calibrate 654\ngravity: [0, 0, 1]\n\n");
    Verify(OverloadedMethod(orientationServices, calibrate, void(const Vector3D<float> &))).Once();
    checkResponse(Response{654, Response::Status::Ok});

    callApi("POST /orientation/calibrate 654\ngravity: [0, \n\n");
    checkResponse(Response{654, Response::Status::BadFormat});
}

void TestClass::test_listen_orientation(){
    callApi("LISTEN /orientation 987\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::Quaternion})).Once();
    checkResponse(Response{987, Response::Status::Registered});
}
void TestClass::test_listen_euler(){
    callApi("LISTEN /orientation 988\nformat: euler\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::Euler})).Once();
    checkResponse(Response{988, Response::Status::Registered});
}
void TestClass::test_listen_yawpitchrow(){
    callApi("LISTEN /orientation 989\nformat: YawPitchRow\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::YawPitchRow})).Once();
    checkResponse(Response{989, Response::Status::Registered});

    checkNotifyOrientation(989, OrientationFormat::YawPitchRow);
}

void TestClass::test_listen_accelgyro(){
    callApi("LISTEN /orientation 123\nformat: AccelGyro\n\n");
    checkResponse(Response{123, Response::Status::Registered});

    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::AccelGyro})).Once();

    checkNotifyOrientation(123, OrientationFormat::AccelGyro);
}

void TestClass::test_listen_interval(){
    callApi("LISTEN /orientation 989\ninterval: 15\n\n");
    Verify(Method(orientationServices, listenOrientation)
            .Using(_, ListenOrientationConfig{OrientationFormat::Quaternion, 15})).Once();
    checkResponse(Response{989, Response::Status::Registered});
}
void TestClass::test_unlisten(){
    callApi("LISTEN /orientation 989\n\n");
    checkResponse(Response{989, Response::Status::Registered});

    callApi("UNLISTEN /orientation 989\n\n");
    Verify(Method(orientationServices, removeListener)).Once();
    checkResponse(Response{989, Response::Status::Unregistered});
}

void TestClass::checkNotifyOrientation(int expectedMessageId, OrientationFormat orientationFormat)
{
    TEST_ASSERT_TRUE_MESSAGE((bool)orientationListener,
        "Should have captured a valid orientation listener");

    Quat quaternion{1, 2.2f, 3.33f, 4.444f};
    Vector3D<float> vector{-1, 0, 1.5f};
    Vector6D<float> vec6D{{-1.0f, 0.0f, 1.5f, 1.0f, 2.0f, 3.0f}};
    String expectedData;

    switch (orientationFormat){
    case OrientationFormat::Euler:
    case OrientationFormat::YawPitchRow:
        orientationListener(vector);
        expectedData = vector.toString();
        break;
    case OrientationFormat::AccelGyro:
        orientationListener(vec6D);
        expectedData = vec6D.toString();
        break;
    case OrientationFormat::Quaternion:
    default:
        orientationListener(quaternion);
        expectedData = quaternion.toString();
        break;
    }

    checkResponse({
        expectedMessageId, Response::Status::Notification, "",
        {{"format", orientationFormat.c_str()}},
        expectedData
    });
}