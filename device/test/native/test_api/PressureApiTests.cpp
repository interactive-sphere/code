#include "PressureApiTests.h"

#include "BaseApiTests.h"
#include "unity.h"

#include "StringStream.h"
#include "log.h"
#include "3dmath/Quaternion.hpp"
#include "3dmath/Vector3D.hpp"

#include "unity_class_tests.h"

#include <exception>
#include <array>

using namespace fakeit;
using namespace api;

void PressureApiTests::runTests(){
    RUN_CLASS_TEST(PressureApiTests, test_disconnected);
    RUN_CLASS_TEST(PressureApiTests, test_get_sensors);
    RUN_CLASS_TEST(PressureApiTests, test_listen_unlisten_sensors);
}

PressureApiTests::PressureApiTests()
    : BaseApiTests()
{
    mockupPressureServices();
}

void PressureApiTests::mockupPressureServices(){
    mockupService(pressureServices);

    Fake(Method(pressureServices, removeListener));
    When(Method(pressureServices, listenPressureEvents))
        .AlwaysDo([this](PressureListener listener){
            this->pressureListener = listener;
        });
}

void PressureApiTests::givenSensors(std::vector<SensorDescription> sensors){
    this->sensors.clear();
    for (auto & s : sensors){
        sensors[s.id()] = std::move(s);
    }

    When(Method(pressureServices, getSensorDescriptions))
        .AlwaysReturn(this->sensors);
}


void PressureApiTests::test_disconnected(){
    this->api.disconnected();

    Verify(Method(pressureServices, removeListener));
}

void PressureApiTests::test_get_sensors(){
    givenSensors({
        {0, "front", "Front button"},
        {1, "top"  , "Top button"}
    });

    callApi("GET /pressure/sensors 3456\n\n");
    Verify(Method(pressureServices, getSensorDescriptions)).Once();
    checkResponse(Response{3456, Response::Status::Ok}
                    .data(parser.serializePressureSensors(sensors)));
}


void PressureApiTests::test_listen_unlisten_sensors(){
    callApi("LISTEN /pressure/sensors 3457\n\n");
    Verify(Method(pressureServices, listenPressureEvents).Using(_)).Once();
    checkResponse(Response{3457, Response::Status::Registered});

    checkNotifyPressureEvent(3457);

    callApi("UNLISTEN /pressure/sensors 3457\n\n");
    Verify(Method(pressureServices, removeListener)).Once();
    checkResponse(Response{3457, Response::Status::Unregistered});
}


void PressureApiTests::checkNotifyPressureEvent(int expectedMessageId){
    TEST_ASSERT_TRUE_MESSAGE((bool)pressureListener,
        "Should have captured a valid pressure listener");

    pressure::Event evt{1, pressure::EventType::Press, 0.50f, 3432, 1234};
    String expectedData = R"({"sensor":1,"type":"Press","relative":0.5,"measure":3432,"raw":1234})";

    pressureListener(evt);

    checkResponse(
        Response{expectedMessageId, Response::Status::Notification}
            .data(expectedData)
    );
}
