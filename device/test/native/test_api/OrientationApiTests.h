#pragma once

#include "BaseApiTests.h"

class OrientationApiTests : public BaseApiTests{
public:
    static void runTests();

public:
    void test_orientation_api_calibrate();

    void test_listen_orientation();
    void test_listen_euler();
    void test_listen_yawpitchrow();
    void test_listen_accelgyro();
    void test_listen_interval();
    void test_unlisten();

protected:
    void checkNotifyOrientation(
        int expectedMessageId,
        OrientationFormat orientationFormat);
};
