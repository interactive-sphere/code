#include <unity.h>

#include "ArduinoFramework.h"

#include "ParseTests.h"
#include "ApiRouterTests.h"
#include "ApiTests.h"
#include "ApiRequestReaderTests.h"
#include "OrientationApiTests.h"
#include "PressureApiTests.h"

int main()
{
    UNITY_BEGIN();

    ParseTests::runTests();
    ApiRouterTests::runTests();
    ApiRequestReaderTests ::runTests();
    ApiTests::runTests();
    OrientationApiTests::runTests();
    PressureApiTests::runTests();

    UNITY_END();
}
