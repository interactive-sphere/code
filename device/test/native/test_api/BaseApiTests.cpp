#include "BaseApiTests.h"

#include "unity.h"

#include "StringStream.h"
#include "log.h"
#include "3dmath/Quaternion.hpp"
#include "3dmath/Vector3D.hpp"

#include "unity_class_tests.h"

#include <exception>
#include <array>

using namespace fakeit;
using namespace api;


BaseApiTests::BaseApiTests()
    : ledServices()
    , vibrationServices()
    , soundServices()
    , orientationServices()
    , pressureServices()
    , services({
        &ledServices.get(),
        &vibrationServices.get(),
        &soundServices.get(),
        &orientationServices.get(),
        &pressureServices.get()
     })
     , stream()
     , api(&stream, &services)
{
    api.setup();

    mockupServices();
}

void BaseApiTests::mockupServices(){
    mockupService(ledServices);
    mockupService(orientationServices);
    mockupService(pressureServices);
    mockupService(vibrationServices);
    mockupService(soundServices);

    Fake(Method(ledServices, begin));
    Fake(Method(ledServices, update));
    Fake(Method(ledServices, changeColor));
    Fake(Method(ledServices, off));

    Fake(Method(orientationServices, removeListener));
    Fake(OverloadedMethod(orientationServices, calibrate, void()));
    Fake(OverloadedMethod(orientationServices, calibrate, void(const Vector3D<float> &)));
    When(Method(orientationServices, listenOrientation))
        .AlwaysDo([this](OrientationListener listener, ListenOrientationConfig config){
            this->orientationListener = listener;
        });

    Fake(Method(pressureServices, removeListener));
    When(Method(pressureServices, listenPressureEvents))
        .AlwaysDo([this](PressureListener listener){
            this->pressureListener = listener;
        });

}


void BaseApiTests::mockupServiceMock(Mock<Service> & mock){
    Fake(Method(mock, begin));
    Fake(Method(mock, update));
}

void BaseApiTests::givenSensors(std::vector<SensorDescription> sensors){
    this->sensors.clear();
    for (auto & s : sensors){
        sensors[s.id()] = std::move(s);
    }

    When(Method(pressureServices, getSensorDescriptions))
        .AlwaysReturn(this->sensors);
}

void BaseApiTests::callApi(String header){
    log_info("Calling api: '%s'\n", header.c_str());

    stream.inStr(std::move(header));

    log_info("updating api\n");
    this->api.update();

    log_info("finished\n");
}

void BaseApiTests::checkResponse(const api::Response & expected){
    auto responseStr = std::move(stream.outStr());
    auto expectedStr = parser.serializeResponse(expected);

    TEST_ASSERT_EQUAL_STRING_MESSAGE(expectedStr.c_str(), responseStr.c_str(),
        "Response does not match the expected");
}


void BaseApiTests::checkNotifyPressureEvent(int expectedMessageId){
    TEST_ASSERT_TRUE_MESSAGE((bool)pressureListener,
        "Should have captured a valid pressure listener");

    pressure::Event evt{1, pressure::EventType::Press, 0.50f, 3432, 1234};
    String expectedData = R"({"sensor":1,"type":"Press","relative":0.5,"measure":3432,"raw":1234})";

    pressureListener(evt);

    checkResponse(
        Response{expectedMessageId, Response::Status::Notification}
            .data(expectedData)
    );
}
