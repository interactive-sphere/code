#pragma once

#include <ArduinoFake.h>

#include "SphereServices.h"
#include "SphereApi.h"
#include "ApiParser.h"
#include "Response.h"

#include "StringStream.h"

using fakeit::Mock;

using pressure::SensorDescription;

class BaseApiTests {
public:
    BaseApiTests();

protected:
    void mockupServices();

    template<typename T>
    void mockupService(Mock<T> & mock){
        mockupServiceMock((Mock<Service> &)mock);
    }
    void mockupServiceMock(Mock<Service> & mock);

    void givenSensors(std::vector<SensorDescription> sensors);

    void callApi(String header);
    void checkResponse(const api::Response & expected);
    void checkNotifyPressureEvent(int expectedMessageId);

protected:
    Mock<LedServices> ledServices;
    Mock<VibrationServices> vibrationServices;
    Mock<SoundServices> soundServices;
    Mock<OrientationServices> orientationServices;
    Mock<PressureServices> pressureServices;

    SphereServices services;

    StringStream stream;
    api::SphereApi api;
    api::Parser parser;

    OrientationListener orientationListener;
    PressureListener    pressureListener;
    SensorDescriptions sensors;
};
