#include <unity.h>

#include <ArduinoFake.h>
#include "ArduinoFramework.h"

#include "CsvReaderTests.h"
#include "CsvMeasureReaderTests.h"

int main()
{
    UNITY_BEGIN();

    CsvReaderTests::runTests();
    CsvMeasureReaderTests::runTests();

    UNITY_END();
}
