#ifndef TEST_DATAREADER__CSV_READER_TESTS_H
#define TEST_DATAREADER__CSV_READER_TESTS_H

#include "CsvReader.h"

class CsvReaderTests {
public:
    static void runTests();

public:
    CsvReaderTests();

public:
    void test_open_csv();
    void test_read_values();
};

#endif
