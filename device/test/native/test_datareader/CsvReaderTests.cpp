#include "CsvReaderTests.h"

#include "log.h"
#include "unity_class_tests.h"
#include "test_directories.h"

#include "ArduinoFramework.h"
#include <vector>

using namespace std;


void CsvReaderTests::runTests(){
  RUN_CLASS_TEST(CsvReaderTests, test_open_csv);
  RUN_CLASS_TEST(CsvReaderTests, test_read_values);
}

CsvReaderTests::CsvReaderTests()
{}

static const auto measures_idle_csv =
    TestDirectories::testDataDir() + "/measures-idle.csv";



using StringVec = CsvReader::StringVec;

std::vector<const char *> toCstrVec(const StringVec & values)
{
    std::vector<const char *> out;

    for(const auto & str: values){
      out.push_back(str.c_str());
    }

    return out;
}

void assertStringVecsAreEquals(
  const StringVec & expected, const StringVec & values, const String & msg)
{
    auto expec_cstr = toCstrVec(expected);
    auto values_cstr = toCstrVec(values);

    TEST_ASSERT_EQUAL_STRING_ARRAY_MESSAGE(
        expec_cstr.data(), values_cstr.data(), expec_cstr.size(),
        msg.c_str());
}

void CsvReaderTests::test_open_csv(){
    CsvReader csv;

    csv.open(measures_idle_csv, "\t");

    // std::vector<const char *> expected_columns{
    std::vector<std::string> expected_columns{
      "measure",	"lower_threshold", "max", "min", "state", "upper_threshold"
    };

    assertStringVecsAreEquals(expected_columns, csv.columns(),
        "Columns does not match");

    for(auto & col: expected_columns){
        auto msg = String("Does not has column: \"") + col.c_str() + "\"";

        TEST_ASSERT_TRUE_MESSAGE(csv.hasColumn(col),msg.c_str());
    }
}



void CsvReaderTests::test_read_values(){
    CsvReader csv;

    csv.open(measures_idle_csv, "\t");

    // std::vector<std::vector<const char *>> expected_values{
    std::vector<std::vector<std::string>> expected_values{
      {"7814.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7816.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7819.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7819.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7819.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7819.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7819.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7818.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7817.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"},
      {"7817.0", "7822.6", "7846.0", "7807.0", "7807.0", "7832.35"}
    };

    int count = 0;
    while(csv.next() && count < expected_values.size()){
        auto & expected = expected_values[count];

        auto msg = String("Readed values at line ") + count + " are not equal.";
        assertStringVecsAreEquals(expected, csv.values(),msg.c_str());

        ++count;
    }

    TEST_ASSERT_EQUAL_MESSAGE(expected_values.size(), count,
      "Not all data were readed"
    );
}