#include "CsvMeasureReaderTests.h"

#include "log.h"
#include "unity_class_tests.h"
#include "unity_test_assertions.h"
#include "test_directories.h"

#include "ArduinoFramework.h"
#include <vector>

#include "CsvMeasureReader.h"


using namespace std;


void CsvMeasureReaderTests::runTests(){
  RUN_CLASS_TEST(CsvMeasureReaderTests, test_read_measures);
}

CsvMeasureReaderTests::CsvMeasureReaderTests()
{}

static const auto measures_idle_csv =
    TestDirectories::testDataDir() + "/measures-sample.csv";



void CsvMeasureReaderTests::test_read_measures(){
    CsvMeasureReader csv;

    std::vector<double> expected_values{
      7814.0, 7816.0, 7819.0, 7819.0, 7819.0,
      7819.0, 7819.0, 7818.0, 7817.0, 7817.0
    };

    auto values = csv.readMeasures(measures_idle_csv.c_str(), "measure", "\t");

    test_helpers::floatVectorsAreEquals(expected_values, values);
}