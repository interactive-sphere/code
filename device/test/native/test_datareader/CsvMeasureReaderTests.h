#ifndef TEST_DATAREADER__CSV_MEASURE_READER_TESTS_H
#define TEST_DATAREADER__CSV_MEASURE_READER_TESTS_H

class CsvMeasureReaderTests {
public:
    static void runTests();

public:
    CsvMeasureReaderTests();

public:
    void test_read_measures();
};

#endif
