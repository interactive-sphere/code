#ifndef MEDIAN_FILTER_TESTS_H
#define MEDIAN_FILTER_TESTS_H

#include "unity.h"

#include "FastRunningMedian.h"
#include "AverageFilter.h"
#include "ArduinoFramework.h"

#include <vector>


class MedianFilterTests {
public:
    static void runTests();

public:
    MedianFilterTests()
        : median(5)
    {}

    void test_initial_value();

protected:
    FastRunningMedian<long> median;
};

#endif
