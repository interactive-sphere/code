#ifndef TEST_FILTERS__FILTER_PIPELINE_TESTS_H
#define TEST_FILTERS__FILTER_PIPELINE_TESTS_H

#include "unity.h"

#include "ArduinoFake.h"

#include "mocks/FilterMock.hpp"

#include "FilterPipeline.h"


using fakeit::Mock;

class FilterPipelineTests {
public:
    static void runTests();

public:
    void test_pass_readed_value_to_filter();
    void test_chain_filter_values();
    void test_should_interrupt_update_chain_on_first_false();
    void test_pipeline_current_value();

    void test_insert_filter();

protected:
    FilterPipeline pipeline;

    FilterMock<long> filterMock;
};

#endif
