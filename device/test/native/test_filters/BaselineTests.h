#ifndef BASELINE_TESTS_H
#define BASELINE_TESTS_H

#include "unity.h"

#include "Baseline.h"
#include "ArduinoFramework.h"

#include <vector>


class BaselineTests {
public:
    static void runTests();

public:
    BaselineTests();

public:
    void test_calibrate_baseline_range();
    void test_stop_calibration();

    void test_default_threshold_boundary();
    void test_configure_threshold_boundary();

    void test_detect_value_range();
    void test_detect_value_range_with_boundaries();

    void test_with_idle_data();

protected:
    void whenCalibrating();
    void whenStopCalibration();
    void whenCalibratingWith(const std::vector<long> & values);
    void whenReading(const std::vector<long> & v);
    void calibratedRangeShouldBe(long min, long max) const;
    void baselineThresholdShouldBe(long threshold) const;

    void shouldBeLow(long value) const;
    void shouldBeHigh(long value) const;
    void checkValueRange(long value, bool isHigh) const;

    void shouldBeInBaselineRange(long value, bool expected=true);
protected:
    void givenBoundaryFactorOf(float factor);

    String noiseRangeString();
protected:
    Baseline baseline;
};

#endif
