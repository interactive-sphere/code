#include "BaselineTests.h"

#include "log.h"

#include "ArduinoFramework.h"
#include <vector>

#include "unity_class_tests.h"

using namespace std;

using TestClass=BaselineTests;


void TestClass::runTests(){
    RUN_CLASS_TEST(BaselineTests, test_calibrate_baseline_range);
    RUN_CLASS_TEST(BaselineTests, test_stop_calibration);
    RUN_CLASS_TEST(BaselineTests, test_default_threshold_boundary);
    RUN_CLASS_TEST(BaselineTests, test_configure_threshold_boundary);
    RUN_CLASS_TEST(BaselineTests, test_detect_value_range);
    RUN_CLASS_TEST(BaselineTests, test_detect_value_range_with_boundaries);
    RUN_CLASS_TEST(BaselineTests, test_with_idle_data);
}

// --------------------------------------------------------------------------

static const std::vector<long> idle_measures = {
    7814, 7816, 7819, 7819, 7819, 7819, 7819, 7818, 7817,
    7817, 7817, 7816, 7815, 7815, 7814, 7814, 7814, 7814,
    7814, 7814, 7814, 7814, 7814, 7814, 7814, 7814, 7814,
    7814, 7814, 7815, 7816, 7816, 7818, 7819, 7820, 7821,
    7821, 7820, 7819, 7818, 7817, 7817, 7817, 7818, 7818,
    7818, 7819, 7819, 7819, 7819, 7819, 7819, 7819, 7819,
    7818, 7817, 7815, 7814, 7814, 7813, 7813, 7813, 7813,
    7813, 7813, 7814, 7814, 7815, 7815, 7815, 7816, 7816,
    7816, 7816, 7815, 7815, 7815, 7815, 7815, 7815, 7815,
    7815, 7815, 7815, 7816, 7817, 7817, 7816, 7814, 7812,
    7812, 7811, 7811, 7811, 7809, 7808, 7808, 7809, 7810,
    7811, 7813, 7815, 7816, 7818, 7820, 7821, 7821, 7821,
    7821, 7821, 7821, 7820, 7820, 7820, 7821, 7821, 7822,
    7823, 7824, 7824, 7824, 7824, 7823, 7823, 7822, 7821,
    7821, 7820, 7820, 7820, 7820, 7821, 7821, 7822, 7823,
    7824, 7825, 7825, 7826, 7825, 7825, 7824, 7824, 7824,
    7824, 7824, 7823
};


TestClass::BaselineTests()
  : baseline()
{
}


void TestClass::test_calibrate_baseline_range(){
    whenCalibrating();
    whenReading({
      8, 1, 5, 10, 2, 20, 3
    });

    calibratedRangeShouldBe(1, 20);
}

void TestClass::test_default_threshold_boundary(){
    whenCalibratingWith({50, 100, 60});

    baselineThresholdShouldBe(100); //the max value
}

void TestClass::test_configure_threshold_boundary(){
    givenBoundaryFactorOf(1.0f);
    whenCalibratingWith({50, 100, 60});

    baselineThresholdShouldBe(150 /* max + range*/);
}

void TestClass::test_stop_calibration(){
    whenCalibrating();
    whenReading({ 2, 3, 4 });
    whenStopCalibration();
    whenReading({1, 5});

    calibratedRangeShouldBe(2, 4);
}

void TestClass::test_detect_value_range(){
    whenCalibratingWith({1, 5, 10});

    shouldBeLow(1);
    shouldBeLow(9);
    shouldBeLow(10);
    shouldBeLow(0);
    shouldBeLow(-1);

    shouldBeHigh(50);
    shouldBeHigh(100);
}

void TestClass::test_detect_value_range_with_boundaries(){
    givenBoundaryFactorOf(0.5f);
    whenCalibratingWith({50, 100});

    baselineThresholdShouldBe(125);

    shouldBeInBaselineRange(110, false);
    shouldBeLow(110);

    shouldBeInBaselineRange(125, false);
    shouldBeLow(125);
    shouldBeInBaselineRange(25, false);
    shouldBeLow(25);

    shouldBeHigh(126);
    shouldBeLow(24);
}


template<typename T>
std::vector<T> sliceVector(const std::vector<T> & vec, int startIdx, int endIdx)
{
    return std::vector<T>(vec.begin() + startIdx, vec.begin() + endIdx + 1);
}

void TestClass::test_with_idle_data(){
    givenBoundaryFactorOf(1.0f);

    auto splitIdx = (int)(idle_measures.size()*0.75);
    auto trainData = sliceVector(idle_measures, 0, splitIdx);
    auto testData = sliceVector(idle_measures, splitIdx + 1, idle_measures.size() - 1);

    whenCalibratingWith(trainData);

    for (auto value: testData) {
        shouldBeLow(value);
    }
}

// ------------------------------------------------------------------------

template<typename T>
String rangeToString(const Range<T> & range){

    return String("{ ")
          + "min:" + range.min()
          + ", max:" + range.max()
          + "}";
}


String TestClass::noiseRangeString(){
    return String("{ ")
          + "baselineRange: " + rangeToString(baseline.baselineRange())
          + ", boundaryFactor:" + baseline.boundaryFactor()
          + ", boundaryThreshold: " + baseline.baselineThreshold()
          + "}";
}

void TestClass::givenBoundaryFactorOf(float factor){
    baseline.boundaryFactor(factor);
}

void TestClass::whenCalibrating(){
    baseline.calibrationStart();
}

void TestClass::whenStopCalibration(){
  baseline.calibrationStop();
}

void TestClass::whenCalibratingWith(const std::vector<long> & values){
  whenCalibrating();
  whenReading(values);
  whenStopCalibration();
}

void TestClass::whenReading(const std::vector<long> & values){
    for(auto v : values){
        baseline.update(v);
    }
}

void TestClass::shouldBeLow(long value) const{
    checkValueRange(value, false);
}
void TestClass::shouldBeHigh(long value) const{
    checkValueRange(value, true);
}
void TestClass::checkValueRange(long value, bool expectedIsHigh) const{
    auto isHigh = this->baseline.isHigh(value);

    auto msg = String("With threshold: ") + baseline.baselineThreshold() + ", " + value +
        (expectedIsHigh ? " should be high." : " should be low.");

    TEST_ASSERT_EQUAL_MESSAGE(expectedIsHigh, isHigh, msg.c_str());
}

void TestClass::shouldBeInBaselineRange(long value, bool expected){
    auto isInRange = this->baseline.baselineRange().isInRange(value);

    auto msg = String(value) +
      (expected ? " should be " : "should not be ") + "in noise range.";

    TEST_ASSERT_EQUAL_MESSAGE(expected, isInRange, msg.c_str());
}

template<typename T>
void verify_range(const Range<T> & range, T expectedMin, T expectedMax){
    auto min = range.min();
    auto max = range.max();

    auto msg_prefix = String("with range: ") + rangeToString(range);

    TEST_ASSERT_EQUAL_FLOAT_MESSAGE(expectedMin, min,
      (msg_prefix + ", expected min = " + expectedMin).c_str());
    TEST_ASSERT_EQUAL_FLOAT_MESSAGE(expectedMax, max,
      (msg_prefix + ", expected max = " + expectedMax).c_str());
    TEST_ASSERT_EQUAL_FLOAT_MESSAGE(max - min, range.range(),
      (msg_prefix + ", expected range = " + range.range()).c_str());
}


void TestClass::calibratedRangeShouldBe(long expectedMin, long expectedMax) const{
    verify_range(baseline.baselineRange(), expectedMin, expectedMax);
}

void TestClass::baselineThresholdShouldBe(long threshold) const{
    TEST_ASSERT_EQUAL_MESSAGE(threshold, baseline.baselineThreshold(),
      "Noise threshold is not the expected.");
}
