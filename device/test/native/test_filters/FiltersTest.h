#ifndef FILTERS_TEST_H
#define FILTERS_TEST_H

#include "unity.h"
#include "RunningAverage.h"
#include "FastRunningMedian.h"
#include "RangeFilter.h"

#include "ArduinoFramework.h"

#include <cmath>

template<typename T>
class RangeValue {
public:
    RangeValue (T current, T min, T max, T range, float relative)
        : current(current)
        , min(min)
        , max(max)
        , range(range)
        , relative(relative)
    {}

public:
    T current, min, max, range;
    float relative;
};

class FiltersTest {
public:
    static void runTests();

public:
    void test_running_average();
    void test_running_median_odd();
    void test_running_median_even();
    void test_median_error();
    void test_long_range_filter();
    void test_float_range_filter();

protected:
    float sum(const std::vector<int> & values){
        float s=0;
        for (float v : values)
        {
            s += v;
        }
        return s;
    }

    void checkMedian(
        RunningMedian<float> & runningMedian,
        float * values, float * expected, int numValues)
    {
        TEST_ASSERT_EQUAL_MESSAGE(0, runningMedian.getMedian(), "Median should start at 0");

        int N = runningMedian.windowSize();

        for (int i = 0; i < numValues; ++i){
            runningMedian.addValue(values[i]);

            TEST_ASSERT_EQUAL_MESSAGE(std::min(i+1, N), runningMedian.getCounter(),
                "Counter does not match expected");

            TEST_ASSERT_EQUAL_FLOAT_MESSAGE((float)expected[i], (float)runningMedian.getMedian(),
                "Median does not match expected");
        }
    }

    template<typename T>
    void checkRangeFilter(
        RangeFilter<T> & rangeFilter, std::vector<RangeValue<T>> ranges)
    {
        TEST_ASSERT_EQUAL_MESSAGE(0, rangeFilter.current(), "Range current should start at 0");

        for (const RangeValue<T> & r: ranges){
            auto value = r.current;
            rangeFilter.update(value);

            auto msgCurrent = String(" Current = ") + value;

            TEST_ASSERT_EQUAL_FLOAT_MESSAGE(
                value, rangeFilter.current(),
                "Current should be the last readed value.");

            TEST_ASSERT_EQUAL_FLOAT_MESSAGE(
                r.min, rangeFilter.min(),
                ("Range min does not match expected."
                + msgCurrent).c_str());

            TEST_ASSERT_EQUAL_FLOAT_MESSAGE(
                r.max, rangeFilter.max(),
                ("Range max does not match expected." + msgCurrent).c_str());

            TEST_ASSERT_EQUAL_FLOAT_MESSAGE(
                r.range, rangeFilter.range(),
                ("Range does not match expected." + msgCurrent).c_str());

            TEST_ASSERT_EQUAL_FLOAT_MESSAGE(
                r.relative, rangeFilter.relative(),
                ("Relative value does not match expected."
                + msgCurrent).c_str());
        }
    }
};

#endif
