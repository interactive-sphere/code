#include "FiltersTest.h"

#include "unity_class_tests.h"

using TestClass = FiltersTest;


void TestClass::runTests(){
    RUN_CLASS_TEST(FiltersTest, test_running_average);
    RUN_CLASS_TEST(FiltersTest, test_running_median_odd);
    RUN_CLASS_TEST(FiltersTest, test_running_median_even);
    RUN_CLASS_TEST(FiltersTest, test_median_error);
    RUN_CLASS_TEST(FiltersTest, test_float_range_filter);
    RUN_CLASS_TEST(FiltersTest, test_long_range_filter);
}

void TestClass::test_running_average(){
    RunningAverage runningAverage(5);

    TEST_ASSERT_EQUAL_MESSAGE(0, runningAverage.current(), "Average should start at 0");

    float values[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    float count[]  {1, 2, 3, 4, 5, 5, 5, 5, 5, 5};
    float currentSum[10]{
        1,
        sum({1, 2}),
        sum({1,2,3}),
        sum({1,2,3,4}),
        sum({1,2,3,4,5 }),
        sum({2,3,4,5,6 }),
        sum({3,4,5,6,7 }),
        sum({4,5,6,7,8 }),
        sum({5,6,7,8,9 }),
        sum({6,7,8,9,10})
    };

    for (int i = 0; i < 10; ++i){
        runningAverage.update(values[i]);

        float expectedAverage = currentSum[i]/count[i];

        TEST_ASSERT_EQUAL_MESSAGE(currentSum[i], runningAverage.sum(),
            "Current sum does not match expected");

        TEST_ASSERT_EQUAL_MESSAGE(count[i], runningAverage.count(),
            "Current count does not match expected");

        TEST_ASSERT_EQUAL_MESSAGE(expectedAverage, runningAverage.current(),
            "Average does not match expected");
    }
}

void TestClass::test_running_median_odd(){
    FastRunningMedian<float> runningMedian{5};

    float values[] {1,    2, 3,    4, 5, -5, 4.1f, 1,   50,   12};
    float median[] {1, 1.5f, 2, 2.5f, 3,  3,    4, 4, 4.1f, 4.1f};

    checkMedian(runningMedian, values, median, 10);
}

void TestClass::test_running_median_even(){
    FastRunningMedian<float> runningMedian{4};

    float values[] {1,    2, 3,    4,    5,   -5,  4.1f,     1,    50,    12};
    float median[] {1, 1.5f, 2, 2.5f, 3.5f, 3.5f, 4.05f, 2.55f, 2.55f, 8.05f};

    checkMedian(runningMedian, values, median, 10);
}

void TestClass::test_median_error(){
    FastRunningMedian<long> runningMedian(10);

    std::vector<long> values{
        112, 29, 91, 38, 95, 75, 43, 88, 51, 102, 31,
        83, 31, 89, 81, 87, 32, 105, 30, 90, 117, 97,

        270, 225, 271, 280, 324, 213, 277, 278, 257,
        196, 254, 240, 274, 206, 302, 213, 260, 210,

        84, 34, 83, 55, 92, 24, 80, 32, 90, 40, 91,
        31, 130, 36, 90, 91, 38, 93,
        31, 72, 28, 83
    };

    for(long v: values){
        runningMedian.update(v);
        runningMedian.current();
    }
}

void TestClass::test_long_range_filter(){

    RangeFilter<long> longRangeFilter;
    std::vector<RangeValue<long>> range2{
        //current, min, max, range, relative
        {267, 267, 267, 0,   0.0f},
        { 71,  71, 267, 196, 0},
        { 72,  71, 267, 196, 0.00510204f},
        { 69,  69, 267, 198, 0},
        { 65,  65, 267, 202, 0}
    };
    checkRangeFilter(longRangeFilter, range2);
}

void TestClass::test_float_range_filter(){
    RangeFilter<float> rangeFilter;

    // float values[] {1,    2, 3,    4,    5,   -5,  4.1f,     1,    50,    12};
    std::vector<RangeValue<float>> ranges {
        //current, min, max, range, relative
        {1, 1, 1, 0, 0.0f},
        {2, 1, 2, 1, 1.0f},
        {3, 1, 3, 2, 1.0f},
        {4, 1, 4, 3, 1.0f},
        {5, 1, 5, 4, 1.0f},
        {-5, -5, 5, 10, 0.0f},
        {4.1f, -5, 5, 10, 0.91f},
        {1, -5, 5, 10, 0.6f},
        {50, -5, 50,55, 1.0f},
        {12, -5, 50,55, 0.309090909f}};

    checkRangeFilter(rangeFilter, ranges);
}