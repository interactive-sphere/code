#include "FilterPipelineTests.h"

#include <vector>

#include "log.h"
#include "ArduinoFramework.h"

#include <exception>

#include "mocks/FilterMock.hpp"
#include "unity_class_tests.h"

using namespace fakeit;


void FilterPipelineTests::runTests(){
    RUN_CLASS_TEST(FilterPipelineTests, test_pass_readed_value_to_filter);
    RUN_CLASS_TEST(FilterPipelineTests, test_chain_filter_values);
    RUN_CLASS_TEST(FilterPipelineTests,
                        test_should_interrupt_update_chain_on_first_false);
    RUN_CLASS_TEST(FilterPipelineTests, test_pipeline_current_value);
    RUN_CLASS_TEST(FilterPipelineTests, test_insert_filter);
}


void FilterPipelineTests::test_pass_readed_value_to_filter(){
    pipeline.addFilter(&filterMock.get());
    pipeline.update(1);

    filterMock.verifyUpdateCalledWith(1);
}

void FilterPipelineTests::test_chain_filter_values(){
    // reader.setNextReadings({1, 2, 3});

    FilterMock<long> f1, f2, f3;;
    std::vector<FilterMock<long> *> filters{&f1,&f2,&f3};
    std::vector<long> values{5,  4, 3};
    std::vector<long> receivedValues{1,  5, 4};

    for (int i=0; i<filters.size(); ++i) {
        filters[i]->setNextReadings({values[i]});
        pipeline.addFilter(&filters[i]->get());
    }

    bool updateResult = pipeline.update(1);

    for (int i=0; i<filters.size(); ++i) {
        filters[i]->verifyUpdateCalledWith(receivedValues[i]);
    }

    TEST_ASSERT_TRUE_MESSAGE(updateResult, "Pipeline should return true");
}



void FilterPipelineTests::test_should_interrupt_update_chain_on_first_false(){
    // reader.setNextReadings({1, 2, 3});

    FilterMock<long> f1, f2, f3;

    for (auto f: std::vector<FilterMock<long> *>{&f1,&f2,&f3}) {
        pipeline.addFilter(&f->get());
    }
    f1.updateReturns.setNextReadings({false});

    bool updateResult = pipeline.update(1);

    f1.verifyUpdateCalledWith(1);
    f2.verifyUpdateNotCalled();
    f3.verifyUpdateNotCalled();

    TEST_ASSERT_FALSE_MESSAGE(updateResult, "Pipeline should return false");
}


void FilterPipelineTests::test_pipeline_current_value(){
    FilterMock<long> f1, f2, f3;

    for (auto f: std::vector<FilterMock<long> *>{&f1,&f2,&f3}) {
        pipeline.addFilter(&f->get());
    }
    std::vector<long> readings{10, 20, 30};
    f3.setNextReadings(readings);

    for (int i=0; i<3; ++i) {
        pipeline.update(i);
        TEST_ASSERT_EQUAL_MESSAGE(readings[i], pipeline.current(),
            "Pipeline should keep the value of the last filter called"
        );
    }

}

void FilterPipelineTests::test_insert_filter(){
    FilterMock<long> f1, f2, f3;
    FilterMock<long> f_new;

    int count = 0;
    for (auto f: std::vector<FilterMock<long> *>{&f1,&f2,&f3}) {
        pipeline.addFilter(&f->get());
        f->setNextReadings({++count * 10});
    }

    //Insert new filter after f2 (index 2)
    pipeline.insertFilter(&f_new.get(), 2);
    pipeline.update(1);

    //should receiving readings from f2
    f_new.verifyUpdateCalledWith(20);
}