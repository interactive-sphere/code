#include "AverageFilterTests.h"

#include "unity_class_tests.h"

using namespace std;

using TestClass=AverageFilterTests;


void TestClass::runTests(){
    RUN_CLASS_TEST(AverageFilterTests, test_initial_value);
}

void TestClass::test_initial_value(){
    bool updated = average.update(10);

    TEST_ASSERT_EQUAL_MESSAGE(10, average.current(),
        "Initial average should be the first value");


    TEST_ASSERT_TRUE_MESSAGE(updated,
        "Should have updated on first run");
}