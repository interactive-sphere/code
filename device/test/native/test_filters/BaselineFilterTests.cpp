#include "BaselineFilterTests.h"

#include "log.h"

#include "ArduinoFramework.h"
#include <vector>

using namespace std;

using TestClass=BaselineFilterTests;

void run_test_filter_without_calibration(){
    TestClass().test_filter_without_calibration();
}

void run_test_filter_with_boundary_but_without_calibration(){
    TestClass().test_filter_with_boundary_but_without_calibration();
}

void run_test_update_while_calibrating()
{ TestClass().test_updating_while_calibrating();}

void run_test_filter_baseline_from_measure()
{ TestClass().test_filter_baseline_from_measure();}

void run_test_filter_with_threshold_boundaries()
{ TestClass().test_filter_with_threshold_boundaries();}

void run_test_reset_calibration(){
    TestClass().test_reset_calibration();
}


void TestClass::runTests(){
    RUN_TEST(run_test_filter_without_calibration);
    RUN_TEST(run_test_filter_with_boundary_but_without_calibration);
    RUN_TEST(run_test_update_while_calibrating);
    RUN_TEST(run_test_filter_baseline_from_measure);
    RUN_TEST(run_test_filter_with_threshold_boundaries);
    RUN_TEST(run_test_reset_calibration);
}

// --------------------------------------------------------------------------

static const std::vector<long> idle_measures = {
    7814, 7816, 7819, 7819, 7819, 7819, 7819, 7818, 7817,
    7817, 7817, 7816, 7815, 7815, 7814, 7814, 7814, 7814,
    7814, 7814, 7814, 7814, 7814, 7814, 7814, 7814, 7814,
    7814, 7814, 7815, 7816, 7816, 7818, 7819, 7820, 7821,
    7821, 7820, 7819, 7818, 7817, 7817, 7817, 7818, 7818,
    7818, 7819, 7819, 7819, 7819, 7819, 7819, 7819, 7819,
    7818, 7817, 7815, 7814, 7814, 7813, 7813, 7813, 7813,
    7813, 7813, 7814, 7814, 7815, 7815, 7815, 7816, 7816,
    7816, 7816, 7815, 7815, 7815, 7815, 7815, 7815, 7815,
    7815, 7815, 7815, 7816, 7817, 7817, 7816, 7814, 7812,
    7812, 7811, 7811, 7811, 7809, 7808, 7808, 7809, 7810,
    7811, 7813, 7815, 7816, 7818, 7820, 7821, 7821, 7821,
    7821, 7821, 7821, 7820, 7820, 7820, 7821, 7821, 7822,
    7823, 7824, 7824, 7824, 7824, 7823, 7823, 7822, 7821,
    7821, 7820, 7820, 7820, 7820, 7821, 7821, 7822, 7823,
    7824, 7825, 7825, 7826, 7825, 7825, 7824, 7824, 7824,
    7824, 7824, 7823
};


TestClass::BaselineFilterTests()
  : filter()
{
}

void TestClass::test_filter_without_calibration(){
    verifyFiltering(0, 0);
    verifyFiltering(30, 30);
    verifyFiltering(50, 50);
}

void TestClass::test_filter_with_boundary_but_without_calibration(){
    givenBoundaryFactorOf(1.0f);

    verifyFiltering(0, 0);
    verifyFiltering(30, 30);
    verifyFiltering(50, 50);
}

void TestClass::test_updating_while_calibrating(){
    filter.calibrationStart();

    bool updated = filter.update(1);

    TEST_ASSERT_EQUAL_MESSAGE(false, updated,
        "When calibrating, update should return false");
}

void TestClass::test_filter_baseline_from_measure(){
    whenCalibratedWith({1, 2, 3});

    verifyFiltering(3, 0);
    verifyFiltering(1, 0);
    verifyFiltering(-10, 0);

    verifyFiltering(4, 1);
}

void TestClass::test_filter_with_threshold_boundaries(){
    givenBoundaryFactorOf(1.0f);
    whenCalibratedWith({20, 10, 30});

    verifyFiltering(30, 0);
    verifyFiltering(50, 0);
    verifyFiltering(51, 1);
    verifyFiltering(70, 20);

    verifyFiltering(0, 0);
}

void TestClass::test_reset_calibration(){
    whenCalibratedWith({50, 40, 30});
    verifyFiltering(30, 0);

    filter.resetCalibration();

    verifyFiltering(30, 30);
}

// --------------------------------------------------------------------------

void TestClass::verifyFiltering(long measure, long expectedResult){
    filter.update(measure);
    auto filtered = filter.current();

    TEST_ASSERT_EQUAL_MESSAGE(expectedResult, filtered,
        "Filtered values does not match the expected"
    );
}



void TestClass::givenBoundaryFactorOf(float factor){
    filter.boundaryFactor(factor);
}

void TestClass::whenCalibrating(){
    filter.calibrationStart();
}

void TestClass::whenStopCalibration(){
  filter.calibrationStop();
}

void TestClass::whenCalibratedWith(const std::vector<long> & values){
  whenCalibrating();
  whenReading(values);
  whenStopCalibration();
}

void TestClass::whenReading(const std::vector<long> & values){
    for(auto v : values){
        filter.update(v);
    }
}
