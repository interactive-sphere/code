#include <unity.h>

#include <ArduinoFake.h>
#include "ArduinoFramework.h"

#include "BaselineTests.h"
#include "BaselineFilterTests.h"
#include "AverageFilterTests.h"
#include "MedianFilterTests.h"
#include "FiltersTest.h"
#include "FilterPipelineTests.h"

int main()
{
    UNITY_BEGIN();

    BaselineTests::runTests();
    BaselineFilterTests::runTests();
    AverageFilterTests::runTests();
    MedianFilterTests::runTests();
    FiltersTest::runTests();
    FilterPipelineTests::runTests();

    UNITY_END();
}
