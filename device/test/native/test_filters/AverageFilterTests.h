#ifndef AVERAGE_FILTER_TESTS_H
#define AVERAGE_FILTER_TESTS_H

#include "unity.h"

#include "RunningAverage.h"
#include "AverageFilter.h"
#include "ArduinoFramework.h"

#include <vector>


class AverageFilterTests {
public:
    static void runTests();

public:
    void test_initial_value();

protected:
    RunningAverage average;
};

#endif
