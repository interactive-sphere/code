#ifndef MOCKS__MOCK_READINGS_HPP
#define MOCKS__MOCK_READINGS_HPP

#include <exception>
#include <list>
#include <stdexcept>
#include <vector>

template<typename T>
class MockReadings{
public:
    using Readings = std::list<T>;

public:
    template<typename ReadingsColl=std::vector<T>>
    void setNextReadings(const ReadingsColl & readings){
        for(const T & value: readings){
            this->sensorReadings.push_back(value);
        }
    }
public:

    T nextReading(T defaultValue){
        if(!hasReadings()){
            return defaultValue;
        }

        return nextReading();
    }

    T nextReading(){
        if(sensorReadings.empty()){
            throw std::runtime_error("No readings left");
        }

        auto value = std::move(sensorReadings.front());
        sensorReadings.pop_front();

        return value;
    }

    bool hasReadings() const{
        return !empty();
    }

    bool empty() const{
        return sensorReadings.empty();
    }

protected:
    Readings sensorReadings;
};

#endif