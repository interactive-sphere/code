#ifndef FILTER_MOCK_HPP
#define FILTER_MOCK_HPP

#include "ArduinoFake.h"

#include "Filter.h"
#include "MockReadings.hpp"

#include <vector>

namespace filters{

using fakeit::Verify;
using fakeit::When;


template<typename T>
class FilterMock{
public:
    using FilterT = Filter<T>;
    using MockFilter = fakeit::Mock<FilterT>;

public:
    FilterMock(){
        setupMockup();
    }

public:
    FilterMock & mock(){ return filter; }
    FilterT    & get (){ return filter.get(); }

public:
    template<typename ReadingsColl=std::vector<T>>
    void setNextReadings(const ReadingsColl & values){
        this->readings.setNextReadings(values);
    }

public:

    void verifyUpdateCalledWith(const T & value){
        Verify(Method(filter, update).Using(value));
    }

    void verifyUpdateNotCalled(){
        Verify(Method(filter, update)).Exactly(0);
    }

protected:
    void setupMockup(){
        fakeit::When(Method(filter, update)).AlwaysDo([this](T value){
            if(readings.empty()){
                current = value;
            }
            else{
                current = readings.nextReading();
            }

            return updateReturns.nextReading(true);
        });
        When(Method(filter, current)).AlwaysDo([this](){
            return current;
        });
    }

public:
    MockReadings<bool> updateReturns;
protected:
    MockFilter filter;
    long current=0;
    MockReadings<long> readings;
};

};//end namespace

using filters::FilterMock;

#endif