#include "MedianFilterTests.h"

#include "unity_class_tests.h"

using namespace std;

using TestClass=MedianFilterTests;


void TestClass::runTests(){
    RUN_CLASS_TEST(MedianFilterTests, test_initial_value);
}

void TestClass::test_initial_value(){
    auto updated = median.update(10);

    TEST_ASSERT_EQUAL_MESSAGE(10, median.current(),
        "Initial median should be the first value");

    TEST_ASSERT_TRUE_MESSAGE(updated,
        "Should have updated on first run");
}