#ifndef BASELINE_FILTER_TESTS_H
#define BASELINE_FILTER_TESTS_H

#include "unity.h"

#include "BaselineFilter.h"
#include "ArduinoFramework.h"

#include <vector>


class BaselineFilterTests {
public:
    static void runTests();

public:
    BaselineFilterTests();

public:
    void test_updating_while_calibrating();
    void test_filter_without_calibration();
    void test_filter_with_boundary_but_without_calibration();
    void test_filter_baseline_from_measure();
    void test_filter_with_threshold_boundaries();
    void test_reset_calibration();

protected:
    void whenCalibrating();
    void whenStopCalibration();
    void whenCalibratedWith(const std::vector<long> & values);
    void whenReading(const std::vector<long> & v);

    void verifyFiltering(long measure, long expectedResult);

protected:
    void givenBoundaryFactorOf(float factor);

protected:
    BaselineFilter filter;
};

#endif
