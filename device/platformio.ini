; PlatformIO Project Configuration File
;
; Please visit documentation for the other options and examples
; https://docs.platformio.org/page/projectconf.html

[platformio]
default_envs = esp32dev


; CUSTOM options
; You need manually inject these options into a section
; using ${extra.<name_of_option>}
[extra]
wifi_manager_lib = https://github.com/tzapu/WiFiManager.git#430a9bcdcb7a066315de98f70c14706d14e83d7d
;
;
; configurations for OTA. Redefine here or use environment variables.
;
WIFI_SSID=${sysenv.WIFI_SSID}
WIFI_PASS=${sysenv.WIFI_PASS}
OTA_PASS=accept_my_OTA
MDNS_HOST=myesp32 ; default = esp32
;

[libs]
multiplexer = CD74HC4067@^1.0.0
json = ArduinoJson@6.14.1
circular_buffer = CircularBuffer@^1.3.3
capsensor = https://github.com/PaulStoffregen/CapacitiveSensor.git#8557da3ef781d537258c6c681a6d3d9b90c959c8
neopixelbus = NeoPixelBus@^2.5.6
spi = SPI
;
fmt = fmtlib/fmt
;
esp32_ble = ESP32 BLE Arduino
; ble_OTA = https://github.com/n0-0b0dy/ArduinoBleOTA
ble_OTA = https://github.com/n0-0b0dy/ArduinoBleOTA#dev
arduino_OTA = jandrassy/ArduinoOTA@^1.0.9
crc32 = bakercp/CRC32@^2.0.0
;
nimble = h2zero/NimBLE-Arduino@^1.4.1

[native_libs]
arduino_fake = ArduinoFake


[env:native]
platform = native
lib_ignore =
	actuators
	bluetooth
	gyroscope_mpu6050
lib_compat_mode = off
lib_deps =
	${libs.multiplexer}
	${libs.json}
	${libs.circular_buffer}
	${libs.fmt}
	;
	${native_libs.arduino_fake}
	;
	; local_libs:
	domain
	filters
	FakeCapacitiveSensor
	services
lib_extra_dirs =
	./native_libs
build_flags =
	-std=c++11
	-D NATIVE
	-D TEST_DIR=\"${platformio.test_dir}\"
test_build_src = false
test_filter =
	native/*

[env:esp32_wifi-OTA]
; By default use env variables to define WIFI and OTA info
build_flags =
	-DWIFI_SSID=\"${extra.WIFI_SSID}\"
	-DWIFI_PASS=\"${extra.WIFI_PASS}\"
	-DOTA_PASS=\"${extra.OTA_PASS}\"
	-DMDNS_HOST=\"${extra.MDNS_HOST}\"
;
; Uncomment to upload using OTA (over-the-air)
; upload_flags =
; 	--auth=${extra.OTA_PASS}
; upload_protocol = espota
; upload_port = ${extra.MDNS_HOST}.local

[BleOTA]
lib_deps =
	${libs.ble_OTA}
	${libs.arduino_OTA}
	${libs.crc32}
	${libs.circular_buffer}
	; ${libs.nimble}
	${libs.esp32_ble}
build_flags =
; 	-D USE_NIM_BLE_ARDUINO_LIB
	-D USE_BLE_ESP32

[env:esp32dev]
; restricting to 3.0 because newer versions conflict with CapacitiveSensor library
platform = espressif32@^3.0.0
framework = arduino
board = esp32dev
lib_deps =
	${libs.multiplexer}
	${libs.capsensor}
	${libs.neopixelbus}
	${libs.json}
	${libs.circular_buffer}
	${libs.spi}
lib_ignore =
	ArduinoFake
lib_extra_dirs =
	./external_libs
test_ignore =
	native_tests
	native/*
test_filter = test_orientation


[env:sphere_ble]
extends = env:esp32dev
build_src_filter = +<../prototypes/sphere-ble>
lib_deps =
	${libs.multiplexer}
	${libs.capsensor}
	${libs.neopixelbus}
	${libs.json}
	${libs.circular_buffer}
	${libs.spi}
;
	${BleOTA.lib_deps}
build_flags =
	${BleOTA.build_flags}


[env:sphere_serial]
extends = env:esp32dev
build_src_filter = +<../prototypes/sphere/sphere-serial>

; ------------ prototypes -------------------------------------------------

[env:proto_accel]
extends = env:esp32dev
build_src_filter = +<../prototypes/accelerometer>
lib_deps =
	${libs.spi}

[env:proto_measure_pressure]
extends = env:esp32dev
build_src_filter = +<../prototypes/measure_pressure_sensors>
lib_deps =
	${libs.multiplexer}
	${libs.capsensor}
	${libs.circular_buffer}
	${libs.spi}
	${libs.neopixelbus}


[env:proto_read_cap]
extends = env:esp32dev
build_src_filter = +<../prototypes/read_cap_sensor>
lib_deps =
	${libs.capsensor}
	${libs.multiplexer}
	board
	domain
	helpers
	capacitive_pressure
	services
	filters
	; WiFiManager
	${extra.wifi_manager_lib}

[env:proto_pressure_sensors]
extends = env:proto_read_cap
build_src_filter = +<../prototypes/read_pressure_sensors>

[env:proto_detect_pressure]
extends = env:proto_read_cap
build_src_filter = +<../prototypes/detect_pressure>


[env:blink_leds]
extends = env:esp32dev
build_src_filter = +<../prototypes/led/blink_leds>
lib_deps =
	${libs.neopixelbus}


[env:BleSerial]
extends = env:esp32dev
build_src_filter = +<../prototypes/bluetooth/BleSerial_basic>
lib_deps =
	${libs.esp32_ble}
; 	${libs.nimble}
build_flags =
; 	${BleOTA.build_flags}


[env:Ble_multiservice]
extends = env:esp32dev
build_src_filter = +<../prototypes/bluetooth/Ble_multiservice>
lib_deps = ${libs.esp32_ble}
build_flags =

; ------------ OTA prototypes ------------------------------------------------


[env:BleStream_with_OTA]
extends = env:esp32dev
build_src_filter = +<../prototypes/bluetooth/BleStream_with_OTA>
lib_deps =
	${BleOTA.lib_deps}
build_flags =
	${BleOTA.build_flags}


[env:ArduinoBleOTA_basic]
extends = env:esp32dev
build_src_filter = +<../prototypes/OTA/ArduinoBleOTA_basic>
lib_deps =
	${BleOTA.lib_deps}
build_flags =
	${BleOTA.build_flags}


[env:ble_ota_dfu_basic]
extends = env:esp32dev
build_src_filter = +<../prototypes/OTA/ble_ota_dfu_basic>
lib_deps =
	ble_ota_dfu
	h2zero/NimBLE-Arduino@^1.4.1
board_build.partitions = default_ffat.csv
; board_build.partitions = default.csv

[env:basic_OTA]
extends = env:esp32dev
build_src_filter = +<../prototypes/OTA/basic_OTA>
lib_deps =
	SPI

[env:proto_wifimanager]
extends = env:esp32dev
build_src_filter = +<../prototypes/wifi/wifimanager>
lib_deps =
	${extra.wifi_manager_lib}


[env:OTA-wifi]
extends = env:esp32dev
build_src_filter = +<../prototypes/OTA/OTA-wifi>
lib_deps =
	SPI
	${extra.wifi_manager_lib}