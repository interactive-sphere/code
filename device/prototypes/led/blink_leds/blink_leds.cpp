// Adapted from: NeoPixelFunFadeInOut example

#include "NeoPixelLedServices.h"
#include "LedAnimator.h"
#include "FadeAnimation.h"

#include "BoardPinout.h"

#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>

#include <Arduino.h>


NeoPixelLedServices<> ledServices{LEDRGB_COUNT, LEDRGB_PIN};
FadeAnimation blinker{&ledServices.ledAnimator()};

long lastUpdate = 0;
long changeTime = 5000;


void SetRandomSeed()
{
    uint32_t seed;

    // random works best with a seed that can use 31 bits
    // analogRead on a unconnected pin tends toward less than four bits
    seed = analogRead(0);
    delay(1);

    for (int shifts = 3; shifts < 31; shifts += 3)
    {
        seed ^= analogRead(0) << shifts;
        delay(1);
    }

    randomSeed(seed);
}

// * luminance: 0.0 = black, 0.25 is normal, 0.5 is bright
//
RgbColor randomColor(float luminance = 0.2f ){
    // Generate a random color
    // we use HslColor object as it allows us to easily pick a hue
    // with the same saturation and luminance so the colors picked
    // will have similiar overall brightness
    auto hue = random(360)/360.0f;
    auto saturation = 1.0f;

    return HslColor(hue, saturation, luminance);
}

void setup()
{
    Serial.begin(115200);

    SetRandomSeed();

    ledServices.begin();
    blinker.start(randomColor());
    lastUpdate = millis();
}

void loop()
{
    ledServices.update();

    if(millis() - lastUpdate >= changeTime){
        lastUpdate = millis();

        blinker.changeColor(randomColor());
    }
}
