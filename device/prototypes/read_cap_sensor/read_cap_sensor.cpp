#include <ArduinoFramework.h>

#include "reader/CapSenseReader.h"
#include "BoardPinout.h"

#include "reader/Multiplexer.h"

#include <array>

#define BAUD_RATE 115200
// #define BAUD_RATE 38400

#define NUM_CHANNELS 16

class ReadCapacitancePrototype {
protected:
  CapSenseReader capReader;
  Multiplexer mux;
  std::array<long, NUM_CHANNELS> readings;
public:
  ReadCapacitancePrototype ()
    : capReader(CAPSENSE_RCV_PIN, CAPSENSE_SND_PIN)
    , mux(MUX_S0_PIN, MUX_S1_PIN, MUX_S2_PIN, MUX_S3_PIN)
  {}
  virtual ~ReadCapacitancePrototype (){}

public:

  void setup(){
    Serial.begin(BAUD_RATE);

    mux.channel(0);
  }

  void loop(){

    // Serial.println(capReader.read());
    read_caps();
    displayReadings();
    delay(100);
  }

  void read_caps(){
    for(auto i=0; i < NUM_CHANNELS; ++i){
      mux.channel(i);
      this->readings[i] = capReader.read();
    }
  }

  void displayReadings(){
    for(auto i=0; i < NUM_CHANNELS; ++i){
      if(i > 0){
        Serial.print(", ");
      }
      Serial.print(i); Serial.print(":"); Serial.print(readings[i]);
    }
    Serial.println();
  }
};

ReadCapacitancePrototype app;

void setup() {
  app.setup();
}
void loop() {
  app.loop();
}
