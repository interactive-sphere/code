/*
    Adapted from 'uart' example in BLE Esp32 library
*/

#include <Arduino.h>

#include "BleStream.h"
#include "BleManager.h"

BleManager ble;
BleStream bleStream;
bool wasConnected = false;

void setup() {
    Serial.begin(115200);
    delay(2000);

    // Create the BLE Device
    ble.begin("UART Service");
    bleStream.begin(ble.server());
    ble.startAdvertising();

    Serial.println("Waiting a client connection to notify...");
}

void loop() {

    if (bleStream && Serial) {
        bool flush = false;

        while (bleStream.available()) {
            Serial.write((char)bleStream.read());
        }
        while (Serial.available()) {
            char c = Serial.read();
            bleStream.write(c);

            //flush on new line
            flush = flush || (c == '\n');
        }

        if(flush){
            bleStream.flush();
        }
    }

    // disconnecting
    if (wasConnected && !bleStream.isConnected()) {
        Serial.println("disconnecting");
    }
    // connecting
    if (!wasConnected && bleStream.isConnected()) {
        Serial.println("connected");
    }
    wasConnected = bleStream.isConnected();
}
