#include "BleUartService.h"
#include "BleManager.h"
#include "BleStream.h"

#include "ArduinoBleOTAEsp32.h"
#include "BleOtaUuids.h"

#define DEVICE_NAME "BleStream_with_OTA"

BleManager bleManager;

// BleUartService bleStream;
BleStream bleStream;
// BleUartService otaService{
//   BLE_OTA_SERVICE_UUID, // service UUID
//   BLE_OTA_CHARACTERISTIC_UUID_RX, // RX UUID
//   BLE_OTA_CHARACTERISTIC_UUID_TX  //TX UUID
// };
ArduinoBleOTAEsp32 bleOta;


void echoData(BleUartService * srv, int uartId, const std::string & data);

void setup() {
    using namespace std::placeholders;

    Serial.begin(115200);
    Serial.println("Starting BLE work!");

    bleManager.begin(DEVICE_NAME);
    bleStream.begin(bleManager.server());
    bleOta.begin(bleManager.server(), InternalStorage);

    bleManager.startAdvertising();

    Serial.println("Setup done!");
}

bool wasConnected = false;

void loop() {
    bleOta.pull();

    bool connected = bleManager.isConnected();

    if(wasConnected != connected){
        Serial.print("connected -> ");
        Serial.println(connected);
        wasConnected = connected;
    }

    if (bleStream && Serial) {
        if (bleStream.available() > 0) {
            auto available = bleStream.available();
            uint8_t buffer[available];

            bleStream.readBytes(buffer, available);

            Serial.print("got: ");
            Serial.write(buffer, available);
            Serial.println();

            bleStream.print("echo: ");
            bleStream.write(buffer, available);
            bleStream.println();
            bleStream.flush();
        }
    }
}

void echoData(BleUartService * srv, int uartId, const std::string & data){
    Serial.print("got: ");
    Serial.println(data.c_str());

    auto reply = "[" + String(uartId) + "] echo: " + data.c_str();

    srv->send((const uint8_t *)reply.c_str(), reply.length());
}