/*
    Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleServer.cpp
    Ported to Arduino ESP32 by Evandro Copercini
    updates by chegewara
*/

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

#include "BleUartService.h"
#include "BleManager.h"

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/


#define DEVICE_NAME "BLE_multi-service"
#define READ_AND_WRITE BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE

BleManager bleManager;

BleUartService defaultUart;
BleUartService uart1{
  "4fafc201-1fb5-459e-8fcc-c5c9c331914c", // service UUID
  "5851f57d-ca89-45e9-9ac5-21bd24b8cd48", // RX UUID
  "7dcbd23c-3ef5-4b68-b785-55a60163427d"  //TX UUID
};
BleUartService uart2{
  "58dff02b-86b5-494b-8701-f59d6b670916", // service UUID
  "af802889-d1e1-4ca2-896a-20fd345c2fe4", // rx UUID
  "826b57bb-9db4-47d3-86dc-43fd77741a0c"  // tx UUID
};

bool wasConnected = false;


void echoData(BleUartService * srv, int uartId, const std::string & data);

void setup() {
    using namespace std::placeholders;

    Serial.begin(115200);
    Serial.println("Starting BLE work!");

    bleManager.begin(DEVICE_NAME);
    auto * server = bleManager.server();

    defaultUart.setOnData(std::bind(echoData, &defaultUart, 0, _1));
    defaultUart.begin(server);

    uart1.setOnData(std::bind(echoData, &uart1, 1, _1));
    uart1.begin(server, {
        {"beb5483e-36e1-4688-b7f5-ea07361b26a8", READ_AND_WRITE,
        "Hello World, human!"}
    });

    uart2.setOnData(std::bind(echoData, &uart2, 2, _1));
    uart2.begin(server, {
        {"c391b59f-3481-4f8b-afbb-69fc7789a3f8", READ_AND_WRITE,
        "Hello World from upside down."}
    });

    bleManager.startAdvertising();

    Serial.println("Characteristic defined! Now you can read it in your phone!");
}

void loop() {
    bool connected = bleManager.isConnected();

    if(wasConnected != connected){
        Serial.print("connected -> ");
        Serial.println(connected);
        wasConnected = connected;
    }
}

void echoData(BleUartService * srv, int uartId, const std::string & data){
    Serial.print("got: ");
    Serial.println(data.c_str());

    auto reply = "[" + String(uartId) + "] echo: " + data.c_str();

    srv->send((const uint8_t *)reply.c_str(), reply.length());
}