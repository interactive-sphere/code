
#include <SPI.h> //required by NeoPixelsBus
// #include <BluetoothSerial.h>

#include "CapPressureServices.h"
#include "GyroscopeServices.h"
#include "BuzzerSoundServices.h"
#include "VibrationMotor.h"
#include "NeoPixelLedServices.h"

#include "SphereServices.h"

#include "SphereApi.h"

#include "BoardSensors.h"
#include "BoardPinout.h"
#include "BoardConfig.h"


#include <Update.h>
// #include "OtaBleManager.h"
#include "ArduinoBleOTAEsp32.h"
#include "BleStream.h"
#include "BleManager.h"


#include "log.h"


#define DEVICE_NAME DEFAULT_DEVICE_NAME "-ota"

GyroscopeServices gyroscopeServices;
CapPressureServices pressureServices{std::move(boardSensors())};
BuzzerSoundServices buzzer{BUZZER_PIN, BUZZER_CHANNEL};
VibrationMotor vibrationMotor{VIBRATION_PIN};
NeoPixelLedServices<> ledServices{LEDRGB_COUNT, LEDRGB_PIN};

SphereServices sphereServices{{
    &ledServices,
    &vibrationMotor,
    &buzzer,
    &gyroscopeServices,
    &pressureServices
}};


BleManager bleManager;
// OtaBleManager otaManager;
ArduinoBleOTAEsp32 bleOTA;

BleStream bleStream;
api::SphereApi sphereApi{&bleStream, sphereServices};

long lastUpdate;


void setup() {
    Serial.begin(115200);
    delay(3000);

    bleManager.begin(DEVICE_NAME);
    bleOTA.begin(bleManager.server(), InternalStorage);
    bleStream.begin(bleManager.server());

    bleManager.startAdvertising();
    // if (!otaManager.begin()) {
    //     Serial.println("OTA setup failed!");
    // }
    // else{
    //   Serial.println("Started ArduinoBleOTA");
    // }

    lastUpdate = millis();


    // sphereServices.begin();
    // sphereApi.setup();
}

bool wasConnected = false;
bool updating = false;

void echoStream(){
    if (bleStream.available() <= 0) {
        return;
    }

    Serial.println("received");

    auto available = bleStream.available();
    uint8_t buffer[available];

    bleStream.readBytes(buffer, available);

    Serial.print("got: \"");
    Serial.write(buffer, available);
    Serial.print("\"");
    Serial.println();

    // bleStream.print("echo: ");
    // bleStream.write(buffer, available);
    // bleStream.println();
    // bleStream.flush();

}

void loop() {
  // otaManager.update();

  // if(millis() - lastUpdate > 1500){
  //   Serial.print(".");
  //   lastUpdate = millis();
  // }

#if defined(BLE_PULL_REQUIRED)
    BLE.poll();
#endif
    bleOTA.pull();

    if(bleOTA.isUpdating() != updating){
        updating = bleOTA.isUpdating();
        Serial.print("updating ->  "); Serial.println(updating);
    }

    if(wasConnected != bleManager.isConnected()){
        // if(wasConnected){ //disconnection
        //     sphereApi.disconnected();
        // }

        wasConnected = bleManager.isConnected();
        Serial.print("connected -> "); Serial.println(wasConnected);
    }

    if(!bleOTA.isUpdating()){
        // sphereServices.update();

        if (bleStream.isConnected()){
            // sphereApi.update();

            echoStream();
        }
    }
}