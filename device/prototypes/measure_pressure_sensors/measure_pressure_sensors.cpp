#include <ArduinoFramework.h>

#include "CapPressureServices.h"
#include "NeoPixelLedServices.h"

#include "BoardSensors.h"
#include "BoardPinout.h"

// Enable Over-the-air (OTA) installations
// #include "OTA-wifi-loop.h"


#include "CommandReader.h"


#define BAUD_RATE 115200
// #define BAUD_RATE 38400

#define NUM_CHANNELS 16
// #define NUM_CHANNELS 4



class MeasureSensorsPrototype {
public:
  CapPressureServices pressureServices;
  NeoPixelLedServices<> ledServices;

  std::array<pressure::Measure, NUM_CHANNELS> readings;

  CommandReader cmdReader;

public:
  MeasureSensorsPrototype ()
    : pressureServices(std::move(boardSensors()))
    , ledServices(LEDRGB_COUNT, LEDRGB_PIN)
  {}
  virtual ~MeasureSensorsPrototype (){}

public:

  void setup(){
    Serial.begin(BAUD_RATE);

    pressureServices.begin();
    ledServices.begin();

    pressureServices.stateNotifier(&ledServices.stateNotifier());
  }

  void loop(){
    receiveCommand();
    readSensors();
    printSensorsReadings();
    ledServices.update();

    delay(100);
  }

protected:
  void receiveCommand(){
    cmdReader.update();

    while(cmdReader.hasNext()){

      auto args = cmdReader.next();
      printCommand(args);

      if(args.empty()){
        continue;
      }

      switch(args[0][0]){
        case 'c'://calibrate
          calibrate();
          break;
        default: break;
      }
    }


  }

  void printCommand(const CommandReader::ArgsList & args){
      Serial.print("command: ");
      for(auto & arg: args){
        Serial.print(arg);
        Serial.print(' ');
      }
      Serial.println();
  }

  void readSensors(){
    pressureServices.update();

    for(auto i=0; i < pressureServices.sensorsCount() && i < NUM_CHANNELS; ++i)
    {
      readings[i] = readSensor(i);
    }
  }

  void printSensorsReadings(){
    Serial.print("c: ");  Serial.print(pressureServices.isCalibrating());
    Serial.print(", ");

    for(auto i=0; i < pressureServices.sensorsCount() && i < NUM_CHANNELS; ++i)
    {
      if(i > 0){
        Serial.print(", ");
      }

      const auto & desc = pressureServices.getSensorDescription(i);
      printMeasure(readings[i], desc, Serial);
    }
    Serial.println();
  }

protected:
  void calibrate(){
    pressureServices.calibrateFor(3000);
  }

protected:
  pressure::Measure readSensor(const pressure::SensorDescription & desc){
    return readSensor(desc.id());
  }
  pressure::Measure readSensor(pressure::SensorId id){
    return pressureServices.getMeasure(id);
  }

  void printMeasure(
      const pressure::Measure & m,
      const pressure::SensorDescription & desc,  Print & out)
  {
    out.print(desc.name()); out.print(":"); out.print(m.measure);
    out.print(", ");
    out.print(desc.name() + "_raw"); out.print(":"); out.print(m.rawValue);
  }

protected:

};

MeasureSensorsPrototype app;

void setup() {
  app.setup();
}
void loop() {
  app.loop();
}
