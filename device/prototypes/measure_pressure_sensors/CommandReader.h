#pragma once

#include <list>
#include <vector>

#include <ArduinoFramework.h>


class CommandReader{
public:
    using ArgsList = std::vector<String>;

protected:
    std::list<ArgsList> readedLines;
    ArgsList readingWords;
    String strBuffer;
public:
    void update(){
        while(Serial.available()){
            int value = Serial.read();
            if(value == -1)
                return;

            char c = (char) value;

            //read until newline
            if(!isSpace(c)){
                strBuffer += c;
            }
            else{
                finishedWord(c);
            }
        }
    }

    bool hasNext() const{
        return !readedLines.empty() && !readedLines.front().empty();
    }

    ArgsList next(){
        if(!hasNext()){
            return {};
        }

        auto args = readedLines.front();
        readedLines.pop_front();

        return args;
    }

protected:
    void finishedWord(char lastChar){
        if(strBuffer.length() > 0){//finished word
            strBuffer.trim();
            readingWords.push_back(strBuffer);

            Serial.print("finished word: "); Serial.println(strBuffer);

            //clear current word
            strBuffer = String();
        }

        if(lastChar == '\n' //finished line
            && // and line not empy
            !readingWords.empty())
        {
            //start new line
            readedLines.push_back(readingWords);
            readingWords = {};
        }
    }


};