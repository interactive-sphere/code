#include <ArduinoFramework.h>

#include "reader/CapSenseReader.h"
#include "reader/Multiplexer.h"
#include "MuxReader.h"
#include "PressureSensorReader.h"
#include "PressureSensor.h"
#include "CapPressureServices.h"

#include "BoardPinout.h"
#include "BoardSensors.h"

#include <array>
#include <map>

// Enable Over-the-air (OTA) installations
#include "OTA-wifi-loop.h"


#define BAUD_RATE 115200
// #define BAUD_RATE 38400

#define NUM_CHANNELS 16


class SensorReaderGetter{
protected:
  CapSenseReader capReader;
  Multiplexer mux;
  std::array<MuxReader, NUM_CHANNELS> muxReaders;
  std::array<PressureSensorReader, NUM_CHANNELS> sensorReaders;
public:
  SensorReaderGetter ()
    : capReader(CAPSENSE_RCV_PIN, CAPSENSE_SND_PIN)
    , mux(MUX_S0_PIN, MUX_S1_PIN, MUX_S2_PIN, MUX_S3_PIN)
  {}

public:
  void setup(){
    for(auto i=0; i < NUM_CHANNELS; ++i){
      muxReaders[i] = MuxReader(i, &mux, &capReader);
      sensorReaders[i] = PressureSensorReader(&muxReaders[i]);
    }
  }
  void readSensor(std::size_t i){
    // mux.channel(i);
    sensorReaders[i].update();
  }

  long getReading(std::size_t i){
    return sensorReaders[i].currentUnfiltered();
  }
};


class PressureSensorGetter{
protected:
  CapSenseReader capReader;
  Multiplexer mux;
  std::array<MuxReader, NUM_CHANNELS> muxReaders;
  std::array<PressureSensor, NUM_CHANNELS> sensorReaders;
public:
  PressureSensorGetter ()
    : capReader(CAPSENSE_RCV_PIN, CAPSENSE_SND_PIN)
    , mux(MUX_S0_PIN, MUX_S1_PIN, MUX_S2_PIN, MUX_S3_PIN)
  {}

public:
  void setup(){
    for(auto i=0; i < NUM_CHANNELS; ++i){
      muxReaders[i] = MuxReader(i, &mux, &capReader);
      sensorReaders[i] = PressureSensor(i, &muxReaders[i]);
    }
  }
  void readSensor(std::size_t i){
    // mux.channel(i);
    sensorReaders[i].update();
  }

  long getReading(std::size_t i){
    return sensorReaders[i].currentMeasure().rawValue;
  }
};



class PressureServiceSensorGetter{
protected:
  CapPressureServices pressureSrv;
public:
  PressureServiceSensorGetter ()
    : pressureSrv(boardSensors())
  {}

public:
  void setup(){
    pressureSrv.begin();
  }

  void listenEvent(PressureListener listener){
    pressureSrv.listenPressureEvents(listener);
  }

  void readSensors(){
    // mux.channel(i);
    pressureSrv.update();
  }

  const pressure::Measure & getMeasure(std::size_t i){
    return pressureSrv.getMeasure(i);
  }

  long getReading(std::size_t i){
    return getMeasure(i).rawValue;
  }
};

class PressureStateHolder{
public:
    using Evt = pressure::Event;

protected:
    pressure::EventListener _pressureListener;
    std::map<pressure::SensorId, pressure::EventType> sensorsState;

public:
    PressureStateHolder()
    {
        this->_pressureListener = [this](const pressure::Event e){
            this->onPressureEvent(e);
        };
    }

    bool isPressed(pressure::SensorId id){
        auto found = this->sensorsState.find(id);

        if(found == sensorsState.end()) return false;

        return found->second == pressure::EventType::Press
            && found->second == pressure::EventType::PressContinue;
    }

    PressureListener pressureListener() const{
        return this->_pressureListener;
    }

protected:
    void onPressureEvent(const pressure::Event & evt){
        this->sensorsState[evt.sensorId] = evt.evtType;
    }

};


enum class DisplayMode{ Raw, Filtered, Percent, Events };


class ReadCapacitancePrototype {
protected:
  // SensorReaderGetter sensorsReader;
  // PressureSensorGetter sensorsReader;
  PressureServiceSensorGetter sensorsReader;
  DisplayMode currentMode = DisplayMode::Raw;
  PressureStateHolder pressureState;
  bool changedState=true;

public:

  void setup(){
    Serial.begin(BAUD_RATE);

    sensorsReader.setup();

    sensorsReader.listenEvent([this](const pressure::Event & evt){
        pressureState.pressureListener()(evt);
        changedState = true;
    });
  }

  void loop(){

    // Serial.println(capReader.read());
    read_caps();
    get_command();
    displayReadings();
    delay(30);
    // delay(20);
  }

  void read_caps(){
    // for(auto i=0; i < NUM_CHANNELS; ++i){
    //   sensorsReader.readSensor(i);
    // }
    sensorsReader.readSensors();
  }

  void get_command(){
    if(Serial.available()){
      int cmd = Serial.read();
      if(cmd == -1) return;

      switch (cmd) {
        case 'e':
          changeMode(DisplayMode::Events);
          changedState = true;
          break;
        case 'r':
          changeMode(DisplayMode::Raw); break;
        case 'f':
          changeMode(DisplayMode::Filtered); break;
        case 'p':
          changeMode(DisplayMode::Percent); break;
        default:
          return;
      }
    }
  }

  void changeMode(DisplayMode newMode){
    this->currentMode = newMode;
  }

  void displayReadings(){
    switch (currentMode) {
      case DisplayMode::Events:
        displayPressureState();
        break;
      default:
        displayMeasure();
        break;
    }
  }
  void displayMeasure(){
    for(auto i=0; i < NUM_CHANNELS; ++i){
      if(i > 0){
        Serial.print(", ");
      }
      Serial.print(i); Serial.print(":");

      printMeasure(i);
    }
    Serial.println();
  }

  void printMeasure(pressure::SensorId id){
    auto measure = sensorsReader.getMeasure(id);

    switch (currentMode) {
      case DisplayMode::Filtered:
        Serial.print(measure.measure);
        break;
      case DisplayMode::Percent:
        Serial.print(measure.percentValue);
        break;
      case DisplayMode::Raw:
      default:
        Serial.print(measure.rawValue);
        break;
    }
  }

  void displayPressureState(){
      // if(!changedState){
      //     return;
      // }

      for(auto i=0; i < NUM_CHANNELS; ++i){
        if(i > 0){
          Serial.print(", ");
        }
        auto pressed = this->pressureState.isPressed(i);

        Serial.print(i); Serial.print(":");
        Serial.print(pressed ? 1 : 0);
      }
      Serial.println();

      changedState = false;
  }
};

ReadCapacitancePrototype app;

void setup_app() {
  app.setup();
}
void loop_app() {
  app.loop();
}
