#include <ArduinoFramework.h>

#include "reader/CapSenseReader.h"
#include "BoardPinout.h"
#include "BoardConfig.h"

#include "reader/Multiplexer.h"
#include "PressureSensorReader.h"
#include "PressureEventDetector.h"

#include "OTA-wifi-loop.h"

#include <array>
#include <list>
#include <vector>

#define BAUD_RATE 115200
// #define BAUD_RATE 38400


using namespace pressure;

class TriggerState{
public:
  enum Value{
      None, High, Middle, Low
  };

  TriggerState() = default;
  constexpr TriggerState(Value value) : value(value) { }

  operator Value() const { return value; }

public:
    const char * c_str() const{
        switch (value){
        case None:    return "None";
        case High:    return "High";
        case Middle:  return "Middle";
        case Low:     return "Low";
        default:      return "Unknown";
        }
    }

protected:
  Value value;
};

class CustomPressureDetector: public PressureEventDetector{
public:
    using PressureEventDetector::PressureEventDetector;

public:
    TriggerState getTriggerState() const{
      if(levelTrigger.isHigh()) return TriggerState::High;
      else if(levelTrigger.isMiddle()) return TriggerState::Middle;
      return TriggerState::Low;
    }

    bool debounceState() const{
      return debounce.state();
    }

    float debouncePercent() const{
      return debounce.countToChange() / (float)debounce.countThreshold();
    }
public:
    inline LevelTrigger<float> & getLevelTrigger(){return levelTrigger;}
};

class DetectPressurePrototype {
protected:
  using WordsList = std::vector<String>;

protected:
  CapSenseReader capReader;
  Multiplexer mux;
  PressureSensorReader sensorReader;
  int channel=0;
  String channelStr = "0";
  unsigned delayTime=100;
  // PressureEventDetector eventDetector;
  CustomPressureDetector eventDetector;
  pressure::Event lastEvent;

  std::list<WordsList> readedLines;
  WordsList readingWords;
  String strBuffer;

  Measure currentMeasure;
  String stateString;
  EventType currentState = EventType::None;
  TriggerState currentTriggerState = TriggerState::None;
  bool currentDebounceState = false;

  bool showMeasures = true;
  bool showRaw = true;


public:
  DetectPressurePrototype ()
    : capReader(CAPSENSE_RCV_PIN, CAPSENSE_SND_PIN)
    , mux(MUX_S0_PIN, MUX_S1_PIN, MUX_S2_PIN, MUX_S3_PIN)
    , sensorReader(&capReader)
    , lastEvent(0, EventType::Release, 0, 0, 0)
  {
  }

public:

  void setup(){
    Serial.begin(BAUD_RATE);

    mux.channel(channel);

    stateString = stateToString();

    eventDetector.onPressureEvent([this](const pressure::Event & evt){
        Serial.println("Event: " + evt.toString());
        lastEvent = evt;
    });
  }

  void loop(){
    readInput();
    get_command();
    readMeasure();
    checkDetection();
    printState();

    delay(delayTime);
  }

  void readInput(){
    while(Serial.available()){
      int value = Serial.read();
      if(value == -1)
        return;

      char c = (char) value;

      Serial.println(c);

      //read until newline
      if(!isSpace(c)){
        strBuffer += c;
      }
      else{
        // Serial.println("got blank");
        finishedWord(c);
      }
    }
  }

  void finishedWord(char lastChar){
    if(strBuffer.length() > 0){//finished word
      strBuffer.trim();
      readingWords.push_back(strBuffer);

      Serial.print("finished word: "); Serial.println(strBuffer);

      //clear current word
      strBuffer = String();
    }

    if(lastChar == '\n' //finished line
      && // and line not empy
      !readingWords.empty())
    {
      //start new line
      readedLines.push_back(readingWords);
      readingWords = {};

      Serial.print("received line:");
      printWords(readedLines.back());
      Serial.println();
    }
  }

  void get_command(){
    if(readedLines.empty() || readedLines.front().empty()){
      return;
    }

    auto args = readedLines.front();
    readedLines.pop_front();

    Serial.println("got cmd: " + args[0]);

    switch (args[0][0]) {
      case 'm': showMeasures = !showMeasures; break;
      case 'r': showRaw = !showRaw; break;
      case 'c': readNewChannel(args); break;
      case 'b': toggleCalibrate(); break;
      case 'u': receiveUpperThreshold(args); break;
      case 'l': receiveLowerThreshold(args); break;
      case 's':
        if(args[0] == "set"){
          setConfig(args);
        }
      break;

      default:
        return;
    }
  }

  void setConfig(const WordsList & args){
    if(args.size() != 3) return;

    auto & cmd = args[1];
    auto & value = args[2];

    if(cmd == "av-window"){
      setAverageWindow(value.toInt());
    }
    else if(cmd == "med-window"){
      setMedianWindow(value.toInt());
    }
    else if(cmd == "debounce"){
      eventDetector.debounceThreshold(value.toInt());
    }
    else if(cmd == "delay"){
      auto d = value.toInt();
      if(d > 0){
        this->delayTime = d;
      }
    }
  }

  void setAverageWindow(int averageWindowSize){
    resetSensorReader( sensorReader.medianWindowSize(), averageWindowSize);
  }
  void setMedianWindow(int medianWindowSize){
    resetSensorReader( medianWindowSize, sensorReader.averageWindowSize());
  }
  void resetSensorReader(int medianWindowSize, int averageWindowSize){
    sensorReader = PressureSensorReader(&capReader,
                    medianWindowSize, averageWindowSize);

    //read until have valid measures
    while(!sensorReader.update())
    {}
  }

  void printWords(const WordsList & words){
      Serial.print("[");
      for(const auto & word: words){
          Serial.print("'");
          Serial.print(word);
          Serial.print("'");
          Serial.print(", ");
      }
      Serial.print("]");
  }

  void readNewChannel(const WordsList & args){
    // Serial.print("readNewChannel: "); printWords(args);Serial.println();

    if(args.size() < 2) return;

    int newChannel = args[1].toInt();
    if(newChannel < 0
      || newChannel > MULTIPLEXER_CHANNELS)
    {
      return;
    }

    //change channel
    this->channel = newChannel;
    mux.channel(newChannel);
    this->channelStr = String(newChannel);

    //reset because new data cames from other sensor
    sensorReader.resetData();
    sensorReader.resetCalibration();
  }

  void toggleCalibrate(){
    if(! sensorReader.isCalibrating()){
      sensorReader.startCalibration();
    }
    else {
      sensorReader.stopCalibration();
    }
  }

  void receiveUpperThreshold(const WordsList & args){
    float t = receiveThreshold(args);

    if(t < 0) return;
    this->eventDetector.upperLevelThreshold(t);
  }
  void receiveLowerThreshold(const WordsList & args){
      float t = receiveThreshold(args);

      if(t < 0) return;
      this->eventDetector.lowerLevelThreshold(t);
  }

  float receiveThreshold(const WordsList & args){
    if(args.size() < 2) return -1;

    float f = args[1].toFloat();

    return (f < 0 || f > 1) ? -1 : f;
  }

  void readMeasure(){
    sensorReader.update();

    currentMeasure.setValues(
      sensorReader.relative(),
      sensorReader.current(),
      sensorReader.currentUnfiltered()
    );
  }

  void checkDetection(){
    this->eventDetector.update(currentMeasure);

    auto state = eventDetector.state();
    auto triggerState = eventDetector.getTriggerState();
    auto debounceState = eventDetector.debounceState();

    if(state != currentState
        || triggerState != currentTriggerState
        || debounceState != currentDebounceState)
    {
      currentState         = state;
      currentTriggerState  = triggerState;
      currentDebounceState = debounceState;

      stateString = stateToString();
    }
  }

  String stateToString(){
    return String(this->channel)
            // + "(" + currentState.c_str()
            // + "--" + currentTriggerState.c_str()
            // + (currentTriggerState == TriggerState::Middle
                            // ? String() : String("--") + currentDebounceState)
            // + ")"
            ;
  }

  void printState(){
    if(!showMeasures) return;

    printVariable(channelStr, currentMeasure.measure);

    if(showRaw){
      printVariable(channelStr+"_raw",
                  currentMeasure.rawValue  - sensorReader.baselineThreshold());
    }

    printVariable("state", relativeMeasure(stateToMeasure()));
    printVariable("min", sensorReader.minReadedValue());
    printVariable("max", sensorReader.maxReadedValue());
    printVariable("lower_threshold",
        relativeMeasure(eventDetector.lowerLevelThreshold()));
    printVariable("upper_threshold",
        relativeMeasure(eventDetector.upperLevelThreshold()));
    printVariable("evt", relativeMeasure(eventToMeasure(lastEvent)));

    Serial.println();
  }

  float stateToMeasure() const{
    bool isPressed = (currentState == EventType::Press
      || currentState == EventType::PressContinue);

    bool changing = (isPressed && currentTriggerState == TriggerState::Low)
                 || (!isPressed && currentTriggerState == TriggerState::High);

    if(currentTriggerState == TriggerState::Middle){  //middle
        if(isPressed){//stable pressed
          return 0.75f;
        }
        else{
          return 0.25f;
        }
    }
    else if(!changing){//stable
        if(isPressed) return 1.0f;
        else return 0.0f;
    }
    else { //changing
        float debounceIncrease = 0.25f * eventDetector.debouncePercent();
        if(currentTriggerState == TriggerState::High){//raising
          return 0.5f + debounceIncrease;
        }
        else{//falling
          return 0.5f - debounceIncrease;
        }
    }
  }

  float eventToMeasure(const pressure::Event & evt) const{
      switch (evt.evtType) {
        case pressure::EventType::Press:
        case pressure::EventType::PressContinue:
          return 1.1f;
        default:
          return -0.1f;
      }
  }

  float relativeMeasure(float value){
      auto range = sensorReader.maxReadedValue() - sensorReader.minReadedValue();

      return (value * range) + sensorReader.minReadedValue();
  }

  void printVariable(const String & var, float value){
    printVariable(var.c_str(), value);
  }
  void printVariable(const char * var, float value){
    Serial.print(var); Serial.print(":"); Serial.print(value);
    Serial.print('\t');
  }
};

DetectPressurePrototype app;

void setup_app() {
  app.setup();
}
void loop_app() {
  app.loop();
}
