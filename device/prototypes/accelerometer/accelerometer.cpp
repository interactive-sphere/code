#include <ArduinoFramework.h>

#include "Mpu6050Services.h"

// #define BAUD_RATE 115200
#define BAUD_RATE 38400 //used by pygarl


class AccelerometerPrototype {
public:
  using Vec3D = Vector3D<int16_t>;

private:
  Mpu6050Services gyroscopeServices;
  TimeCounter notifyInterval{100};

public:
  AccelerometerPrototype (){}
  virtual ~AccelerometerPrototype (){}

public:

  void setup(){
    Serial.begin(BAUD_RATE);
    Serial.println("starting...");

    gyroscopeServices.begin();
    gyroscopeServices.listenOrientation([this](const Vector<float> & measure){
      this->onAccelGyro(measure);
    }, {OrientationFormat::AccelGyro});

    // gyro().listen([this](const GyroscopeMeasure & measure){
    //   this->onMeasure(measure);
    // });
  }

  void loop(){
      gyroscopeServices.update();
      delay(10);
  }

protected:

  void onMeasure(const GyroscopeMeasure & measure){
    if(!notifyInterval.completed()){
      return;
    }
    notifyInterval.reset();


    Vec3D accelRaw, accel, gyroRaw;

    gyro().getRawAccelGyro(measure, accelRaw, gyroRaw);
    // gyro().getWorldAccel(measure, accel);
    gyro().getRealAccel(measure, accel);

    printData(accelRaw, gyroRaw, accel);
  }

  void onAccelGyro(const Vector<float> & measure){
    if(!notifyInterval.completed()){
      return;
    }
    notifyInterval.reset();

    pygarlPrint(measure);
  }

    void printData(
      const Vec3D & accelRaw, const Vec3D & gyroRaw, const Vec3D & accel)
    {
      // printValues(accelRaw, gyroRaw, accel);
      // pygarlPrint(accelRaw, gyroRaw);
      pygarlPrint(accel, gyroRaw);
    }

    void printValues(
      const Vec3D & accelRaw, const Vec3D & gyroRaw, const Vec3D & accel)
    {
      printVector(accelRaw);
      printVector(gyroRaw);
      printVector(accel);
      Serial.println("");
    }

    void pygarlPrint(const Vec3D & accel, const Vec3D & gyroVec)
    {
      Serial.print("START ");
      printVector(accel, " ");
      printVector(gyroVec, " ");
      Serial.println("END");
    }


    void pygarlPrint(const Vector<float> & measure)
    {
      Serial.print("START ");
      printVector<float, long>(measure, " ");
      Serial.println("END");
    }

protected:
  Gyroscope & gyro(){
    return gyroscopeServices.gyro();
  }

  template <typename T, typename Other=T>
  void printVector(const Vector<T> & vec, const char * sep = "\t"){
      for(int i=0; i < vec.size(); ++i){
          Serial.print(static_cast<Other>(vec.get(i)));
          Serial.print(sep);
      }
  }

};

AccelerometerPrototype app;

void setup() {
  app.setup();
}
void loop() {
  app.loop();
}
