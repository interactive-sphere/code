#ifndef WIFI_CONNECTOR_H
#define WIFI_CONNECTOR_H

#include <WiFi.h>

// Default parameters
//
#ifndef WIFI_SSID
#define WIFI_SSID NULL
#endif
#ifndef WIFI_PASS
#define WIFI_PASS NULL
#endif

class WifiConnector{
public:
  bool connect(
      const char * wifi_ssid=WIFI_SSID,
      const char * wifi_password=WIFI_PASS
  );

  bool connect( String wifi_ssid, String wifi_password );
};


#endif
