#include "WifiConnector.h"

#include "OTAManager.h"


bool WifiConnector::connect( String ssid, String wifiPassword)
{
  return connect(ssid.c_str(), wifiPassword.c_str());
}

bool WifiConnector::connect(
    const char * ssid,
    const char * wifiPassword)
{
  Serial.println("Booting");
  WiFi.mode(WIFI_STA);

  if(!ssid || !wifiPassword){
    Serial.println("missing wi-fi ssid or password");

    return false;
  }

  Serial.print("Connecting to: ");Serial.println(ssid);
  WiFi.begin(ssid, wifiPassword);

  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    return false;
  }

  Serial.println("Network ready!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  return true;
}
