#include "OTAManager.h"

#include "WifiConnector.h"

WifiConnector wifiConnector;
OtaManager otaManager;

void reboot(){
  delay(5000);
  ESP.restart();
}

void setup() {
  Serial.begin(115200);
  delay(1000);

  if(!wifiConnector.connect()){//Use default values
      Serial.println("Failed to connect. Rebooting.");
      reboot();
  };
  if (!otaManager.begin()) {
    Serial.println("OTA setup failed! Rebooting...");
    reboot();
  }
}

void loop() {
  otaManager.update();
}
