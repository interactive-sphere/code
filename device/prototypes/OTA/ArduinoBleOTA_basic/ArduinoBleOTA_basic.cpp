#include <ArduinoBleOTA.h>


long lastUpdate;

void setup() {
  Serial.begin(115200);
  delay(3000);

  if(!ArduinoBleOTA.begin("ArduinoBleOTA_basic", InternalStorage)){
    Serial.println("Failed to begin ArduinoBLeOTA");
  }
  else{
    Serial.println("Started ArduinoBleOTA");
  }

  lastUpdate = millis();
}


void loop() {
#if defined(BLE_PULL_REQUIRED)
  BLE.poll();
#endif
  ArduinoBleOTA.pull();

  if(millis() - lastUpdate > 1500){
    Serial.print(".");
    lastUpdate = millis();
  }
}