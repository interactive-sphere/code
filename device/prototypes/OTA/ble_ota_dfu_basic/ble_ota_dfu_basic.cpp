/* Copyright 2022 Vincent Stragier */

#include <Arduino.h>
#include <ble_ota_dfu.hpp>

BLE_OTA_DFU ota_dfu_ble;


void setup() {
  Serial.begin(115200);
  ota_dfu_ble.begin("Test OTA DFU");
}

void loop() {
  // Kill the holly loop()
  // Delete the task, comment if you want to keep the loop()
//   vTaskDelete(NULL);
    Serial.print("-");
    delay(1000);
}
