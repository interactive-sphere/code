#include <Arduino.h>

#include "WiFiConfigurator.h"

WiFiConfigurator wifiConfig;

void setup() {
    // WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
    // put your setup code here, to run once:
    Serial.begin(115200);

    //wait monitor to connect with serial
    delay(1000);

    //reset settings - wipe credentials for testing
    // wifiConfig.resetSettings();

    //automatically connect using saved credentials if they exist
    //If connection fails it starts an access point with the specified name
    if(wifiConfig.autoConnect(nullptr, "wifimanager-pass")){
        Serial.println("connected...yeey :)");
    }
    else {
        Serial.println("Configportal running");
    }
}

void loop() {
    wifiConfig.update();
    // put your main code here, to run repeatedly:
}
