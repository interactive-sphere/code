#include <ArduinoFramework.h>

#include "SphereDevice.h"
#include "log.h"

SphereDevice device{&Serial};

void setup() {
    Serial.begin(115200);
    device.begin();
}

void loop() {
    device.setConnected(Serial);
    device.update();
}
