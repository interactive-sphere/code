from ursina import *


class CommandDisplay(Button):
    def __init__(self, parent=scene, position=(1,1),
                       color=color.white, scale=2, **kwargs):

        super().__init__(parent=parent, position=position,
                         color=color, scale=scale, **kwargs)

        self.command = None
        # self.change_command(command)
        # self._load_texture()

    def change_command(self, new_cmd):
        self.command = new_cmd
        self.text = self.command.name
        self._load_texture()

        self.text_entity.scale *= .6
        self.text_entity.y = -.4
        # self.text_entity.world_y = self.world_y - .2
        self.text_entity.z = -.5
        # self.text_entity.world_parent = scene
        # self.text_entity.color = self.color.tint(-.5)


    def _load_texture(self):
        self.texture = "assets/" + self.command.texture

class ProgressBar(Button):
    def __init__(self, value=1.0, name='', x=0, color=None, **kwargs):
        if color is None:
            color = ursina.color.color(30*random.randint(0, 10), 1, .7)

        super().__init__(
            parent = scene,
            name = name,
            model = 'cube',
            # x = i - (len(names)/2),
            x = x,
            scale = (value * 2, .1, .5),
            color = color,
            origin_x = -.5,
            text = name,
            tooltip = Tooltip('00'), # to ensure uniform with
            **kwargs
        )
        self.tooltip.text = str(value)

        # self.text_entity.scale *= .4
        # self.text_entity.world_y = self.world_y - .2
        # self.text_entity.z = -.5
        # self.text_entity.world_parent = scene
        # self.text_entity.color = self.color.tint(-.5)

    def update_value(self, percent):
        self.scale_x = 2*percent


class PointsDisplay(Text):
    def __init__(self, points=0,
            color=color.color(100, 1, .7),
            resolution=100*Text.size,
            scale=5,
            y=0.4,
            x=-0.1,
            **kwargs):

        super().__init__(str(points),
            color=color,
            resolution=resolution,
            scale=scale,
            y=y, x=x, **kwargs)

        self.points = points

    def increment(self):
        self.points += 1
        self.text =str(self.points)