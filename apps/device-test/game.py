import ursina
from ursina import *

import random
import datetime
import time

from commands import *
from keyboard import KeyboardController
from ui import *
from counter import Counter



class Game(Entity):
    def __init__(self, controller, **kwargs):
        super().__init__(**kwargs)

        camera.orthographic = True
        camera.fov = 4
        camera.position = (1, 1)
        Text.default_resolution *= 2

        # mouse.visible = False

        self.bg = Entity(parent=scene, model='quad', texture='shore', scale=(16,8), z=10, color=color.light_gray)

        self.progress_bar = ProgressBar(color=color.blue, y=-.2)
        self.counter = Counter(1.5)
        self.counter.reset()

        self.cmd_display = CommandDisplay()
        self.points_display = PointsDisplay()

        self.controller = controller
        self.controller.on_command(self.on_command)

        self.change_command()

    def on_command(self, command):
        if command == self.current_command().cmd_id:
            self.complete(success=True)


    def current_command(self):
        return self.cmd_display.command

    def update(self):
        if self.counter.completed():
            self.complete(success=False)

        self.progress_bar.update_value(self.counter.remaining_percent())

    def complete(self, success):
        self.change_command()
        self.counter.reset()

        if success:
            self.points_display.increment()

    def change_command(self):
        self.cmd_display.change_command(random.choice(commands))



if __name__ == '__main__':
    app = Ursina(borderless=False)
    game = Game(KeyboardController())
    app.run()
