import random
import datetime
import time


class Counter:
    def __init__(self, duration_seconds):
        self.time_to_end = duration_seconds
        self.reset()

    def reset(self):
        self.start_time = time.time()

    def completed(self):
        return self.elapsed() >= self.time_to_end

    def elapsed(self):
        return time.time() - self.start_time

    def remaining_percent(self):
        return (self.time_to_end - self.elapsed()) / self.time_to_end

