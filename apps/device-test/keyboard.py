from ursina import *

from commands import *


class KeyboardController(Entity):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.keys_mapping = {}
        self.keys_mapping[Keys.up_arrow] = UP.cmd_id
        self.keys_mapping[Keys.right_arrow] = RIGHT.cmd_id
        self.keys_mapping[Keys.left_arrow] = LEFT.cmd_id
        self.keys_mapping[Keys.down_arrow] = DOWN.cmd_id
        self.keys_mapping[Keys.enter] = "center"


    def on_command(self, listener):
        self.listener = listener

    def input(self, key):
        cmd_id = self.keys_mapping.get(key, None)

        if cmd_id is not None:
            self.notify(cmd_id)

    def notify(self, cmd_id):
        if self.listener:
            self.listener(cmd_id)