
class GameCommand():
    def __init__(self, name, texture, cmd_id=None):
        self.name = name
        self.texture = texture
        self.cmd_id =  cmd_id if cmd_id else self.name

UP          = GameCommand("cima", "sphere-up", "up")
DOWN        = GameCommand("baixo", "sphere-down", "down")
LEFT        = GameCommand("esquerda", "sphere-left", "left")
RIGHT       = GameCommand("direita", "sphere-right", "right")
CENTER      = GameCommand("centro", "sphere-center", "center")
TOP         = GameCommand("topo", "sphere-top", "top")
FING_INDEX  = GameCommand("indicador", "sphere-index", "index")
FING_MIDDLE = GameCommand("médio", "sphere-middle", "middle")
FING_RING   = GameCommand("anelar", "sphere-ring", "ring")
FING_MIN    = GameCommand("mínimo", "sphere-minimal", "minimal")
GUARD       = GameCommand("defesa", "guard", "guard")
HADOUKEN    = GameCommand("hadouken", "hadouken", "hadouken")
PUNCH       = GameCommand("soco", "punch", "punch")
UPPERCUT    = GameCommand("gancho", "uppercut", "uppercut")

commands = [
    UP, DOWN, LEFT, RIGHT, CENTER, TOP, FING_INDEX, FING_MIDDLE, FING_RING, FING_MIN, GUARD, HADOUKEN, PUNCH, UPPERCUT
]