import asyncio
import bluetooth

from asyncio_discover import AsyncDiscoverer

start_time = None

async def main():
    import sys

    device_name = None if len(sys.argv) < 2 else sys.argv[1]

    print(device_name)

    device_address = await search_device(device_name)

    print("found address: {}".format(device_address))

    if device_address is not None:
        reader, writer = await connect_to(device_address)

        t1 = from_reader_to_sdout(reader)
        t2 = from_stdin_to_writer(writer)

        await asyncio.gather(t1, t2)

async def search_device(device_name):
    import time

    device_address = None

    discoverer = AsyncDiscoverer()

    discoverer.find_devices(lookup_names=True)

    # async for d in discoverer.async_discover():
    while discoverer.is_discovering():
        d = await discoverer.discover_next()

        if not d:
            continue

    # async for d in discoverer.async_discover():
        now_time = time.perf_counter() - start_time
        print('{:0.2f}s: discovered {}'.format(now_time, d))

        address, name, device_class, rssi = d

        if name.decode() == device_name:
            print("found {}".format(name))

            device_address = address

            discoverer.cancel_inquiry()
            # loop = asyncio.get_event_loop()
            # loop.create_task(connect_to(address))

    print("finished")
    return device_address


_DEFAULT_LIMIT = 2 ** 16  # 64 KiB

async def _create_socket_streams(sock, loop=None, limit=_DEFAULT_LIMIT):
    from asyncio import StreamReader, StreamWriter, StreamReaderProtocol

    if loop is None:
        loop = asyncio.get_event_loop()

    reader = StreamReader(limit=limit, loop=loop)
    protocol = StreamReaderProtocol(reader, loop=loop)
    transport, _ = await loop.create_connection(
        lambda: protocol, sock=sock)
    writer = StreamWriter(transport, protocol, reader, loop)

    return (reader, writer)


async def connect_to(device_addr, port=1, loop=None):
    print('connecting')

    if loop is None:
        loop = asyncio.get_event_loop()

    sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    try:
        sock.connect((device_addr, port))
        print("connected!")

        return await _create_socket_streams(sock=sock)

    except Exception as e:
        print("something's wrong with %s:%d. Exception is %s" % (device_addr, port, e))

        sock.close()

        raise


async def from_reader_to_sdout(reader):
    l = asyncio.get_event_loop()

    while l.is_running():
        data = await reader.readline()
        if not data:
            break

        print(data.decode())

async def from_stdin_to_writer(writer):
    from asyncio import StreamReader, StreamWriter, StreamReaderProtocol
    import sys

    l = asyncio.get_event_loop()

    reader = asyncio.StreamReader(loop=l)
    await loop.connect_read_pipe(
        lambda: asyncio.StreamReaderProtocol(reader, loop=l), sys.stdin)


    while l.is_running():
        data = await reader.readline()
        if not data:
            break

        writer.write(data)
        await writer.drain()

if __name__ == "__main__":
    import time
    start_time = time.perf_counter()

    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(main())
    except (KeyboardInterrupt, SystemExit):
        print("finishing...")
        loop.stop()

    elapsed = time.perf_counter() - start_time
    print("{} executed in {:0.2f} seconds.".format(__file__, elapsed))
