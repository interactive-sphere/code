# Example from: https://github.com/hbldh/bleak/blob/develop/examples/uart_service.py

"""
UART Service
-------------
An example showing how to write a simple program using the Nordic Semiconductor
(nRF) UART service.
"""

import asyncio
import sys
from itertools import count, takewhile
from typing import Iterator

from bleak import BleakClient, BleakScanner
from bleak.backends.characteristic import BleakGATTCharacteristic
from bleak.backends.device import BLEDevice
from bleak.backends.scanner import AdvertisementData

UART_SERVICE_UUID = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E"
UART_RX_CHAR_UUID = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
UART_TX_CHAR_UUID = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

def main():
    try:
        asyncio.run(uart_terminal())
    except asyncio.CancelledError:
        # task is cancelled on disconnect, so we ignore this error
        pass


# TIP: you can get this function and more from the ``more-itertools`` package.
def sliced(data: bytes, n: int) -> Iterator[bytes]:
    """
    Slices *data* into chunks of size *n*. The last slice may be smaller than
    *n*.
    """
    return takewhile(len, (data[i : i + n] for i in count(0, n)))


def readline() -> str:
    line = sys.stdin.buffer.readline()

    # assume line is a bytes instance
    return line.decode("unicode_escape")

async def uart_terminal():
    """This is a simple "terminal" program that uses the Nordic Semiconductor
    (nRF) UART service. It reads from stdin and sends each line of data to the
    remote device. Any data received from the device is printed to stdout.
    """

    # service_id = UART_SERVICE_UUID if len(sys.argv) <= 1 else sys.argv[1]
    service_id = sys.argv[1] if len(sys.argv) > 1 else UART_SERVICE_UUID
    rx_uuid    = sys.argv[2] if len(sys.argv) > 2 else UART_RX_CHAR_UUID
    tx_uuid    = sys.argv[3] if len(sys.argv) > 3 else UART_TX_CHAR_UUID




    def match_nus_uuid(device: BLEDevice, adv: AdvertisementData):
        # This assumes that the device includes the UART service UUID in the
        # advertising data. This test may need to be adjusted depending on the
        # actual advertising data supplied by the device.
        if service_id.lower() in adv.service_uuids:
            return True

        return False

    device = await BleakScanner.find_device_by_filter(match_nus_uuid)
    print('found: ', device)

    if device is None:
        print("no matching device found, you may need to edit match_nus_uuid().")
        sys.exit(1)

    def handle_disconnect(_: BleakClient):
        print("Device was disconnected, goodbye.")
        # cancelling all tasks effectively ends the program
        for task in asyncio.all_tasks():
            task.cancel()

    def handle_rx(_: BleakGATTCharacteristic, data: bytearray):
        print("received:", data)

    async with BleakClient(device, disconnected_callback=handle_disconnect) as client:
        print("connected")
        # print("pairing")
        # await client.pair()

        print("listen notifications")
        await client.start_notify(tx_uuid, handle_rx)

        print("Connected, start typing and press ENTER...")

        nus = client.services.get_service(service_id)
        rx_char = nus.get_characteristic(rx_uuid)
        loop = asyncio.get_running_loop()


        # nus = client.services.get_service(service_id)
        # rx_char = nus.get_characteristic(rx_uuid)
        # tx_char = nus.get_characteristic(tx_uuid)

        # print("listen notifications")
        # await client.start_notify(tx_char, handle_rx)

        # print("Connected, start typing and press ENTER...")

        loop = asyncio.get_running_loop()
        while True:
            # This waits until you type a line and press ENTER.
            # A real terminal program might put stdin in raw mode so that things
            # like CTRL+C get passed to the remote device.
            data = await loop.run_in_executor(None, readline)

            # data will be empty on EOF (e.g. CTRL+D on *nix)
            if not data:
                break

            # some devices, like devices running MicroPython, expect Windows
            # line endings (uncomment line below if needed)
            # data = data.replace(b"\n", b"\r\n")

            # Writing without response requires that the data can fit in a
            # single BLE packet. We can use the max_write_without_response_size
            # property to split the data into chunks that will fit.

            for s in sliced(data, rx_char.max_write_without_response_size):
                print('client.write_gatt_char: ', s)
                await client.write_gatt_char(rx_char, s)

            print("sent:", data)


if __name__ == "__main__":
    main()