"""
Service Explorer
----------------

An example showing how to access and print out the services, characteristics and
descriptors of a connected GATT server.

Created on 2019-03-25 by hbldh <henrik.blidh@nedomkull.com>

"""
import platform
import asyncio
import logging
import sys

from bleak import BleakClient, BleakScanner


async def run(loop, debug=True):
    if len(sys.argv) < 2:
        print("Usage: <name or device address>")
        sys.exit(1);

    log = logging.getLogger(__name__)
    if debug:
        loop.set_debug(True)
        log.setLevel(logging.DEBUG)
        h = logging.StreamHandler(sys.stdout)
        h.setLevel(logging.DEBUG)
        log.addHandler(h)

    name_or_address = sys.argv[1]

    devices = await BleakScanner.discover()
    d = select_device(devices, name_or_address)
    if d is None:
        return

    print('connecting to: ', d)

    # c = BleakClient(d, loop=loop)

    # connected = False
    # try:
    #     print('connecting')
    #     connected = await c.connect()
    #     print('connected = ', connected)
    # finally:
    #     if connected:
    #         print('disconnecting')
    #         await c.disconnect()

    async with BleakClient(d, loop=loop) as client:
        print('got client: ', client)
        print('await for connection')
        x = await client.is_connected()
        log.info("Connected: {0}".format(x))

        for service in client.services:
            log.info("[Service] {0}: {1}".format(service.uuid, service.description))
            for char in service.characteristics:
                if "read" in char.properties:
                    try:
                        value = bytes(await client.read_gatt_char(char.uuid))
                    except Exception as e:
                        value = str(e).encode()
                else:
                    value = None
                log.info(
                    "\t[Characteristic] {0}: ({1}) | Name: {2}, Value: {3} ".format(
                        char.uuid, ",".join(char.properties), char.description, value
                    )
                )
                for descriptor in char.descriptors:
                    value = await client.read_gatt_descriptor(descriptor.handle)
                    log.info(
                        "\t\t[Descriptor] {0}: (Handle: {1}) | Value: {2} ".format(
                            descriptor.uuid, descriptor.handle, bytes(value)
                        )
                    )

def select_device(devices, name_or_address):
    print('selecting device with name or address = ', name_or_address)

    for d in devices:
        print('{}: {}'.format(d.name, d.address))

        if d.name == name_or_address or d.address.lower() == name_or_address.lower():
            return d

    return None


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(loop, True))
