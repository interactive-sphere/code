var noble = require('@abandonware/noble');
const async = require('async')

var lookingForDevice = true;

function onDevice(nameOrAddress, callback) {
    // console.log(`onDevice(${nameOrAddress})`);
    noble.on('stateChange', function(state) {
      if (state === 'poweredOn') {
        noble.startScanning();
      } else {
        noble.stopScanning();
      }
    });

    noble.on('discover', function(peripheral) {
        if (!lookingForDevice) {
            console.debug('discovered peripheral after stop scanning');
            return;
        }

        name = peripheral.advertisement.localName ? peripheral.advertisement.localName.toLowerCase() : undefined;
        addr = peripheral.address.toLowerCase();

        // console.log(`device = ${name}, ${addr}`);

        if (name === nameOrAddress
            ||  addr === nameOrAddress)
        {
            lookingForDevice = false;

            noble.stopScanning();

            // callback(peripheral);
            peripheral.once('disconnect', function() {
                console.log('Peripheral disconnected!');
                process.exit(0);
            });

            peripheral.connect(function(error) {
                if (error) {
                    console.error('Connection error:');
                    console.error(error);
                }
                // else {
                //     console.debug('Connected!');
                // }

                callback(peripheral);
            });
        }
    });
}

function discoverService(peripheral, srvUUID, callback) {
    peripheral.discoverServices([srvUUID], (error, services) => {
        // console.log(services);
        srv = (services && services.length > 0) ? services[0] : null;

        callback(error, srv);
    });
}

function discoverCharacteristic(srv, characteristicUUID, callback) {
    srv.discoverCharacteristics([characteristicUUID], (error, characteristics) => {
        characteristic = (characteristics && characteristics.length > 0) ? characteristics[0] : null;

        callback(error, characteristic);
    });
}

function getCharacteristic(peripheral, srvUUID, characteristicUUID, callback) {

    peripheral.discoverSomeServicesAndCharacteristics([srvUUID], [characteristicUUID],
        function (error, services, characteristics) {
            characteristic = (characteristics && characteristics.length > 0)
                             ? characteristics[0]
                             : undefined;

            callback(error, characteristic);
        }
    );
}

var peripheralNameOrAddress = process.argv[2].toLowerCase();
var serviceUUID = process.argv[3].toLowerCase();
var characteristicUUID = process.argv[4].toLowerCase();


console.log(`Looking for device: "${peripheralNameOrAddress}"`);

onDevice(peripheralNameOrAddress, peripheral => {
    // console.debug('peripheral with name "' + peripheral.advertisement.localName + '" and address "' + peripheral.address + '" found');

    console.debug(`Listing service ${serviceUUID} `
        + `and characteristic ${characteristicUUID} `
        + `of device: "${peripheral.advertisement.localName}" at ${peripheral.address}`);

    getCharacteristic(peripheral, serviceUUID, characteristicUUID,
        function (error, characteristic) {
            if (error) {
                console.error(error);
                process.exit(0);
            }

            console.debug(`Watching characteristic: uuid=${characteristic.uuid}, name=${characteristic.name}`);
            console.debug(characteristic);

            characteristic.on('read', function(data, isNotification){
                console.log(`Got value:`);
                console.log(`\thex: ${data.toString('hex')}`);
                console.log(`\tutf-8: "${data.toString('utf-8')}"`);
            });

            // characteristic.subscribe(function (error) {
            //     if (error) {
            //         console.error(error);
            //     }
            //     console.log('subscribe');
            // })

            characteristic.once('notify', function () {
                console.log('subscribed with success!');
            });

            characteristic.subscribe();

            console.log('subscribed');
            // characteristic.read(function (error, data) {
            //     console.log(`Characteristic value:`);
            //     console.log(`\thex: ${data.toString('hex')}`);
            //     console.log(`\tutf-8: "${data.toString('utf-8')}"`);
            //     process.exit(0);
            // });

            // console.log(noble._bindings);
        }
    );
});
