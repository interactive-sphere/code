import asyncio
import bluetooth

_DEFAULT_READER_LIMIT = 2 ** 16  # 64 KiB


class ConnectionException(Exception):
    pass


class Device(object):
    def __init__(self, address, name, device_class, rssi):
        self.address = address
        self.name = (name if isinstance(name, bytes) else name.encode("utf-8"))
        self.device_class = device_class
        self.rssi = rssi

        self.reader = None
        self.writer = None

    def __str__(self):
        return "{}:{}".format(self.name, self.address)

    def match_name(self, name):
        try:
            name = name.encode("utf-8")
        except (UnicodeEncodeError, AttributeError):
            # already bytes?
            pass

        return self.name == name

    async def connect(self, port=1, loop=None):
        if loop is None:
            loop = asyncio.get_event_loop()

        sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

        try:
            sock.connect((self.address, port))

            self.reader, self.writer = await _create_socket_streams(sock=sock)

        except Exception as e:
            sock.close()

            raise ConnectionException("Failed to connect with %s:%d") from e


async def _create_socket_streams(sock, loop=None, limit=_DEFAULT_READER_LIMIT):
    from asyncio import StreamReader, StreamWriter, StreamReaderProtocol

    if loop is None:
        loop = asyncio.get_event_loop()

    reader = StreamReader(limit=limit, loop=loop)
    protocol = MyStreamReaderProtocol(reader, loop=loop)
    transport, _ = await loop.create_connection(
        lambda: protocol, sock=sock)
    writer = StreamWriter(transport, protocol, reader, loop)

    return (reader, writer)


class MyStreamReaderProtocol(asyncio.StreamReaderProtocol):
    def connection_lost(self, exc):
        print("connection lost!")

        super().connection_lost(exc)
